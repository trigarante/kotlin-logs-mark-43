package com.ahorraseguros.mx.logskotlinmark43

import org.junit.Test
import org.junit.runner.RunWith
import org.springframework.boot.test.context.SpringBootTest
import org.springframework.test.context.junit4.SpringRunner

@RunWith(SpringRunner::class)
@SpringBootTest
class Logskotlinmark43ApplicationTests {

    @Test
    fun contextLoads() {
    }

}
