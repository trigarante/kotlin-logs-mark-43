package com.ahorraseguros.mx.logskotlinmark43.models.marketing

import javax.persistence.*
import java.util.Objects

@Entity
@Table(name = "campana")
class CampanaModelsLogs {
    @get:Id
    @get:Column(name = "id")
    var id: Long = 0
    @get:Basic
    @get:Column(name = "idSubRamo")
    var idSubRamo: Int = 0
    @get:Basic
    @get:Column(name = "nombre")
    var nombre: String? = null
    @get:Basic
    @get:Column(name = "descripcion")
    var descripcion: String? = null
    @get:Basic
    @get:Column(name = "activo")
    var activo: Int = 0
    @get:Basic
    @get:Column(name = "idEstadoCampana")
    var idEstadoCampana: Int = 0
    @get:Basic
    @get:Column(name = "idLog")
    var idLog: Long = 0
}
