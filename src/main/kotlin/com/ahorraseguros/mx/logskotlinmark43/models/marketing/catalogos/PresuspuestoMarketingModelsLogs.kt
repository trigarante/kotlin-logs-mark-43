package com.ahorraseguros.mx.logskotlinmark43.models.marketing.catalogos

import javax.persistence.*
import java.sql.Timestamp

@Entity
@Table(name = "presuspuestoMarketing")
class PresuspuestoMarketingModelsLogs {
    @get:Id
    @get:Column(name = "id")
    var id: Int = 0
    @get:Basic
    @get:Column(name = "candidad")
    var candidad: Double = 0.toDouble()
    @get:Basic
    @get:Column(name = "fechaPresupuesto")
    var fechaPresupuesto: Timestamp? = null
    @get:Basic
    @get:Column(name = "activo")
    var activo: Byte = 0
    @get:Basic
    @get:Column(name = "idLog")
    var idLog: Long = 0
}
