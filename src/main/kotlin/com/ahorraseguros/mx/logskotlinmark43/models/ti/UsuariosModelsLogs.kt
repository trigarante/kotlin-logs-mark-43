package com.ahorraseguros.mx.logskotlinmark43.models.ti

import javax.persistence.*
import java.sql.Timestamp

@Entity
@Table(name = "usuarios")
data class UsuariosModelsLogs (
    @get:Id
    @get:Column(name = "id")
    var id: Long = 0,
    @get:Basic
    @get:Column(name = "idCorreo")
    var idCorreo: String? = null,
    @get:Basic
    @get:Column(name = "idGrupo")
    var idGrupo: Int = 0,
    @get:Basic
    @get:Column(name = "idTipo")
    var idTipo: Int = 0,
    @get:Basic
    @get:Column(name = "idSubarea")
    var idSubarea: Int = 0,
    @get:Basic
    @get:Column(name = "usuario")
    var usuario: String? = null,
    @get:Basic
    @get:Column(name = "password")
    var password: String? = null,
    @get:Basic
    @get:Column(name = "fechaCreacion")
    var fechaCreacion: Timestamp? = null,
    @get:Basic
    @get:Column(name = "datosUsuario")
    var datosUsuario: String? = null,
    @get:Basic
    @get:Column(name = "estado")
    var estado: Byte = 0,
    @get:Basic
    @get:Column(name = "nombre")
    var nombre: String? = null,
    @get:Basic
    @get:Column(name = "idLog")
    var idLog: Long? = null
)
