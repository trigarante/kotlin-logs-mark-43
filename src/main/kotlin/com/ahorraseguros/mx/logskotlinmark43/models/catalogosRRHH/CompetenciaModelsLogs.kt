package com.ahorraseguros.mx.logskotlinmark43.models.catalogosRRHH

import javax.persistence.*

@Entity
@Table(name = "competencias")
data class CompetenciaModelsLogs (
    @get:Id
    @get:Column(name = "id")
    var id: Int = 0,
    @get:Basic
    @get:Column(name = "escala")
    var escala: Int = 0,
    @get:Basic
    @get:Column(name = "estado")
    var estado: String? = null,
    @get:Basic
    @get:Column(name = "descripcion")
    var descripcion: String? = null,
    @get:Basic
    @get:Column(name = "activo")
    var activo: Int? = null,
    @get:Basic
    @get:Column(name = "idLogs")
    var idLogs: Long = 0
)
