package com.ahorraseguros.mx.logskotlinmark43.models.marketing.catalogos

import javax.persistence.*

@Entity
@Table(name = "estadoCampana")
class EstadoCampanaModelsLogs {
    @get:Id
    @get:Column(name = "id")
    var id: Int = 0
    @get:Basic
    @get:Column(name = "estado")
    var estado: String? = null
    @get:Basic
    @get:Column(name = "activo")
    var activo: Int = 0
    @get:Basic
    @get:Column(name = "idLogs")
    var idLogs: Long = 0
}
