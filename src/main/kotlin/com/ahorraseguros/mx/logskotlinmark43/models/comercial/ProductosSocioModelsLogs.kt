package com.ahorraseguros.mx.logskotlinmark43.models.comercial

import javax.persistence.*
import java.util.Objects

@Entity
@Table(name = "productosSocio")
class ProductosSocioModelsLogs {
    @get:Id
    @get:Column(name = "id")
    var id: Long = 0
    @get:Basic
    @get:Column(name = "idSubRamo")
    var idSubRamo: Int = 0
    @get:Basic
    @get:Column(name = "idTipoProducto")
    var idTipoProducto: Int = 0
    @get:Basic
    @get:Column(name = "prioridad")
    var prioridad: Int = 0
    @get:Basic
    @get:Column(name = "nombre")
    var nombre: String? = null
    @get:Basic
    @get:Column(name = "activo")
    var activo: Int = 0
    @get:Basic
    @get:Column(name = "idLog")
    var idLog: Long = 0

}
