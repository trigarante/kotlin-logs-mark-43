package com.ahorraseguros.mx.logskotlinmark43.models.ti.catalogos

import javax.persistence.*
import java.util.Objects

@Entity
@Table(name = "permisosVisualizacion")
class PermisosVisualizacionModelLogs {
    @get:Id
    @get:Column(name = "id")
    var id: Int = 0
    @get:Basic
    @get:Column(name = "descripción")
    var descripcion: String? = null
    @get:Basic
    @get:Column(name = "activo")
    var activo: Byte = 0
    @get:Basic
    @get:Column(name = "idLog")
    var idLog: Long = 0

}
