package com.ahorraseguros.mx.logskotlinmark43.models.comercial

import javax.persistence.*

@Entity
@Table(name = "socios")
class SociosModelsLogs {
    @get:Id
    @get:Column(name = "id")
    var id: Int = 0
    @get:Basic
    @get:Column(name = "prioridad")
    var prioridad: Int = 0
    @get:Basic
    @get:Column(name = "nombreComercial")
    var nombreComercial: String? = null
    @get:Basic
    @get:Column(name = "rfc")
    var rfc: String? = null
    @get:Basic
    @get:Column(name = "razonSocial")
    var razonSocial: String? = null
    @get:Basic
    @get:Column(name = "alias")
    var alias: String? = null
    @get:Basic
    @get:Column(name = "idEstadoSocio")
    var idEstadoSocio: Int = 0
    @get:Basic
    @get:Column(name = "idPais")
    var idPais: Int = 0
    @get:Basic
    @get:Column(name = "activo")
    var activo: String? = null
    @get:Basic
    @get:Column(name = "idLog")
    var idLog: Long = 0
}
