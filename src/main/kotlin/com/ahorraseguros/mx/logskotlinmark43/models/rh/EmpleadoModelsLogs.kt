package com.ahorraseguros.mx.logskotlinmark43.models.rh

import java.sql.Blob
import javax.persistence.*
import java.sql.Date
import java.sql.Timestamp

@Entity
@Table(name = "empleado")
data class EmpleadoModelsLogs (
    @get:Id
    @get:Column(name = "id")
    var id: Long = 0,
    @get:Basic
    @get:Column(name = "idBanco")
    var idBanco: Int? = null,
    @get:Basic
    @get:Column(name = "idCandidato")
    var idCandidato: Long = 0,
    @get:Basic
    @get:Column(name = "idPuesto")
    var idPuesto: Int = 0,
    @get:Basic
    @get:Column(name = "idTipoPuesto")
    var idTipoPuesto: Int = 0,
    @get:Basic
    @get:Column(name = "idUsuario")
    var idUsuario: Long? = null,
    @get:Basic
    @get:Column(name = "puestoDetalle")
    var puestoDetalle: String? = null,
    @get:Basic
    @get:Column(name = "fechaAltaIMSS")
    var fechaAltaImss: Timestamp? = null,
    @get:Basic
    @get:Column(name = "documentosPersonales")
    var documentosPersonales: String? = null,
    @get:Basic
    @get:Column(name = "documentosAdministrativos")
    var documentosAdministrativos: String? = null,
    @get:Basic
    @get:Column(name = "fechaRegistro")
    var fechaRegistro: Timestamp? = Timestamp(System.currentTimeMillis()),
    @get:Basic
    @get:Column(name = "fechaIngreso")
    var fechaIngreso: Timestamp? = null,
    @get:Basic
    @get:Column(name = "idTurnoEmpleado")
    var idTurnoEmpleado: Int = 0,
    @get:Basic
    @get:Column(name = "sueldoDiario")
    var sueldoDiario: Double? = null,
    @get:Basic
    @get:Column(name = "sueldoMensual")
    var sueldoMensual: Double? = null,
    @get:Basic
    @get:Column(name = "kpi")
    var kpi: Boolean = false,
    @get:Basic
    @get:Column(name = "kpiMensual")
    var kpiMensual: Float? = null,
    @get:Basic
    @get:Column(name = "kpiTrimestral")
    var kpiTrimestral: Float? = null,
    @get:Basic
    @get:Column(name = "kpiSemestral")
    var kpiSemestral: Float? = null,
    @get:Basic
    @get:Column(name = "fechaCambioSueldo")
    var fechaCambioSueldo: Date? = null,
    @get:Basic
    @get:Column(name = "ctaClabe")
    var ctaClabe: String? = null,
    @get:Basic
    @get:Column(name = "fechaAsignacion")
    var fechaAsignacion: Timestamp? = null,
    @get: Basic
    @get: Column(name="comentarios")
    var comentarios: String? = null,
    @get: Basic
    @get: Column(name="imss")
    var imss: String? = null,
    @Lob
    @get: Column(name="imagenEmpleado")
    var imagenEmpleado: Blob? = null,
    @get:Basic
    @get: Column(name ="idRazonSocial")
    var razonSocial: String? = null,
    @get:Basic
    @get: Column(name ="rfc")
    var rfc: String? = null,
    @get: Column(name = "fechaFiniquito")
    var fechaFiniquito: Timestamp? = null,
    @get: Column(name = "idEstadoFiniquito")
    var idEstadoFiquito: Int? = 0,
    @get: Column(name = "montoFiniquito")
    var montoFiniquito: Float? = null,
    @get: Column(name = "tarjetaSecundaria")
    var tarjetaSecundaria: String? = null,
    @get:Basic
    @get:Column(name = "idLog")
    var idLog: Long = 0
)

