package com.ahorraseguros.mx.logskotlinmark43.models.autorizacion

import java.util.*
import javax.persistence.*

@Entity
@Table(name = "erroresAnexosA")
class ErroresAnexosALogs {
    @get:Column(name = "id")
    @get:Id
    var id: Long = 0

    @get:Column(name = "idAutorizacionRegistro")
    @get:Basic
    var idAutorizacionRegistro: Long = 0

    @get:Column(name = "idEmpleado")
    @get:Basic
    var idEmpleado: Long = 0

    @get:Column(name = "idEstadoCorreccion")
    @get:Basic
    var idEstadoCorreccion: Int = 0

    @get:Column(name = "idTipoDocumento")
    @get:Basic
    var idTipoDocumento: Int = 0

    @get:Column(name = "idEmpleadoCorreccion")
    @get:Basic
    var idEmpleadoCorreccion: Long = 0

    @get:Column(name = "correcciones")
    @get:Basic
    var correcciones: String? = null

    @get:Column(name = "idLogs")
    @get:Basic
    var idLogs: Long = 0


}