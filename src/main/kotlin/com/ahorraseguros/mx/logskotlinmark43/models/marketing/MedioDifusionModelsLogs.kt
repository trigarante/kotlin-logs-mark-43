package com.ahorraseguros.mx.logskotlinmark43.models.marketing

import javax.persistence.*
import java.util.Objects

@Entity
@Table(name = "medioDifusion")
class MedioDifusionModelsLogs {
    @get:Id
    @get:Column(name = "id")
    var id: Int = 0
    @get:Basic
    @get:Column(name = "idSubarea")
    var idSubarea: Int = 0
    @get:Basic
    @get:Column(name = "idProveedor")
    var idProveedor: Int = 0
    @get:Basic
    @get:Column(name = "idExterno")
    var idExterno: String? = null
    @get:Basic
    @get:Column(name = "idPagina")
    var idPagina: Int = 0
    @get:Basic
    @get:Column(name = "idPresupuesto")
    var idPresupuesto: Int = 0
    @get:Basic
    @get:Column(name = "descripcion")
    var descripcion: String? = null
    @get:Basic
    @get:Column(name = "configuracion")
    var configuracion: String? = null
    @get:Basic
    @get:Column(name = "activo")
    var activo: Byte = 0
    @get:Basic
    @get:Column(name = "idLod")
    var idLod: Long = 0

}
