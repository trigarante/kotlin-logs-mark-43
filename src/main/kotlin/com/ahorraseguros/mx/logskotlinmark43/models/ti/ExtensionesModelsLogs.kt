package com.ahorraseguros.mx.logskotlinmark43.models.ti

import javax.persistence.*

@Entity
@Table(name = "extensiones")
data class ExtensionesModelsLogs (
    @get:Id
    @get:Column(name = "id")
    var id: Int = 0,
    @get:Basic
    @get:Column(name = "idSubarea")
    var idSubarea: Int = 0,
    @get:Basic
    @get:Column(name = "idEstado")
    var idEstado: Int = 0,
    @get:Basic
    @get:Column(name = "descrip")
    var descrip: String? = null,
    @get:Basic
    @get:Column(name = "idLog")
    var idLog: Long = 0
)
