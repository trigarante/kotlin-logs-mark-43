package com.ahorraseguros.mx.logskotlinmark43.models.ti

import javax.persistence.*

@Entity
@Table(name = "grupoUsuarios")
data class GrupoUsuariosModelsLogs (
    @get:Id
    @get:Column(name = "id")
    var id: Int = 0,
    @get:Basic
    @get:Column(name = "idSubarea")
    var idSubarea: Int = 0,
    @get:Basic
    @get:Column(name = "idPuesto")
    var idPuesto: Int = 0,
    @get:Basic
    @get:Column(name = "idPermisosVisualizacion")
    var idPermisosVisualizacion: Int = 0,
    @get:Basic
    @get:Column(name = "nombre")
    var nombre: String = "",
    @get:Basic
    @get:Column(name = "descripcion")
    var descripcion: String? = null,
    @get:Basic
    @get:Column(name = "permisos")
    var permisos: String? = null,
    @get:Basic
    @get:Column(name = "activo")
    var activo: Int = 0,
    @get:Basic
    @get:Column(name = "idLog")
    var idLog: Long? = null
)
