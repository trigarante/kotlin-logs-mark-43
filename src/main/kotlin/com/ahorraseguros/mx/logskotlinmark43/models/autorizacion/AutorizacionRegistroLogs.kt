package com.ahorraseguros.mx.logskotlinmark43.models.autorizacion

import com.ahorraseguros.mx.logskotlinmark43.models.autorizacion.AutorizacionRegistroLogs
import java.util.*
import javax.persistence.*

@Entity
@Table(name = "autorizacionRegistro")
class AutorizacionRegistroLogs {
    @get:Column(name = "id")
    @get:Id
    var id: Long = 0

    @get:Column(name = "idRegistro")
    @get:Basic
    var idRegistro: Long = 0

    @get:Column(name = "idEstadoVerificacion")
    @get:Basic
    var idEstadoVerificacion: Int = 0

    @get:Column(name = "numero")
    @get:Basic
    var numero: Int = 0

    @get:Column(name = "verificadoCliente")
    @get:Basic
    var verificadoCliente: Byte = 0

    @get:Column(name = "verificadoProducto")
    @get:Basic
    var verificadoProducto: Byte = 0

    @get:Column(name = "verificadoRegistro")
    @get:Basic
    var verificadoRegistro: Byte = 0

    @get:Column(name = "verificadoPago")
    @get:Basic
    var verificadoPago: Byte = 0

    @get:Column(name = "verificadoInspeccion")
    @get:Basic
    var verificadoInspeccion: Byte = 0

    @get:Column(name = "idLogs")
    @get:Basic
    var idLogs: Long = 0

}