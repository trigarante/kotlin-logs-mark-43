package com.ahorraseguros.mx.logskotlinmark43.models.ventaNueva

import javax.persistence.*
import java.sql.Timestamp
import java.util.Objects

@Entity
@Table(name = "colaSolicitudes")
class ColaSolicitudesModelsLogs {
    @get:Id
    @get:Column(name = "id")
    var id: Int = 0
    @get:Basic
    @get:Column(name = "idCotizacionAli")
    var idCotizacionAli: Long = 0
    @get:Basic
    @get:Column(name = "idEmpleado")
    var idEmpleado: Long = 0
    @get:Basic
    @get:Column(name = "fechaCotizacion")
    var fechaCotizacion: Timestamp? = null
    @get:Basic
    @get:Column(name = "idLogs")
    var idLogs: Long = 0

}
