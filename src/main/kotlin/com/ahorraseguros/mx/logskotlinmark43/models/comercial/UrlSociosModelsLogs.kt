package com.ahorraseguros.mx.logskotlinmark43.models.comercial

import javax.persistence.*

@Entity
@Table(name = "urlSocios")
class UrlSociosModelsLogs {
    @get:Id
    @get:Column(name = "id")
    var id: Int = 0
    @get:Basic
    @get:Column(name = "idSocio")
    var idSocio: Int? = null
    @get:Basic
    @get:Column(name = "url")
    var url: String? = null
    @get:Basic
    @get:Column(name = "descripcion")
    var descripcion: String? = null
    @get:Basic
    @get:Column(name = "activo")
    var activo: Int = 0
    @get:Basic
    @get:Column(name = "idLos")
    var idLogs: Long = 0


}
