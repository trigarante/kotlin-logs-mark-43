package com.ahorraseguros.mx.logskotlinmark43.models.rh

import javax.persistence.*
import java.sql.Timestamp

@Entity
@Table(name = "candidato")
data class CandidatoModelsLogs (
    @get:Id
    @get:Column(name = "id")
    var id: Long = 0,
    @get:Basic
    @get:Column(name = "idPrecandidato")
    var idPrecandidato: Long = 0,
    @get:Basic
    @get:Column(name = "calificacionPsicometrico")
    var calificacionPsicometrico: Boolean = false,
    @get:Basic
    @get:Column(name = "calificacionExamen")
    var calificacionExamen: Double = 0.toDouble(),
    @get:Basic
    @get:Column(name = "comentarios")
    var comentarios: String? = null,
    @get:Basic
    @get:Column(name = "fechaIngreso")
    var fechaIngreso: Timestamp? = null,
    @get:Basic
    @get:Column(name = "idLog")
    var idLog: Long = 0
)
