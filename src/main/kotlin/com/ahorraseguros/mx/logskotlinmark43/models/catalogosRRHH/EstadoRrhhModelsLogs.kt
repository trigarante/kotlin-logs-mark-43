package com.ahorraseguros.mx.logskotlinmark43.models.catalogosRRHH

import javax.persistence.*

@Entity
@Table(name = "estadoRRHH")
data class EstadoRrhhModelsLogs (
    @get:Id
    @get:Column(name = "id")
    var id: Long = 0,
    @get:Basic
    @get:Column(name = "idEstadoRH")
    var idEstadoRh: Int = 0,
    @get:Basic
    @get:Column(name = "idEtapa")
    var idEtapa: Int = 0,
    @get:Basic
    @get:Column(name = "estado")
    var estado: String? = null,
    @get:Basic
    @get:Column(name = "activo")
    var activo: Int = 0,
    @get:Basic
    @get:Column(name = "idLog")
    var idLog: Long = 0
)
