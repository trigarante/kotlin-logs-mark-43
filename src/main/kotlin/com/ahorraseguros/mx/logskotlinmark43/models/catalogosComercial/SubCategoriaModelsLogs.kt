package com.ahorraseguros.mx.logskotlinmark43.models.catalogosComercial

import javax.persistence.*
import java.util.Objects

@Entity
@Table(name = "subCategoria")
class SubCategoriaModelsLogs {
    @get:Id
    @get:Column(name = "id")
    var id: Int = 0
    @get:Basic
    @get:Column(name = "idCategoria")
    var idCategoria: Int = 0
    @get:Basic
    @get:Column(name = "detalle")
    var detalle: String? = null
    @get:Basic
    @get:Column(name = "regla")
    var regla: String? = null
    @get:Basic
    @get:Column(name = "activo")
    var activo: Int = 0
    @get:Basic
    @get:Column(name = "idDivisas")
    var idDivisas: Int = 0
    @get:Basic
    @get:Column(name = "idLog")
    var idLog: Long = 0

}
