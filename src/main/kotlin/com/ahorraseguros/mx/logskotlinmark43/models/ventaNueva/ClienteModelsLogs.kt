package com.ahorraseguros.mx.logskotlinmark43.models.ventaNueva

import javax.persistence.*
import java.sql.Date
import java.sql.Timestamp
import java.util.Objects

@Entity
@Table(name = "cliente")
class ClienteModelsLogs {
    @get:Id
    @get:Column(name = "id")
    var id: Long = 0
    @get:Basic
    @get:Column(name = "idPais")
    var idPais: Int = 0
    @get:Basic
    @get:Column(name = "razonSocial")
    var razonSocial: Byte = 0
    @get:Basic
    @get:Column(name = "nombre")
    var nombre: String? = null
    @get:Basic
    @get:Column(name = "paterno")
    var paterno: String? = null
    @get:Basic
    @get:Column(name = "materno")
    var materno: String? = null
    @get:Basic
    @get:Column(name = "cp")
    var cp: Int = 0
    @get:Basic
    @get:Column(name = "calle")
    var calle: String? = null
    @get:Basic
    @get:Column(name = "numInt")
    var numInt: String? = null
    @get:Basic
    @get:Column(name = "numExt")
    var numExt: String? = null
    @get:Basic
    @get:Column(name = "idColonia")
    var idColonia: Int = 0
    @get:Basic
    @get:Column(name = "genero")
    var genero: String? = null
    @get:Basic
    @get:Column(name = "telefonoFijo")
    var telefonoFijo: String? = null
    @get:Basic
    @get:Column(name = "telefonoMovil")
    var telefonoMovil: String? = null
    @get:Basic
    @get:Column(name = "correo")
    var correo: String? = null
    @get:Basic
    @get:Column(name = "fechaNacimiento")
    var fechaNacimiento: Date? = null
    @get:Basic
    @get:Column(name = "curp")
    var curp: String? = null
    @get:Basic
    @get:Column(name = "rfc")
    var rfc: String? = null
    @get:Basic
    @get:Column(name = "idLogs")
    var idLogs: Long = 0

}
