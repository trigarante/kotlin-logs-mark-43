package com.ahorraseguros.mx.logskotlinmark43.models.rh

import javax.persistence.*

@Entity
@Table(name = "vacantes")
data class VacantesModelsLogs (
    @get:Id
    @get:Column(name = "id")
    var id: Int = 0,
    @get:Basic
    @get:Column(name = "nombre")
    var nombre: String? = null,
    @get:Basic
    @get:Column(name = "idPuesto")
    var idPuesto: Int = 0,
    @get:Basic
    @get:Column(name = "idSubarea")
    var idSubarea: Int = 0,
    @get:Basic
    @get:Column(name = "idTipoPuesto")
    var idTipoPuesto: Int = 0,
    @get:Basic
    @get:Column(name = "activo")
    var activo: Int = 0,
    @get:Basic
    @get:Column(name = "idLog")
    var idLog: Long = 0

)
