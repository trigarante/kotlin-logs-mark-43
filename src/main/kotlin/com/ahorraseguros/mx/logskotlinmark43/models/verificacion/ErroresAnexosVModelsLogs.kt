package com.ahorraseguros.mx.logskotlinmark43.models.verificacion

import java.util.*
import javax.persistence.*

@Entity
@Table(name = "erroresAnexosV")
class ErroresAnexosVModelsLogs {
    @get:Column(name = "id")
    @get:Id
    var id: Long = 0

    @get:Column(name = "idVerificacionRegistro")
    @get:Basic
    var idVerificacionRegistro: Long = 0

    @get:Column(name = "idEmpleado")
    @get:Basic
    var idEmpleado: Long = 0

    @get:Column(name = "idEstadoCorreccion")
    @get:Basic
    var idEstadoCorreccion: Byte = 0

    @get:Column(name = "idTipoDocumento")
    @get:Basic
    var idTipoDocumento: Int? = null

    @get:Column(name = "idEmpleadoCorreccion")
    @get:Basic
    var idEmpleadoCorreccion: Long = 0

    @get:Column(name = "correcciones")
    @get:Basic
    var correcciones: String? = null

    @get:Column(name = "idLogs")
    @get:Basic
    var idLogs: Long = 0

}