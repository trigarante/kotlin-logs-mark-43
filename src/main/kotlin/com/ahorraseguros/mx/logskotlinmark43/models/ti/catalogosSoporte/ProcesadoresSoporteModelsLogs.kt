package com.ahorraseguros.mx.logskotlinmark43.models.ti.catalogosSoporte

import javax.persistence.*
import java.util.Objects

@Entity
@Table(name = "procesadoresSoporte")
class ProcesadoresSoporteModelsLogs {
    @get:Id
    @get:Column(name = "id")
    var id: Int = 0
    @get:Basic
    @get:Column(name = "nombreProcesador")
    var nombreProcesador: String? = null
    @get:Basic
    @get:Column(name = "activo")
    var activo: Int = 0
    @get:Basic
    @get:Column(name = "idLogs")
    var idLogs: Long = 0

}
