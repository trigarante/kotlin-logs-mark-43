package com.ahorraseguros.mx.logskotlinmark43.models.ti.soporte

import javax.persistence.*
import java.sql.Timestamp
import java.util.Objects

@Entity
@Table(name = "racks")
class RacksModelsLogs {
    @get:Id
    @get:Column(name = "id")
    var id: Int = 0
    @get:Basic
    @get:Column(name = "idEmpleado")
    var idEmpleado: Long? = null
    @get:Basic
    @get:Column(name = "idObservacionesInventario")
    var idObservacionesInventario: Int = 0
    @get:Basic
    @get:Column(name = "idEstadoInventario")
    var idEstadoInventario: Int = 0
    @get:Basic
    @get:Column(name = "Folio")
    var folio: String? = null
    @get:Basic
    @get:Column(name = "idMarca")
    var idMarca: Int = 0
    @get:Basic
    @get:Column(name = "idSede")
    var idSede: Int = 0
    @get:Basic
    @get:Column(name = "origenRecurso")
    var origenRecurso: String? = null
    @get:Basic
    @get:Column(name = "activo")
    var activo: Int = 0
    @get:Basic
    @get:Column(name = "fechaRecepcion")
    var fechaRecepcion: Timestamp? = null
    @get:Basic
    @get:Column(name = "fechaSalida")
    var fechaSalida: Timestamp? = null
    @get:Basic
    @get:Column(name = "idLogs")
    var idLogs: Long = 0

}
