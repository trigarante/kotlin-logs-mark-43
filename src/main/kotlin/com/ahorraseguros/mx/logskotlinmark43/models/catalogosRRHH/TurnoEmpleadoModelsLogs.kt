package com.ahorraseguros.mx.logskotlinmark43.models.catalogosRRHH

import javax.persistence.*
import java.util.Objects

@Entity
@Table(name = "turnoEmpleado" )
class TurnoEmpleadoModelsLogs {
    @get:Id
    @get:Column(name = "id")
    var id: Int = 0
    @get:Basic
    @get:Column(name = "turno")
    var turno: String? = null
    @get:Basic
    @get:Column(name = "horario")
    var horario: String? = null
    @get:Basic
    @get:Column(name = "activo")
    var activo: Int = 0
    @get:Basic
    @get:Column(name = "idLog")
    var idLog: Long = 0

}
