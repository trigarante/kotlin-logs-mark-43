package com.ahorraseguros.mx.logskotlinmark43.models.catalogosComercial

import javax.persistence.*
import java.util.Objects

@Entity
@Table(name = "tipoCobertura")
class TipoCoberturaModelsLogs {
    @get:Id
    @get:Column(name = "id")
    var id: Int = 0
    @get:Basic
    @get:Column(name = "cobertura")
    var cobertura: String? = null
    @get:Basic
    @get:Column(name = "activo")
    var activo: Int = 0
    @get:Basic
    @get:Column(name = "idLog")
    var idLog: Long = 0

}
