package com.ahorraseguros.mx.logskotlinmark43.models.catalogosRRHH

import javax.persistence.*

@Entity
@Table(name = "bancos")
data class BancosModelsLogs (
    @get:Id
    @get:Column(name = "id")
    var id: Int = 0,
    @get:Basic
    @get:Column(name = "nombre")
    var nombre: String? = null,
    @get:Basic
    @get:Column(name = "activo")
    var activo: Int = 0,
    @get:Basic
    @get:Column(name = "idLogs")
    var idLogs: Long = 0
)
