package com.ahorraseguros.mx.logskotlinmark43.models.ventaNueva

import javax.persistence.*
import java.sql.Date
import java.sql.Timestamp
import java.util.Objects

@Entity
@Table(name = "registro")
class RegistroModelsLogs {
    @get:Id
    @get:Column(name = "id")
    var id: Long = 0
    @get:Basic
    @get:Column(name = "idEmpleado")
    var idEmpleado: Long = 0
    @get:Basic
    @get:Column(name = "idProducto")
    var idProducto: Long = 0
    @get:Basic
    @get:Column(name = "idTipoPago")
    var idTipoPago: Int = 0
    @get:Basic
    @get:Column(name = "idEstadoPoliza")
    var idEstadoPoliza: Int = 0
    @get:Basic
    @get:Column(name = "idProductoSocio")
    var idProductoSocio: Long = 0
    @get:Basic
    @get:Column(name = "idFlujoPoliza")
    var idFlujoPoliza: Int = 0
    @get:Basic
    @get:Column(name = "poliza")
    var poliza: String? = null
    @get:Basic
    @get:Column(name = "fechaInicio")
    var fechaInicio: Date? = null
    @get:Basic
    @get:Column(name = "primaNeta")
    var primaNeta: Double = 0.toDouble()
    @get:Basic
    @get:Column(name = "fechaRegistro")
    var fechaRegistro: Timestamp? = null
    @get:Basic
    @get:Column(name = "archivo")
    var archivo: String? = null
    @get:Basic
    @get:Column(name = "idLogs")
    var idLogs: Long = 0

}
