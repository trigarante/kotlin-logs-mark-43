package com.ahorraseguros.mx.logskotlinmark43.models.comercial

import javax.persistence.*

@Entity
@Table(name = "presupuestoSocio", schema = "mark44Logs", catalog = "")
class PresupuestoSocioModelsLogs {
    @get:Id
    @get:Column(name = "id")
    var id: Int = 0
    @get:Basic
    @get:Column(name = "idSocio")
    var idSocio: Int = 0
    @get:Basic
    @get:Column(name = "presupuesto")
    var presupuesto: String? = null
    @get:Basic
    @get:Column(name = "activo")
    var activo: Byte = 0
    @get:Basic
    @get:Column(name = "anio")
    var anio: String? = null
    @get:Basic
    @get:Column(name = "idLog")
    var idLog: Long = 0

}
