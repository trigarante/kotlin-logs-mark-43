package com.ahorraseguros.mx.logskotlinmark43.models.catalogosComercial;

import javax.persistence.*;
import java.util.Objects;

@Entity
@Table(name = "tipoSubRamo", schema = "trigarante2020Logs", catalog = "")
public class TipoSubRamoModelsLogs {
    private int id;
    private String tipo;
    private byte activo;
    private long idLog;

    @Id
    @Column(name = "id")
    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    @Basic
    @Column(name = "tipo")
    public String getTipo() {
        return tipo;
    }

    public void setTipo(String tipo) {
        this.tipo = tipo;
    }

    @Basic
    @Column(name = "activo")
    public byte getActivo() {
        return activo;
    }

    public void setActivo(byte activo) {
        this.activo = activo;
    }

    @Basic
    @Column(name = "idLog")
    public long getIdLog() {
        return idLog;
    }

    public void setIdLog(long idLog) {
        this.idLog = idLog;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        TipoSubRamoModelsLogs that = (TipoSubRamoModelsLogs) o;
        return id == that.id &&
                activo == that.activo &&
                idLog == that.idLog &&
                Objects.equals(tipo, that.tipo);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, tipo, activo, idLog);
    }
}
