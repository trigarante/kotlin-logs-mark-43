package com.ahorraseguros.mx.logskotlinmark43.models.ventaNueva

import javax.persistence.*
import java.sql.Date
import java.util.Objects

@Entity
@Table(name = "pagos")
class PagosModelsLogs {
    @get:Id
    @get:Column(name = "id")
    var id: Long = 0
    @get:Basic
    @get:Column(name = "idRecibo")
    var idRecibo: Long? = null
    @get:Basic
    @get:Column(name = "idFormaPago")
    var idFormaPago: Int? = null
    @get:Basic
    @get:Column(name = "idEstadoPago")
    var idEstadoPago: Int? = null
    @get:Basic
    @get:Column(name = "fechaPago")
    var fechaPago: Date? = null
    @get:Basic
    @get:Column(name = "cantidad")
    var cantidad: Double? = null
    @get:Basic
    @get:Column(name = "archivo")
    var archivo: String? = null
    @get:Basic
    @get:Column(name = "datos")
    var datos: String? = null
    @get:Basic
    @get:Column(name = "idLogs")
    var idLogs: Long = 0

}
