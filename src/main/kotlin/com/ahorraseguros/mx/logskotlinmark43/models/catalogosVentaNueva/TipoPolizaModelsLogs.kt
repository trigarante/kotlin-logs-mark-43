package com.ahorraseguros.mx.logskotlinmark43.models.catalogosVentaNueva

import javax.persistence.*
import java.util.Objects

@Entity
@Table(name = "tipoPoliza")
class TipoPolizaModelsLogs {
    @get:Id
    @get:Column(name = "id")
    var id: Int = 0
    @get:Basic
    @get:Column(name = "descripcion")
    var descripcion: String? = null
    @get:Basic
    @get:Column(name = "activo")
    var activo: Int = 0
    @get:Basic
    @get:Column(name = "idLogs")
    var idLogs: Long = 0

}
