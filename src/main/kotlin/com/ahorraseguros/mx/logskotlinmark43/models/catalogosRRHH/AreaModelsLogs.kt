package com.ahorraseguros.mx.logskotlinmark43.models.catalogosRRHH

import javax.persistence.*

@Entity
@Table(name = "area")
data class AreaModelsLogs (
    @get:Id
    @get:Column(name = "id")
    var id: Int = 0,
    @get:Basic
    @get:Column(name = "idSede")
    var idSede: Int = 0,
    @get:Basic
    @get:Column(name = "nombre")
    var nombre: String? = null,
    @get:Basic
    @get:Column(name = "descripcion")
    var descripcion: String? = null,
    @get:Basic
    @get:Column(name = "activo")
    var activo: Int = 0,
    @get:Basic
    @get:Column(name = "codigo")
    var codigo: String? = null,
    @get:Basic
    @get:Column(name = "idLog")
    var idLog: Long = 0
)
