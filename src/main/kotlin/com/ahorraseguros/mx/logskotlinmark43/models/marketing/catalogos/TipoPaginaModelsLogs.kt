package com.ahorraseguros.mx.logskotlinmark43.models.marketing.catalogos

import javax.persistence.*
import java.util.Objects

@Entity
@Table(name = "tipoPagina")
class TipoPaginaModelsLogs {
    @get:Id
    @get:Column(name = "id")
    var id: Int = 0
    @get:Basic
    @get:Column(name = "tipo")
    var tipo: String? = null
    @get:Basic
    @get:Column(name = "objetivo")
    var objetivo: String? = null
    @get:Basic
    @get:Column(name = "activo")
    var activo: Int = 0
    @get:Basic
    @get:Column(name = "idLog")
    var idLog: Long = 0
}
