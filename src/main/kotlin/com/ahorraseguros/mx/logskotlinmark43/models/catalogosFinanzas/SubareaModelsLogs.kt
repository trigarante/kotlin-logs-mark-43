package com.ahorraseguros.mx.logskotlinmark43.models.catalogosFinanzas

import javax.persistence.*
import java.util.Objects

@Entity
@Table(name = "subarea")
class SubareaModelsLogs {
    @get:Id
    @get:Column(name = "id")
    var id: Int = 0
    @get:Basic
    @get:Column(name = "idArea")
    var idArea: Int = 0
    @get:Basic
    @get:Column(name = "subarea")
    var subarea: String? = null
    @get:Basic
    @get:Column(name = "descripcion")
    var descripcion: String? = null
    @get:Basic
    @get:Column(name = "activo")
    var activo: Byte = 0
    @get:Basic
    @get:Column(name = "codigo")
    var codigo: String? = null
    @get:Basic
    @get:Column(name = "idLogs")
    var idLogs: Long = 0
}
