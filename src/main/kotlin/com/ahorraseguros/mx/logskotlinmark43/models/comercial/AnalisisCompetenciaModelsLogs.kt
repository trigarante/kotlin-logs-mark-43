package com.ahorraseguros.mx.logskotlinmark43.models.comercial

import javax.persistence.*
import java.util.Objects

@Entity
@Table(name = "analisisCompetencia")
class AnalisisCompetenciaModelsLogs {
    @get:Id
    @get:Column(name = "id")
    var id: Long = 0
    @get:Basic
    @get:Column(name = "idCompetencia")
    var idCompetencia: Int = 0
    @get:Basic
    @get:Column(name = "precios")
    var precios: Double = 0.toDouble()
    @get:Basic
    @get:Column(name = "participacion")
    var participacion: Double = 0.toDouble()
    @get:Basic
    @get:Column(name = "activo")
    var activo: Int = 0
    @get:Basic
    @get:Column(name = "idLog")
    var idLog: Long = 0

}
