package com.ahorraseguros.mx.logskotlinmark43.models.catalogosComercial

import javax.persistence.*
import java.util.Objects

@Entity
@Table(name = "estadoSocio", schema = "mark44Logs", catalog = "")
class EstadoSocioModelsLogs {
    @get:Id
    @get:Column(name = "id")
    var id: Int = 0
    @get:Basic
    @get:Column(name = "idEstado")
    var idEstado: Byte = 0
    @get:Basic
    @get:Column(name = "estado")
    var estado: String? = null
    @get:Basic
    @get:Column(name = "activo")
    var activo: Byte = 0
    @get:Basic
    @get:Column(name = "idLog")
    var idLog: Long = 0


}
