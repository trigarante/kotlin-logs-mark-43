package com.ahorraseguros.mx.logskotlinmark43.models.ventaNueva

import javax.persistence.*
import java.util.Objects

@Entity
@Table(name = "productoCliente")
class ProductoClienteModelsLogs {
    @get:Id
    @get:Column(name = "id")
    var id: Long = 0
    @get:Basic
    @get:Column(name = "idCliente")
    var idCliente: Long = 0
    @get:Basic
    @get:Column(name = "idSolictud")
    var idSolictud: Long = 0
    @get:Basic
    @get:Column(name = "idSubRamo")
    var idSubRamo: Int = 0
    @get:Basic
    @get:Column(name = "datos")
    var datos: String? = null
    @get:Basic
    @get:Column(name = "idLogs")
    var idLogs: Long = 0

}
