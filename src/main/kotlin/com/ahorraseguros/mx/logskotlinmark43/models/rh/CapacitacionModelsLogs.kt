package com.ahorraseguros.mx.logskotlinmark43.models.rh

import javax.persistence.*
import java.sql.Timestamp

@Entity
@Table(name = "capacitacion")
data class CapacitacionModelsLogs (
    @get:Id
    @get:Column(name = "id")
    var id: Int = 0,
    @get:Basic
    @get:Column(name = "idCandidato")
    var idCandidato: Long = 0,
    @get:Basic
    @get:Column(name = "calificacion")
    var calificacion: Double = 0.toDouble(),
    @get:Basic
    @get:Column(name = "idCompetenciaCandidato")
    var idCompetenciaCandidato: Int = 0,
    @get:Basic
    @get:Column(name = "idCalificacionCompetencia")
    var idCalificacionCompetencia: Int = 0,
    @get:Basic
    @get:Column(name = "comentarios")
    var comentarios: String? = null,
    @get:Basic
    @get:Column(name = "idCapacitador")
    var idCapacitador: Long = 0,
    @get:Basic
    @get:Column(name = "asistencia")
    var asistencia: String? = null,
    @get:Basic
    @get:Column(name = "fechaIngreso")
    var fechaIngreso: Timestamp? = null,
    @get:Basic
    @get:Column(name = "fechaRegistro")
    var fechaRegistro: Timestamp? = null,
    @get:Basic
    @get:Column(name = "idLog")
    var idLog: Int = 0
)
