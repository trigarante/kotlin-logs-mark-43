package com.ahorraseguros.mx.logskotlinmark43.models.ti.catalogosSoporte

import javax.persistence.*

@Entity
@Table(name = "marcaSoporte")
class MarcaSoporteModelsLogs {
    @get:Id
    @get:Column(name = "id")
    var id: Int = 0
    @get:Basic
    @get:Column(name = "nombreMarca")
    var nombreMarca: String? = null
    @get:Basic
    @get:Column(name = "activo")
    var activo: Int = 0
    @get:Basic
    @get:Column(name = "idLogs")
    var idLogs: Long = 0


}
