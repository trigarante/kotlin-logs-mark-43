package com.ahorraseguros.mx.logskotlinmark43.models.atencionaClientes

import javax.persistence.*
import java.sql.Timestamp
import java.util.Objects

@Entity
@Table(name = "endososCancelaciones")
class EndososCancelacionesModelsLogs {
    @get:Id
    @get:Column(name = "id")
    var id: Long = 0
    @get:Basic
    @get:Column(name = "idSolicitudEndoso")
    var idSolicitudEndoso: Long = 0
    @get:Basic
    @get:Column(name = "idTipoEndoso")
    var idTipoEndoso: Int = 0
    @get:Basic
    @get:Column(name = "idEstadoEndoso")
    var idEstadoEndoso: Int = 0
    @get:Basic
    @get:Column(name = "fechaEmision")
    var fechaEmision: Timestamp? = null
    @get:Basic
    @get:Column(name = "fechaAplicacion")
    var fechaAplicacion: Timestamp? = null
    @get:Basic
    @get:Column(name = "prima")
    var prima: Double = 0.toDouble()
    @get:Basic
    @get:Column(name = "comentarios")
    var comentarios: String? = null
    @get:Basic
    @get:Column(name = "activo")
    var activo: Byte = 0
    @get:Basic
    @get:Column(name = "idLogs")
    var idLogs: Long = 0

}
