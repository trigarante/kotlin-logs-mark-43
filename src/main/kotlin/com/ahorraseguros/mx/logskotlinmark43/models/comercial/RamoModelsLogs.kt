package com.ahorraseguros.mx.logskotlinmark43.models.comercial

import javax.persistence.*
import java.util.Objects

@Entity
@Table(name = "ramo")
class RamoModelsLogs {
    @get:Id
    @get:Column(name = "id")
    var id: Int = 0
    @get:Basic
    @get:Column(name = "idSocio")
    var idSocio: Int = 0
    @get:Basic
    @get:Column(name = "idTipoRamo")
    var idTipoRamo: Int = 0
    @get:Basic
    @get:Column(name = "descripcion")
    var descripcion: String? = null
    @get:Basic
    @get:Column(name = "prioridad")
    var prioridad: Int = 0
    @get:Basic
    @get:Column(name = "activo")
    var activo: Int = 0
    @get:Basic
    @get:Column(name = "idLog")
    var idLog: Long = 0

}
