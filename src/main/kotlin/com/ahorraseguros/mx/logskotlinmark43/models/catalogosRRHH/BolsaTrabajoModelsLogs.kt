package com.ahorraseguros.mx.logskotlinmark43.models.catalogosRRHH

import javax.persistence.*

@Entity
@Table(name = "bolsaTrabajo")
data class BolsaTrabajoModelsLogs (
    @get:Id
    @get:Column(name = "id")
    var id: Int = 0,
    @get:Basic
    @get:Column(name = "nombre")
    var nombre: String? = null,
    @get:Basic
    @get:Column(name = "tipo")
    var tipo: String? = null,
    @get:Basic
    @get:Column(name = "activo")
    var activo: Int = 0,
    @get:Basic
    @get:Column(name = "idLog")
    var idLog: Long? = null
)
