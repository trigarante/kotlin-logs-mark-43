package com.ahorraseguros.mx.logskotlinmark43.models.ventaNueva

import javax.persistence.*
import java.util.Objects

@Entity
@Table(name = "productoSolicitud")
class ProductoSolicitudModelsLogs {
    @get:Id
    @get:Column(name = "id")
    var id: Long = 0
    @get:Basic
    @get:Column(name = "idProspecto")
    var idProspecto: Long = 0
    @get:Basic
    @get:Column(name = "idTipoCategoria")
    var idTipoCategoria: Int = 0
    @get:Basic
    @get:Column(name = "datos")
    var datos: String? = null
    @get:Basic
    @get:Column(name = "idLogs")
    var idLogs: Long = 0

}
