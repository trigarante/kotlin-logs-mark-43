package com.ahorraseguros.mx.logskotlinmark43.models.marketing

import javax.persistence.*

@Entity
@Table(name = "campanaSubarea")
class CampanaSubareaModelsLogs {
    @get:Id
    @get:Column(name = "id")
    var id: Int = 0
    @get:Basic
    @get:Column(name = "idCampana")
    var idCampana: Long = 0
    @get:Basic
    @get:Column(name = "idSubarea")
    var idSubarea: Int = 0
    @get:Basic
    @get:Column(name = "descripcionn")
    var descripcionn: String? = null
    @get:Basic
    @get:Column(name = "activo")
    var activo: Int = 0
    @get:Basic
    @get:Column(name = "idLog")
    var idLog: Long = 0
}
