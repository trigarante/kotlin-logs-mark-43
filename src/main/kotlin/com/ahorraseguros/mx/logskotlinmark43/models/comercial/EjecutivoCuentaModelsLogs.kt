package com.ahorraseguros.mx.logskotlinmark43.models.comercial

import javax.persistence.*


@Entity
@Table(name = "ejecutivoCuenta")
class EjecutivoCuentaModelsLogs {
    @get:Id
    @get:Column(name = "id")
    var id: Int = 0
    @get:Basic
    @get:Column(name = "idSocio")
    var idSocio: Int = 0
    @get:Basic
    @get:Column(name = "nombre")
    var nombre: String? = null
    @get:Basic
    @get:Column(name = "apellidoPaterno")
    var apellidoPaterno: String? = null
    @get:Basic
    @get:Column(name = "apellidoMaterno")
    var apellidoMaterno: String? = null
    @get:Basic
    @get:Column(name = "telefono")
    var telefono: String? = null
    @get:Basic
    @get:Column(name = "ext")
    var ext: String? = null
    @get:Basic
    @get:Column(name = "celular")
    var celular: String? = null
    @get:Basic
    @get:Column(name = "activo")
    var activo: Int = 0
    @get:Basic
    @get:Column(name = "favorito")
    var favorito: Int? = null
    @get:Basic
    @get:Column(name = "email")
    var email: String? = null
    @get:Basic
    @get:Column(name = "puesto")
    var puesto: String? = null
    @get:Basic
    @get:Column(name = "direccion")
    var direccion: String? = null
    @get:Basic
    @get:Column(name = "idLog")
    var idLog: Long = 0


}
