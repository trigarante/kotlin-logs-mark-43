package com.ahorraseguros.mx.logskotlinmark43.models.ventaNueva

import javax.persistence.*
import java.sql.Timestamp
import java.util.Objects

@Entity
@Table(name = "solicitudes")
class SolicitudesModelsLogs {
    @get:Id
    @get:Column(name = "id")
    var id: Long = 0
    @get:Basic
    @get:Column(name = "idCotizacionAli")
    var idCotizacionAli: Long = 0
    @get:Basic
    @get:Column(name = "idEmpleado")
    var idEmpleado: Long = 0
    @get:Basic
    @get:Column(name = "idEstadoSolicitud")
    var idEstadoSolicitud: Int = 0
    @get:Basic
    @get:Column(name = "idEtiquetaSolicitud")
    var idEtiquetaSolicitud: Long = 0
    @get:Basic
    @get:Column(name = "idFlujoSolicitud")
    var idFlujoSolicitud: Long = 0
    @get:Basic
    @get:Column(name = "comentarios")
    var comentarios: String? = null
    @get:Basic
    @get:Column(name = "idLogs")
    var idLogs: Long = 0

}
