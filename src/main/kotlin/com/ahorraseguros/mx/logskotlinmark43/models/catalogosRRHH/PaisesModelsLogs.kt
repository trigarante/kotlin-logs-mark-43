package com.ahorraseguros.mx.logskotlinmark43.models.catalogosRRHH

import javax.persistence.*
import java.util.Objects

@Entity
@Table(name = "paises")
data class PaisesModelsLogs (
    @get:Id
    @get:Column(name = "id")
    var id: Int = 0,
    @get:Basic
    @get:Column(name = "nombre")
    var nombre: String? = null,
    @get:Basic
    @get:Column(name = "activo")
    var activo: Int = 0,
    @get:Basic
    @get:Column(name = "idLog")
    var idLog: Long = 0
)
