package com.ahorraseguros.mx.logskotlinmark43.models.catalogosFinanzas

import javax.persistence.*
import java.util.Objects

@Entity
@Table(name = "sede")
class SedeModelsLogs {
    @get:Id
    @get:Column(name = "id")
    var id: Int = 0
    @get:Basic
    @get:Column(name = "idEmpresa")
    var idEmpresa: Int = 0
    @get:Basic
    @get:Column(name = "nombre")
    var nombre: String? = null
    @get:Basic
    @get:Column(name = "cp")
    var cp: String? = null
    @get:Basic
    @get:Column(name = "colonia")
    var colonia: String? = null
    @get:Basic
    @get:Column(name = "calle")
    var calle: String? = null
    @get:Basic
    @get:Column(name = "numero")
    var numero: String? = null
    @get:Basic
    @get:Column(name = "idPais")
    var idPais: Int = 0
    @get:Basic
    @get:Column(name = "codigo")
    var codigo: String? = null
    @get:Basic
    @get:Column(name = "idLogs")
    var idLogs: Long = 0
}
