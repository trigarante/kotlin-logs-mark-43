package com.ahorraseguros.mx.logskotlinmark43.models.marketing

import javax.persistence.*
import java.util.Objects

@Entity
@Table(name = "campanaCategoria")
class CampanaCategoriaModelsLogs {
    @get:Id
    @get:Column(name = "id")
    var id: Int = 0
    @get:Basic
    @get:Column(name = "idCampana")
    var idCampana: Long = 0
    @get:Basic
    @get:Column(name = "idCategoria")
    var idCategoria: Int = 0
    @get:Basic
    @get:Column(name = "activo")
    var activo: Int = 0
    @get:Basic
    @get:Column(name = "comentarios")
    var comentarios: String? = null
    @get:Basic
    @get:Column(name = "idLog")
    var idLog: Long = 0
}
