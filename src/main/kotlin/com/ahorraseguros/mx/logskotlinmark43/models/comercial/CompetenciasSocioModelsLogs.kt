package com.ahorraseguros.mx.logskotlinmark43.models.comercial

import javax.persistence.*
import java.util.Objects

@Entity
@Table(name = "competenciaSocio")
class CompetenciasSocioModelsLogs {
    @get:Id
    @get:Column(name = "id")
    var id: Int = 0
    @get:Basic
    @get:Column(name = "idProducto")
    var idProducto: Int = 0
    @get:Basic
    @get:Column(name = "idTipoCompetencia")
    var idTipoCompetencia: Int = 0
    @get:Basic
    @get:Column(name = "competencia")
    var competencia: String? = null
    @get:Basic
    @get:Column(name = "activo")
    var activo: Int = 0
    @get:Basic
    @get:Column(name = "idLog")
    var idLog: Long = 0
}
