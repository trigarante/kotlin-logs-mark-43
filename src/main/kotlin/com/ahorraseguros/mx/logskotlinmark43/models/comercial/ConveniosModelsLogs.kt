package com.ahorraseguros.mx.logskotlinmark43.models.comercial

import javax.persistence.*
import java.util.Objects

@Entity
@Table(name = "convenios")
class ConveniosModelsLogs {
    @get:Id
    @get:Column(name = "id")
    var id: Int = 0
    @get:Basic
    @get:Column(name = "idSubCategoria")
    var idSubCategoria: Int = 0
    @get:Basic
    @get:Column(name = "idSubRamo")
    var idSubRamo: Int = 0
    @get:Basic
    @get:Column(name = "idTipoDivisa")
    var idTipoDivisa: Int = 0
    @get:Basic
    @get:Column(name = "cantidad")
    var cantidad: Double = 0.toDouble()
    @get:Basic
    @get:Column(name = "activo")
    var activo: Int = 0
    @get:Basic
    @get:Column(name = "idLog")
    var idLog: Long = 0

}
