package com.ahorraseguros.mx.logskotlinmark43

import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.boot.runApplication

@SpringBootApplication
class Logskotlinmark43Application

fun main(args: Array<String>) {
    runApplication<Logskotlinmark43Application>(*args)
}
