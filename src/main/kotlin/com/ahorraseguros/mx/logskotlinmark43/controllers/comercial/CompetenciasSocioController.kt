package com.ahorraseguros.mx.logskotlinmark43.controllers.comercial


import com.ahorraseguros.mx.logskotlinmark43.models.comercial.CompetenciasSocioModelsLogs
import com.ahorraseguros.mx.logskotlinmark43.repositories.comercial.CompetenciasSocioRepository
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.web.bind.annotation.*
import javax.validation.Valid

@CrossOrigin
@RestController
@RequestMapping("/v1/competenciasSocio")
class CompetenciasSocioController {

    @Autowired
    lateinit var competenciasSocioRepository: CompetenciasSocioRepository

    @GetMapping
    fun findAll():MutableList<CompetenciasSocioModelsLogs> = competenciasSocioRepository.findAll()

    @PostMapping
    fun create(@Valid @RequestBody nuevoLog: CompetenciasSocioModelsLogs): CompetenciasSocioModelsLogs = competenciasSocioRepository.save(nuevoLog)
}