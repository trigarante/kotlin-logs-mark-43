package com.ahorraseguros.mx.logskotlinmark43.controllers.ventaNueva

import com.ahorraseguros.mx.logskotlinmark43.models.ventaNueva.ProductoSolicitudModelsLogs
import com.ahorraseguros.mx.logskotlinmark43.repositories.ventaNueva.ProductoSolicitudRepository
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.web.bind.annotation.*
import javax.validation.Valid

@CrossOrigin
@RestController
@RequestMapping("/v1/productoSolicitud")
class ProductoSolicitudController {
    @Autowired
    lateinit var productoSolicitudRepository: ProductoSolicitudRepository

    @GetMapping
    fun findAll():MutableList<ProductoSolicitudModelsLogs> = productoSolicitudRepository.findAll()

    @PostMapping
    fun create(@Valid @RequestBody nuevoLog: ProductoSolicitudModelsLogs): ProductoSolicitudModelsLogs = productoSolicitudRepository.save(nuevoLog)
}