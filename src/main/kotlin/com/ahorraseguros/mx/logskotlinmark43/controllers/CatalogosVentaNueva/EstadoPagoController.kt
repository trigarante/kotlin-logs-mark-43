package com.ahorraseguros.mx.logskotlinmark43.controllers.CatalogosVentaNueva

import com.ahorraseguros.mx.logskotlinmark43.models.catalogosVentaNueva.EstadoPagoModelLogs
import com.ahorraseguros.mx.logskotlinmark43.repositories.catalogosVentaNueva.EstadoPagoRepository
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.web.bind.annotation.*
import javax.validation.Valid

@CrossOrigin
@RestController
@RequestMapping("/v1/estadoPago")
class EstadoPagoController {
    @Autowired
    lateinit var estadoPagoRepository: EstadoPagoRepository

    @GetMapping
    fun findAll():MutableList<EstadoPagoModelLogs> = estadoPagoRepository.findAll()

    @PostMapping
    fun create(@Valid @RequestBody nuevoLog: EstadoPagoModelLogs): EstadoPagoModelLogs = estadoPagoRepository.save(nuevoLog)
}