package com.ahorraseguros.mx.logskotlinmark43.controllers.ti.catalogoSoporte


import com.ahorraseguros.mx.logskotlinmark43.models.ti.catalogosSoporte.MarcaSoporteModelsLogs
import com.ahorraseguros.mx.logskotlinmark43.repositories.ti.catalogoSoporte.MarcaSoporteRepository
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.web.bind.annotation.*
import javax.validation.Valid

@CrossOrigin
@RestController
@RequestMapping("/v1/marcaSoporte")
class MarcaSoporteController  {
    @Autowired
    lateinit var marcaSoporteRepository: MarcaSoporteRepository

    @GetMapping
    fun findAll():MutableList<MarcaSoporteModelsLogs> = marcaSoporteRepository.findAll()

    @PostMapping
    fun create(@Valid @RequestBody nuevoLog: MarcaSoporteModelsLogs): MarcaSoporteModelsLogs = marcaSoporteRepository.save(nuevoLog)
}