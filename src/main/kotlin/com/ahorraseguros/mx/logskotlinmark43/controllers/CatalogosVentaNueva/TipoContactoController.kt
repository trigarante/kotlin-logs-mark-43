package com.ahorraseguros.mx.logskotlinmark43.controllers.CatalogosVentaNueva

import com.ahorraseguros.mx.logskotlinmark43.models.catalogosVentaNueva.TipoContactoModelsLogs
import com.ahorraseguros.mx.logskotlinmark43.repositories.catalogosVentaNueva.TipoContactoRepository
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.web.bind.annotation.*
import javax.validation.Valid

@CrossOrigin
@RestController
@RequestMapping("/v1/tipoContacto")
class TipoContactoController {
    @Autowired
    lateinit var tipoContactoRepository: TipoContactoRepository

    @GetMapping
    fun findAll():MutableList<TipoContactoModelsLogs> = tipoContactoRepository.findAll()

    @PostMapping
    fun create(@Valid @RequestBody nuevoLog: TipoContactoModelsLogs): TipoContactoModelsLogs = tipoContactoRepository.save(nuevoLog)
}