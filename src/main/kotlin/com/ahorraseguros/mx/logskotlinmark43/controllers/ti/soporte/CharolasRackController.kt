package com.ahorraseguros.mx.logskotlinmark43.controllers.ti.soporte



import com.ahorraseguros.mx.logskotlinmark43.models.ti.soporte.CharolasRackModelsLogs
import com.ahorraseguros.mx.logskotlinmark43.repositories.ti.soporte.CharolasRackRepository
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.web.bind.annotation.*
import javax.validation.Valid

@CrossOrigin
@RestController
@RequestMapping("/v1/charolasRack")
class CharolasRackController {
    @Autowired
    lateinit var charolasRackRepository: CharolasRackRepository

    @GetMapping
    fun findAll():MutableList<CharolasRackModelsLogs> = charolasRackRepository.findAll()

    @PostMapping
    fun create(@Valid @RequestBody nuevoLog: CharolasRackModelsLogs): CharolasRackModelsLogs = charolasRackRepository.save(nuevoLog)
}