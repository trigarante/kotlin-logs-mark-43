package com.ahorraseguros.mx.logskotlinmark43.controllers.ventaNueva

import com.ahorraseguros.mx.logskotlinmark43.models.ventaNueva.CotizacionesAliModelsLogs
import com.ahorraseguros.mx.logskotlinmark43.repositories.ventaNueva.CotizacionesAliRepository
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.web.bind.annotation.*
import javax.validation.Valid

@CrossOrigin
@RestController
@RequestMapping("/v1/cotizacionesAli")
class CotizacionAliController {
    @Autowired
    lateinit var cotizacionesAliRepository: CotizacionesAliRepository

    @GetMapping
    fun findAll():MutableList<CotizacionesAliModelsLogs> = cotizacionesAliRepository.findAll()

    @PostMapping
    fun create(@Valid @RequestBody nuevoLog: CotizacionesAliModelsLogs): CotizacionesAliModelsLogs = cotizacionesAliRepository.save(nuevoLog)
}