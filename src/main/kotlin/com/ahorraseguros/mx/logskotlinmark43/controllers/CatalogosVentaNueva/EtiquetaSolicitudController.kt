package com.ahorraseguros.mx.logskotlinmark43.controllers.CatalogosVentaNueva


import com.ahorraseguros.mx.logskotlinmark43.models.catalogosVentaNueva.EtiquetaSolicitudModelsLogs
import com.ahorraseguros.mx.logskotlinmark43.repositories.catalogosVentaNueva.EtiquetaSolicitudRepository
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.web.bind.annotation.*
import javax.validation.Valid

@CrossOrigin
@RestController
@RequestMapping("/v1/etiquetaSolicitud")
class EtiquetaSolicitudController {
    @Autowired
    lateinit var etiquetaSolicitudRepository: EtiquetaSolicitudRepository

    @GetMapping
    fun findAll():MutableList<EtiquetaSolicitudModelsLogs> = etiquetaSolicitudRepository.findAll()

    @PostMapping
    fun create(@Valid @RequestBody nuevoLog: EtiquetaSolicitudModelsLogs): EtiquetaSolicitudModelsLogs = etiquetaSolicitudRepository.save(nuevoLog)
}