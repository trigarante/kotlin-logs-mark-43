package com.ahorraseguros.mx.logskotlinmark43.controllers.catalogosRRHH

import com.ahorraseguros.mx.logskotlinmark43.models.catalogosRRHH.TurnoEmpleadoModelsLogs
import com.ahorraseguros.mx.logskotlinmark43.repositories.catalogosRRHH.TurnoEmpleadoRepository
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.web.bind.annotation.*
import javax.validation.Valid

@CrossOrigin
@RestController
@RequestMapping("/v1/turnoEmpleado")
class TurnoEmpleadoController {

    @Autowired
    lateinit var turnoEmpleadoRepository: TurnoEmpleadoRepository

    @GetMapping
    fun findAll():MutableList<TurnoEmpleadoModelsLogs> = turnoEmpleadoRepository.findAll()

    @PostMapping
    fun create(@Valid @RequestBody nuevoLog: TurnoEmpleadoModelsLogs): TurnoEmpleadoModelsLogs = turnoEmpleadoRepository.save(nuevoLog)
}