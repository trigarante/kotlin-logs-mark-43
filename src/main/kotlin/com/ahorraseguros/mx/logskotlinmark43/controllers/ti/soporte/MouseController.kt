package com.ahorraseguros.mx.logskotlinmark43.controllers.ti.soporte

import com.ahorraseguros.mx.logskotlinmark43.models.ti.soporte.MouseModelsLogs
import com.ahorraseguros.mx.logskotlinmark43.repositories.ti.soporte.MouseRepository
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.web.bind.annotation.*
import javax.validation.Valid

@CrossOrigin
@RestController
@RequestMapping("/v1/mouse")
class MouseController {
    @Autowired
    lateinit var mouseRepository: MouseRepository

    @GetMapping
    fun findAll():MutableList<MouseModelsLogs> = mouseRepository.findAll()

    @PostMapping
    fun create(@Valid @RequestBody nuevoLog: MouseModelsLogs): MouseModelsLogs = mouseRepository.save(nuevoLog)
}