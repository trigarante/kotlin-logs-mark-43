package com.ahorraseguros.mx.logskotlinmark43.controllers.ventaNueva

import com.ahorraseguros.mx.logskotlinmark43.models.ventaNueva.ConductorHabitualModelsLogs
import com.ahorraseguros.mx.logskotlinmark43.repositories.ventaNueva.ConductorHabitualRepository
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.web.bind.annotation.*
import javax.validation.Valid

@CrossOrigin
@RestController
@RequestMapping("/v1/conductorHabitual")
class ConductorHabitualController {
    @Autowired
    lateinit var conductorHabitualRepository: ConductorHabitualRepository

    @GetMapping
    fun findAll():MutableList<ConductorHabitualModelsLogs> = conductorHabitualRepository.findAll()

    @PostMapping
    fun create(@Valid @RequestBody nuevoLog: ConductorHabitualModelsLogs): ConductorHabitualModelsLogs = conductorHabitualRepository.save(nuevoLog)
}