package com.ahorraseguros.mx.logskotlinmark43.controllers.rh

import com.ahorraseguros.mx.logskotlinmark43.models.rh.PrecandidatoModelsLogs
import com.ahorraseguros.mx.logskotlinmark43.repositories.rh.PrecandidatoRepository
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.web.bind.annotation.*
import javax.validation.Valid

@CrossOrigin
@RestController
@RequestMapping ("v1/precandidato")

class PrecandidatoController {

    @Autowired
    lateinit var precandidatoRepository: PrecandidatoRepository

    @GetMapping
    fun findAll(): MutableList<PrecandidatoModelsLogs> = precandidatoRepository.findAll()

    @PostMapping
    fun create(@Valid @RequestBody precandidatoModelsLogs: PrecandidatoModelsLogs): PrecandidatoModelsLogs {
        return precandidatoRepository.save(precandidatoModelsLogs)
    }

}

