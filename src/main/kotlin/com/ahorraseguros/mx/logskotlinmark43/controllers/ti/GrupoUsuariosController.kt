package com.ahorraseguros.mx.logskotlinmark43.controllers.ti

import com.ahorraseguros.mx.logskotlinmark43.models.ti.GrupoUsuariosModelsLogs
import com.ahorraseguros.mx.logskotlinmark43.repositories.ti.GrupoUsuariosRepository
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.web.bind.annotation.*
import javax.validation.Valid

@CrossOrigin
@RestController
@RequestMapping("/v1/grupos")
class GrupoUsuariosController {

    @Autowired
    lateinit var grupoUsuariosRepository: GrupoUsuariosRepository

    @GetMapping
    fun findAll():MutableList<GrupoUsuariosModelsLogs> = grupoUsuariosRepository.findAll()

    @PostMapping
    fun create(@Valid @RequestBody nuevoLog: GrupoUsuariosModelsLogs): GrupoUsuariosModelsLogs
            = grupoUsuariosRepository.save(nuevoLog)
}