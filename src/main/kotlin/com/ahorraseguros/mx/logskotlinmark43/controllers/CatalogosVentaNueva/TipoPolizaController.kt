package com.ahorraseguros.mx.logskotlinmark43.controllers.CatalogosVentaNueva

import com.ahorraseguros.mx.logskotlinmark43.models.catalogosVentaNueva.TipoPolizaModelsLogs
import com.ahorraseguros.mx.logskotlinmark43.repositories.catalogosVentaNueva.TipoPolizaRepository
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.web.bind.annotation.*
import javax.validation.Valid

@CrossOrigin
@RestController
@RequestMapping("/v1/tipoPoliza")
class TipoPolizaController {
    @Autowired
    lateinit var tipoPolizaRepository: TipoPolizaRepository

    @GetMapping
    fun findAll():MutableList<TipoPolizaModelsLogs> = tipoPolizaRepository.findAll()

    @PostMapping
    fun create(@Valid @RequestBody nuevoLog: TipoPolizaModelsLogs): TipoPolizaModelsLogs = tipoPolizaRepository.save(nuevoLog)
}