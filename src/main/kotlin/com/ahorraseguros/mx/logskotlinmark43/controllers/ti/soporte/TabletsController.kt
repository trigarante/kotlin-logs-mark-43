package com.ahorraseguros.mx.logskotlinmark43.controllers.ti.soporte


import com.ahorraseguros.mx.logskotlinmark43.models.ti.soporte.TabletsModelsLogs
import com.ahorraseguros.mx.logskotlinmark43.repositories.ti.soporte.TabletsRepository
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.web.bind.annotation.*
import javax.validation.Valid

@CrossOrigin
@RestController
@RequestMapping("/v1/tablets")
class TabletsController {
    @Autowired
    lateinit var tabletsRepository: TabletsRepository

    @GetMapping
    fun findAll():MutableList<TabletsModelsLogs> = tabletsRepository.findAll()

    @PostMapping
    fun create(@Valid @RequestBody nuevoLog: TabletsModelsLogs): TabletsModelsLogs = tabletsRepository.save(nuevoLog)
}