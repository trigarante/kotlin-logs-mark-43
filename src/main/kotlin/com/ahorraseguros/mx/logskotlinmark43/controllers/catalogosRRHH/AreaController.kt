package com.ahorraseguros.mx.logskotlinmark43.controllers.catalogosRRHH

import com.ahorraseguros.mx.logskotlinmark43.models.catalogosRRHH.AreaModelsLogs
import com.ahorraseguros.mx.logskotlinmark43.repositories.catalogosRRHH.AreaRepository
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.web.bind.annotation.*
import javax.validation.Valid

@CrossOrigin
@RestController
@RequestMapping("/v1/areas")
class AreaController {

    @Autowired
    lateinit var areaRepository: AreaRepository

    @GetMapping
    fun findAll():MutableList<AreaModelsLogs> = areaRepository.findAll()

    @PostMapping
    fun create(@Valid @RequestBody nuevoLog: AreaModelsLogs): AreaModelsLogs = areaRepository.save(nuevoLog)
}