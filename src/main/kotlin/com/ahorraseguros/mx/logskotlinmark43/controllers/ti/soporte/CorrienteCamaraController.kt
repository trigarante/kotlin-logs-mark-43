package com.ahorraseguros.mx.logskotlinmark43.controllers.ti.soporte

import com.ahorraseguros.mx.logskotlinmark43.models.ti.soporte.CorrienteCamaraModelsLogs
import com.ahorraseguros.mx.logskotlinmark43.repositories.ti.soporte.CorrienteCamaraRepository
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.web.bind.annotation.*
import javax.validation.Valid

@CrossOrigin
@RestController
@RequestMapping("/v1/corrienteCamara")
class CorrienteCamaraController {
    @Autowired
    lateinit var corrienteCamaraRepository: CorrienteCamaraRepository

    @GetMapping
    fun findAll():MutableList<CorrienteCamaraModelsLogs> = corrienteCamaraRepository.findAll()

    @PostMapping
    fun create(@Valid @RequestBody nuevoLog: CorrienteCamaraModelsLogs): CorrienteCamaraModelsLogs = corrienteCamaraRepository.save(nuevoLog)
}