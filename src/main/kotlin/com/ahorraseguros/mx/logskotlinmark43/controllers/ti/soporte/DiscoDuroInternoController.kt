package com.ahorraseguros.mx.logskotlinmark43.controllers.ti.soporte


import com.ahorraseguros.mx.logskotlinmark43.models.ti.soporte.DiscoDuroInternoModelsLogs
import com.ahorraseguros.mx.logskotlinmark43.repositories.ti.soporte.DiscoDuroInternoRepository
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.web.bind.annotation.*
import javax.validation.Valid

@CrossOrigin
@RestController
@RequestMapping("/v1/discoDuroInterno")
class DiscoDuroInternoController {
    @Autowired
    lateinit var discoDuroInternoRepository: DiscoDuroInternoRepository

    @GetMapping
    fun findAll():MutableList<DiscoDuroInternoModelsLogs> = discoDuroInternoRepository.findAll()

    @PostMapping
    fun create(@Valid @RequestBody nuevoLog: DiscoDuroInternoModelsLogs): DiscoDuroInternoModelsLogs = discoDuroInternoRepository.save(nuevoLog)
}