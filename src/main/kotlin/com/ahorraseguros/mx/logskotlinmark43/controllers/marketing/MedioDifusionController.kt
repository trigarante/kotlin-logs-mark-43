package com.ahorraseguros.mx.logskotlinmark43.controllers.marketing


import com.ahorraseguros.mx.logskotlinmark43.models.marketing.MedioDifusionModelsLogs
import com.ahorraseguros.mx.logskotlinmark43.repositories.marketing.MedioDifusionRepository
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.web.bind.annotation.*
import javax.validation.Valid

@CrossOrigin
@RestController
@RequestMapping("/v1/medioDifusion")
class MedioDifusionController {
    @Autowired
    lateinit var medioDifusionRepository: MedioDifusionRepository

    @GetMapping
    fun findAll():MutableList<MedioDifusionModelsLogs> = medioDifusionRepository.findAll()

    @PostMapping
    fun create(@Valid @RequestBody nuevoLog: MedioDifusionModelsLogs): MedioDifusionModelsLogs = medioDifusionRepository.save(nuevoLog)
}