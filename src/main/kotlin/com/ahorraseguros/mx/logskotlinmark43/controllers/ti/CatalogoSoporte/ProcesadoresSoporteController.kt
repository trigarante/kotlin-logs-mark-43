package com.ahorraseguros.mx.logskotlinmark43.controllers.ti.catalogoSoporte


import com.ahorraseguros.mx.logskotlinmark43.models.ti.catalogosSoporte.ProcesadoresSoporteModelsLogs
import com.ahorraseguros.mx.logskotlinmark43.repositories.ti.catalogoSoporte.ProcesadoresSoporteRepository
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.web.bind.annotation.*
import javax.validation.Valid

@CrossOrigin
@RestController
@RequestMapping("/v1/procesadores")
class ProcesadoresSoporteController {
    @Autowired
    lateinit var procesadoresSoporteRepository: ProcesadoresSoporteRepository

    @GetMapping
    fun findAll():MutableList<ProcesadoresSoporteModelsLogs> = procesadoresSoporteRepository.findAll()

    @PostMapping
    fun create(@Valid @RequestBody nuevoLog: ProcesadoresSoporteModelsLogs): ProcesadoresSoporteModelsLogs = procesadoresSoporteRepository.save(nuevoLog)
}