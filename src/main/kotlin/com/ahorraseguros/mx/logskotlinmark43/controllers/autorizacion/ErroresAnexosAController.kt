package com.ahorraseguros.mx.logskotlinmark43.controllers.autorizacion

import com.ahorraseguros.mx.logskotlinmark43.models.autorizacion.ErroresAnexosALogs
import com.ahorraseguros.mx.logskotlinmark43.repositories.autorizacion.ErroresAnexosARepository
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.web.bind.annotation.*
import javax.validation.Valid

@CrossOrigin
@RestController
@RequestMapping("/v1/erroresAnexosA")
class ErroresAnexosAController {
    @Autowired
    lateinit var erroresAnexosARepository: ErroresAnexosARepository

    @GetMapping
    fun findAll():MutableList<ErroresAnexosALogs> = erroresAnexosARepository.findAll()

    @PostMapping
    fun create(@Valid @RequestBody nuevoLog: ErroresAnexosALogs): ErroresAnexosALogs = erroresAnexosARepository.save(nuevoLog)
}