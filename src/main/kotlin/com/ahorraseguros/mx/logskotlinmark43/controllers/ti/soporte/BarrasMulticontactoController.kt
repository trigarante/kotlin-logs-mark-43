package com.ahorraseguros.mx.logskotlinmark43.controllers.ti.soporte


import com.ahorraseguros.mx.logskotlinmark43.models.ti.soporte.BarrasMulticontactoModelsLogs
import com.ahorraseguros.mx.logskotlinmark43.repositories.ti.soporte.BarrasMulticontactoRepository
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.web.bind.annotation.*
import javax.validation.Valid

@CrossOrigin
@RestController
@RequestMapping("/v1/barrasMulticontacto")
class BarrasMulticontactoController {
    @Autowired
    lateinit var barrasMulticontactoRepository: BarrasMulticontactoRepository

    @GetMapping
    fun findAll():MutableList<BarrasMulticontactoModelsLogs> = barrasMulticontactoRepository.findAll()

    @PostMapping
    fun create(@Valid @RequestBody nuevoLog: BarrasMulticontactoModelsLogs): BarrasMulticontactoModelsLogs = barrasMulticontactoRepository.save(nuevoLog)
}