package com.ahorraseguros.mx.logskotlinmark43.controllers.rh

import com.ahorraseguros.mx.logskotlinmark43.models.rh.SeguimientoModelsLogs
import com.ahorraseguros.mx.logskotlinmark43.repositories.rh.SeguimientoRepository
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.web.bind.annotation.*
import javax.validation.Valid

@CrossOrigin
@RestController
@RequestMapping("v1/seguimientos")

class SeguimientoController {
    @Autowired
    lateinit var seguimientoRepository: SeguimientoRepository

    @GetMapping
    fun findAll():MutableList<SeguimientoModelsLogs> = seguimientoRepository.findAll()

    @PostMapping
    fun create(@Valid @RequestBody seguimientoModelsLogs: SeguimientoModelsLogs): SeguimientoModelsLogs {
        return seguimientoRepository.save(seguimientoModelsLogs)
    }

}