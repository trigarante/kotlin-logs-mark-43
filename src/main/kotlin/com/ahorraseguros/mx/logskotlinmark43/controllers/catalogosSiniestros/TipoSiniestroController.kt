package com.ahorraseguros.mx.logskotlinmark43.controllers.catalogosSiniestros

import com.ahorraseguros.mx.logskotlinmark43.models.catalogosSiniestros.TipoSiniestroModelsLogs
import com.ahorraseguros.mx.logskotlinmark43.repositories.catalogosSiniestros.TipoSiniestroRepository
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.web.bind.annotation.*
import javax.validation.Valid

@CrossOrigin
@RestController
@RequestMapping("/v1/tipoSiniestro")
class TipoSiniestroController {
    @Autowired
    lateinit var tipoSiniestroRepository: TipoSiniestroRepository

    @GetMapping
    fun findAll():MutableList<TipoSiniestroModelsLogs> = tipoSiniestroRepository.findAll()

    @PostMapping
    fun create(@Valid @RequestBody nuevoLog: TipoSiniestroModelsLogs): TipoSiniestroModelsLogs = tipoSiniestroRepository.save(nuevoLog)
}
