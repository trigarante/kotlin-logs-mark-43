package com.ahorraseguros.mx.logskotlinmark43.controllers.ventaNueva

import com.ahorraseguros.mx.logskotlinmark43.models.ventaNueva.ProductoClienteModelsLogs
import com.ahorraseguros.mx.logskotlinmark43.repositories.ventaNueva.ProductoClienteRepository
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.web.bind.annotation.*
import javax.validation.Valid

@CrossOrigin
@RestController
@RequestMapping("/v1/productoCliente")
class ProductoClienteController {
    @Autowired
    lateinit var productoClienteRepository: ProductoClienteRepository

    @GetMapping
    fun findAll():MutableList<ProductoClienteModelsLogs> = productoClienteRepository.findAll()

    @PostMapping
    fun create(@Valid @RequestBody nuevoLog: ProductoClienteModelsLogs): ProductoClienteModelsLogs = productoClienteRepository.save(nuevoLog)
}