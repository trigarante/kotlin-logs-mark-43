package com.ahorraseguros.mx.logskotlinmark43.controllers.atencionaClientes

import com.ahorraseguros.mx.logskotlinmark43.models.atencionaClientes.EndososCancelacionesModelsLogs
import com.ahorraseguros.mx.logskotlinmark43.repositories.atencionaClientes.EndososCancelacionesRepository
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.web.bind.annotation.*
import javax.validation.Valid

@CrossOrigin
@RestController
@RequestMapping("/v1/endososCancelaciones")
class EndososCancelacionesController {
    @Autowired
    lateinit var endososCancelacionesRepository: EndososCancelacionesRepository

    @GetMapping
    fun findAll():MutableList<EndososCancelacionesModelsLogs> = endososCancelacionesRepository.findAll()

    @PostMapping
    fun create(@Valid @RequestBody nuevoLog: EndososCancelacionesModelsLogs): EndososCancelacionesModelsLogs = endososCancelacionesRepository.save(nuevoLog)
}