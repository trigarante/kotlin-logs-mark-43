package com.ahorraseguros.mx.logskotlinmark43.controllers.catalogosComercial


import com.ahorraseguros.mx.logskotlinmark43.models.catalogosComercial.EstadoSocioModelsLogs
import com.ahorraseguros.mx.logskotlinmark43.repositories.catalogosComercial.EstadoSocioRepository
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.web.bind.annotation.*
import javax.validation.Valid
@CrossOrigin
@RestController
@RequestMapping("/v1/estadoSocio")
class EstadoSocioController {
    @Autowired
    lateinit var estadoSocioRepository: EstadoSocioRepository

    @GetMapping
    fun findAll():MutableList<EstadoSocioModelsLogs> = estadoSocioRepository.findAll()

    @PostMapping
    fun create(@Valid @RequestBody nuevoLog: EstadoSocioModelsLogs): EstadoSocioModelsLogs = estadoSocioRepository.save(nuevoLog)
}