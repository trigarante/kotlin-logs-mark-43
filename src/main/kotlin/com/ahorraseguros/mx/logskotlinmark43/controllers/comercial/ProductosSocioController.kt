package com.ahorraseguros.mx.logskotlinmark43.controllers.comercial

import com.ahorraseguros.mx.logskotlinmark43.models.comercial.ProductosSocioModelsLogs
import com.ahorraseguros.mx.logskotlinmark43.repositories.comercial.ProductosSociosRepository
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.web.bind.annotation.*
import javax.validation.Valid

@CrossOrigin
@RestController
@RequestMapping("/v1/productosSocio")
class ProductosSocioController {

    @Autowired
    lateinit var productosSociosRepository: ProductosSociosRepository

    @GetMapping
    fun findAll():MutableList<ProductosSocioModelsLogs> = productosSociosRepository.findAll()

    @PostMapping
    fun create(@Valid @RequestBody nuevoLog: ProductosSocioModelsLogs): ProductosSocioModelsLogs = productosSociosRepository.save(nuevoLog)
}