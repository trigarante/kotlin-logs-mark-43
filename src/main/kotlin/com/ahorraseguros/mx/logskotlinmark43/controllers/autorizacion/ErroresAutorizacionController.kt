package com.ahorraseguros.mx.logskotlinmark43.controllers.autorizacion

import com.ahorraseguros.mx.logskotlinmark43.models.autorizacion.ErroresAutorizacionLogs
import com.ahorraseguros.mx.logskotlinmark43.repositories.autorizacion.ErroresAutorizacionRepository
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.web.bind.annotation.*
import javax.validation.Valid

@CrossOrigin
@RestController
@RequestMapping("/v1/erroresAutorizacion")
class ErroresAutorizacionController {
    @Autowired
    lateinit var erroresAutorizacionRepository: ErroresAutorizacionRepository

    @GetMapping
    fun findAll():MutableList<ErroresAutorizacionLogs> = erroresAutorizacionRepository.findAll()

    @PostMapping
    fun create(@Valid @RequestBody nuevoLog: ErroresAutorizacionLogs): ErroresAutorizacionLogs = erroresAutorizacionRepository.save(nuevoLog)
}