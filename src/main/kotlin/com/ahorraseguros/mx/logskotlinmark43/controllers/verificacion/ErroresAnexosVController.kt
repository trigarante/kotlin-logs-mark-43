package com.ahorraseguros.mx.logskotlinmark43.controllers.verificacion

import com.ahorraseguros.mx.logskotlinmark43.models.verificacion.ErroresAnexosVModelsLogs
import com.ahorraseguros.mx.logskotlinmark43.repositories.verificacion.ErroresAnexosVRepository
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.web.bind.annotation.*
import javax.validation.Valid

@CrossOrigin
@RestController
@RequestMapping("/v1/erroresAnexosV")
class ErroresAnexosVController {
    @Autowired
    lateinit var erroresAnexosVRepository: ErroresAnexosVRepository

    @GetMapping
    fun findAll():MutableList<ErroresAnexosVModelsLogs> = erroresAnexosVRepository.findAll()

    @PostMapping
    fun create(@Valid @RequestBody nuevoLog: ErroresAnexosVModelsLogs): ErroresAnexosVModelsLogs = erroresAnexosVRepository.save(nuevoLog)
}