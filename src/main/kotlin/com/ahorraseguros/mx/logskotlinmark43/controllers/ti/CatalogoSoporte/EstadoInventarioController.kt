package com.ahorraseguros.mx.logskotlinmark43.controllers.ti.catalogoSoporte


import com.ahorraseguros.mx.logskotlinmark43.models.ti.catalogosSoporte.EstadoInventarioModelsLogs
import com.ahorraseguros.mx.logskotlinmark43.repositories.ti.catalogoSoporte.EstadoInventarioRepository
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.web.bind.annotation.*
import javax.validation.Valid

@CrossOrigin
@RestController
@RequestMapping("/v1/estadoInventario")
class EstadoInventarioController {
    @Autowired
    lateinit var estadoInventarioRepository: EstadoInventarioRepository

    @GetMapping
    fun findAll():MutableList<EstadoInventarioModelsLogs> = estadoInventarioRepository.findAll()

    @PostMapping
    fun create(@Valid @RequestBody nuevoLog: EstadoInventarioModelsLogs): EstadoInventarioModelsLogs = estadoInventarioRepository.save(nuevoLog)
}