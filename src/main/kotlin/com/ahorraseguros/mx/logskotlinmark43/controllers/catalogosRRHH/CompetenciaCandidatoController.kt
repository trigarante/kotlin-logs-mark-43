package com.ahorraseguros.mx.logskotlinmark43.controllers.catalogosRRHH

import com.ahorraseguros.mx.logskotlinmark43.models.catalogosRRHH.CompetenciaCandidatoModelsLogs
import com.ahorraseguros.mx.logskotlinmark43.repositories.catalogosRRHH.CompetenciaCandidatoRepository
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.web.bind.annotation.*
import javax.validation.Valid

@CrossOrigin
@RestController
@RequestMapping("/v1/competencias-candidatos")
class CompetenciaCandidatoController {

    @Autowired
    lateinit var competenciaCandidatoRepository: CompetenciaCandidatoRepository

    @GetMapping
    fun findAll():MutableList<CompetenciaCandidatoModelsLogs> = competenciaCandidatoRepository.findAll()

    @PostMapping
    fun create(@Valid @RequestBody nuevoLog: CompetenciaCandidatoModelsLogs): CompetenciaCandidatoModelsLogs =
            competenciaCandidatoRepository.save(nuevoLog)
}
