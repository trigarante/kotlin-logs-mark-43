package com.ahorraseguros.mx.logskotlinmark43.controllers.ti.soporte

import com.ahorraseguros.mx.logskotlinmark43.models.ti.soporte.BiometricosSoporteModelsLogs
import com.ahorraseguros.mx.logskotlinmark43.repositories.ti.soporte.BiometricosSoporteRepository
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.web.bind.annotation.*
import javax.validation.Valid

@CrossOrigin
@RestController
@RequestMapping("/v1/biometricosSoporte")
class BiometricosSoporteController {
    @Autowired
    lateinit var biometricosSoporteRepository: BiometricosSoporteRepository

    @GetMapping
    fun findAll():MutableList<BiometricosSoporteModelsLogs> = biometricosSoporteRepository.findAll()

    @PostMapping
    fun create(@Valid @RequestBody nuevoLog: BiometricosSoporteModelsLogs): BiometricosSoporteModelsLogs = biometricosSoporteRepository.save(nuevoLog)
}