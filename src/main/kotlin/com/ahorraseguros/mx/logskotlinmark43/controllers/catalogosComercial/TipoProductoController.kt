package com.ahorraseguros.mx.logskotlinmark43.controllers.catalogosComercial


import com.ahorraseguros.mx.logskotlinmark43.models.catalogosComercial.TipoProductoModelsLogs
import com.ahorraseguros.mx.logskotlinmark43.repositories.catalogosComercial.TipoProductoRepository
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.web.bind.annotation.*
import javax.validation.Valid

@CrossOrigin
@RestController
@RequestMapping("/v1/tipoProducto")
class TipoProductoController {
    @Autowired
    lateinit var tipoProductoRepository: TipoProductoRepository

    @GetMapping
    fun findAll():MutableList<TipoProductoModelsLogs> = tipoProductoRepository.findAll()

    @PostMapping
    fun create(@Valid @RequestBody nuevoLog: TipoProductoModelsLogs): TipoProductoModelsLogs = tipoProductoRepository.save(nuevoLog)
}