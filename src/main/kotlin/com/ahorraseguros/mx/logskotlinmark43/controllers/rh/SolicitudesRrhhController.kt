package com.ahorraseguros.mx.logskotlinmark43.controllers.rh

import com.ahorraseguros.mx.logskotlinmark43.models.rh.SolicitudesRrhhModelsLogs
import com.ahorraseguros.mx.logskotlinmark43.repositories.rh.SolicitudesRrhhRepository
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.web.bind.annotation.*
import javax.validation.Valid

@CrossOrigin
@RestController
@RequestMapping("v1/solicitudesRrhh")

class SolicitudesRrhhController {

    @Autowired
    lateinit var solicitudesRrhhRepository: SolicitudesRrhhRepository

    @GetMapping
    fun findAll(): MutableList<SolicitudesRrhhModelsLogs> = solicitudesRrhhRepository.findAll()

    @PostMapping
    fun create (@Valid @RequestBody solicitudesRrhhModelsLogs: SolicitudesRrhhModelsLogs): SolicitudesRrhhModelsLogs {
        return solicitudesRrhhRepository.save(solicitudesRrhhModelsLogs)
    }
}