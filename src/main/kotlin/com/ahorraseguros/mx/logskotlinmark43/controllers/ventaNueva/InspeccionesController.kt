package com.ahorraseguros.mx.logskotlinmark43.controllers.ventaNueva

import com.ahorraseguros.mx.logskotlinmark43.models.ventaNueva.InspeccionesModelsLogs
import com.ahorraseguros.mx.logskotlinmark43.repositories.ventaNueva.InspeccionesRepository
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.web.bind.annotation.*
import javax.validation.Valid

@CrossOrigin
@RestController
@RequestMapping("/v1/inspecciones")
class InspeccionesController {
    @Autowired
    lateinit var inspeccionesRepository: InspeccionesRepository

    @GetMapping
    fun findAll():MutableList<InspeccionesModelsLogs> = inspeccionesRepository.findAll()

    @PostMapping
    fun create(@Valid @RequestBody nuevoLog: InspeccionesModelsLogs): InspeccionesModelsLogs = inspeccionesRepository.save(nuevoLog)
}