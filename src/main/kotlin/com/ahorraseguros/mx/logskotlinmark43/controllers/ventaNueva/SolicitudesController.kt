package com.ahorraseguros.mx.logskotlinmark43.controllers.ventaNueva

import com.ahorraseguros.mx.logskotlinmark43.models.ventaNueva.SolicitudesModelsLogs
import com.ahorraseguros.mx.logskotlinmark43.repositories.ventaNueva.SolicitudesRepository
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.web.bind.annotation.*
import javax.validation.Valid

@CrossOrigin
@RestController
@RequestMapping("/v1/solicitudes")
class SolicitudesController {
    @Autowired
    lateinit var solicitudesRepository: SolicitudesRepository

    @GetMapping
    fun findAll():MutableList<SolicitudesModelsLogs> = solicitudesRepository.findAll()

    @PostMapping
    fun create(@Valid @RequestBody nuevoLog: SolicitudesModelsLogs): SolicitudesModelsLogs = solicitudesRepository.save(nuevoLog)
}