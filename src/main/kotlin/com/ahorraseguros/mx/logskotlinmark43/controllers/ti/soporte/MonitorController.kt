package com.ahorraseguros.mx.logskotlinmark43.controllers.ti.soporte

import com.ahorraseguros.mx.logskotlinmark43.models.ti.soporte.MonitorModelsLogs
import com.ahorraseguros.mx.logskotlinmark43.repositories.ti.soporte.MonitorRepository
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.web.bind.annotation.*
import javax.validation.Valid

@CrossOrigin
@RestController
@RequestMapping("/v1/monitor")
class MonitorController {
    @Autowired
    lateinit var monitorRepository: MonitorRepository

    @GetMapping
    fun findAll():MutableList<MonitorModelsLogs> = monitorRepository.findAll()

    @PostMapping
    fun create(@Valid @RequestBody nuevoLog: MonitorModelsLogs): MonitorModelsLogs = monitorRepository.save(nuevoLog)
}