package com.ahorraseguros.mx.logskotlinmark43.controllers.ventaNueva

import com.ahorraseguros.mx.logskotlinmark43.models.ventaNueva.RegistroModelsLogs
import com.ahorraseguros.mx.logskotlinmark43.repositories.ventaNueva.RegistroRepository
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.web.bind.annotation.*
import javax.validation.Valid

@CrossOrigin
@RestController
@RequestMapping("/v1/registro")
class RegistroController {
    @Autowired
    lateinit var registroRepository: RegistroRepository

    @GetMapping
    fun findAll():MutableList<RegistroModelsLogs> =registroRepository.findAll()

    @PostMapping
    fun create(@Valid @RequestBody nuevoLog: RegistroModelsLogs): RegistroModelsLogs = registroRepository.save(nuevoLog)
}