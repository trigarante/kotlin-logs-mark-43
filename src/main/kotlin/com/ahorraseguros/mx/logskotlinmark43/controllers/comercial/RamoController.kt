package com.ahorraseguros.mx.logskotlinmark43.controllers.comercial


import com.ahorraseguros.mx.logskotlinmark43.models.comercial.RamoModelsLogs
import com.ahorraseguros.mx.logskotlinmark43.repositories.comercial.RamoRepository
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.web.bind.annotation.*
import javax.validation.Valid

@CrossOrigin
@RestController
@RequestMapping("/v1/ramo")
class RamoController {
    @Autowired
    lateinit var ramoRepository: RamoRepository

    @GetMapping
    fun findAll():MutableList<RamoModelsLogs> = ramoRepository.findAll()

    @PostMapping
    fun create(@Valid @RequestBody nuevoLog: RamoModelsLogs): RamoModelsLogs = ramoRepository.save(nuevoLog)
}