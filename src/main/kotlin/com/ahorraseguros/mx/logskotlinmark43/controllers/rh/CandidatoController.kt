package com.ahorraseguros.mx.logskotlinmark43.controllers.rh

import com.ahorraseguros.mx.logskotlinmark43.models.rh.CandidatoModelsLogs
import com.ahorraseguros.mx.logskotlinmark43.repositories.rh.CandidatoRepository
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.web.bind.annotation.*
import javax.validation.Valid


@CrossOrigin
@RestController
@RequestMapping("/v1/candidatos")
class CandidatoController {

    @Autowired
    lateinit var candidatoRepository: CandidatoRepository


    @GetMapping
    fun findAll():MutableList<CandidatoModelsLogs> = candidatoRepository.findAll()


    @PostMapping
    fun create(@Valid  @RequestBody candidatoModelsLogs: CandidatoModelsLogs): CandidatoModelsLogs
            = candidatoRepository.save(candidatoModelsLogs)
}