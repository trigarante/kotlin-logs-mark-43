package com.ahorraseguros.mx.logskotlinmark43.controllers.catalogosComercial

import com.ahorraseguros.mx.logskotlinmark43.models.catalogosComercial.TipoDivisaModelsLogs
import com.ahorraseguros.mx.logskotlinmark43.repositories.catalogosComercial.TipoDivisaRepository
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.web.bind.annotation.*
import javax.validation.Valid

@CrossOrigin
@RestController
@RequestMapping("/v1/tipoDivisa")
class TipoDivisaController {
    @Autowired
    lateinit var tipoDivisaRepository: TipoDivisaRepository

    @GetMapping
    fun findAll():MutableList<TipoDivisaModelsLogs> = tipoDivisaRepository.findAll()

    @PostMapping
    fun create(@Valid @RequestBody nuevoLog: TipoDivisaModelsLogs): TipoDivisaModelsLogs = tipoDivisaRepository.save(nuevoLog)
}