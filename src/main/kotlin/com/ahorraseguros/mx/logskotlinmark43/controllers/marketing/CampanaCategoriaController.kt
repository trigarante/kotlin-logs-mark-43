package com.ahorraseguros.mx.logskotlinmark43.controllers.marketing


import com.ahorraseguros.mx.logskotlinmark43.models.marketing.CampanaCategoriaModelsLogs
import com.ahorraseguros.mx.logskotlinmark43.repositories.marketing.CampanaCategoriaRepository
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.web.bind.annotation.*
import javax.validation.Valid

@CrossOrigin
@RestController
@RequestMapping("/v1/campanaCategoria")
class CampanaCategoriaController {

    @Autowired
    lateinit var campanaCategoriaRepository: CampanaCategoriaRepository

    @GetMapping
    fun findAll():MutableList<CampanaCategoriaModelsLogs> = campanaCategoriaRepository.findAll()

    @PostMapping
    fun create(@Valid @RequestBody nuevoLog: CampanaCategoriaModelsLogs): CampanaCategoriaModelsLogs = campanaCategoriaRepository.save(nuevoLog)
}