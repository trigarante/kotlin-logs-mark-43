package com.ahorraseguros.mx.logskotlinmark43.controllers.catalogosRRHH

import com.ahorraseguros.mx.logskotlinmark43.models.catalogosRRHH.BolsaTrabajoModelsLogs
import com.ahorraseguros.mx.logskotlinmark43.repositories.catalogosRRHH.BolsaTrabajoRepository
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.web.bind.annotation.*
import javax.validation.Valid

@CrossOrigin
@RestController
@RequestMapping("/v1/bolsa-trabajo")
class BolsaTrabajoController {

    @Autowired
    lateinit var bolsaTrabajoRepository: BolsaTrabajoRepository

    @GetMapping
    fun findAll():MutableList<BolsaTrabajoModelsLogs> = bolsaTrabajoRepository.findAll()

    @PostMapping
    fun create(@Valid @RequestBody nuevoLog: BolsaTrabajoModelsLogs): BolsaTrabajoModelsLogs = bolsaTrabajoRepository.save(nuevoLog)
}