package com.ahorraseguros.mx.logskotlinmark43.controllers.verificacion

import com.ahorraseguros.mx.logskotlinmark43.models.autorizacion.AutorizacionRegistroLogs
import com.ahorraseguros.mx.logskotlinmark43.models.verificacion.ErroresVerificacionModelsLogs
import com.ahorraseguros.mx.logskotlinmark43.repositories.autorizacion.AutorizacionRegistroRepository
import com.ahorraseguros.mx.logskotlinmark43.repositories.verificacion.ErroresVerificacionRepository
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.web.bind.annotation.*
import javax.validation.Valid

@CrossOrigin
@RestController
@RequestMapping("/v1/erroresVerificacion")
class ErroresVerificacionController {
    @Autowired
    lateinit var erroresVerificacionRepository: ErroresVerificacionRepository

    @GetMapping
    fun findAll():MutableList<ErroresVerificacionModelsLogs> = erroresVerificacionRepository.findAll()

    @PostMapping
    fun create(@Valid @RequestBody nuevoLog: ErroresVerificacionModelsLogs): ErroresVerificacionModelsLogs = erroresVerificacionRepository.save(nuevoLog)
}