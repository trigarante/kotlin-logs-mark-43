package com.ahorraseguros.mx.logskotlinmark43.controllers.ti.soporte

import com.ahorraseguros.mx.logskotlinmark43.models.ti.soporte.ApsModelsLogs
import com.ahorraseguros.mx.logskotlinmark43.repositories.ti.soporte.ApsRepository
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.web.bind.annotation.*
import javax.validation.Valid

@CrossOrigin
@RestController
@RequestMapping("/v1/aps")
class ApsController {
    @Autowired
    lateinit var apsRepository: ApsRepository

    @GetMapping
    fun findAll():MutableList<ApsModelsLogs> = apsRepository.findAll()

    @PostMapping
    fun create(@Valid @RequestBody nuevoLog: ApsModelsLogs): ApsModelsLogs = apsRepository.save(nuevoLog)
}