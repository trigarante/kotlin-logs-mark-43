package com.ahorraseguros.mx.logskotlinmark43.controllers.ti

import com.ahorraseguros.mx.logskotlinmark43.models.ti.ExtensionesModelsLogs
import com.ahorraseguros.mx.logskotlinmark43.repositories.ti.ExtensionesRepository
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.web.bind.annotation.*
import javax.validation.Valid

@CrossOrigin
@RestController
@RequestMapping("/v1/extensiones")
class ExtensionesController {

    @Autowired
    lateinit var extensionesRepository: ExtensionesRepository

    @GetMapping
    fun findAll():MutableList<ExtensionesModelsLogs> = extensionesRepository.findAll()

    @PostMapping
    fun create(@Valid @RequestBody nuevoLog: ExtensionesModelsLogs): ExtensionesModelsLogs =
            extensionesRepository.save(nuevoLog)
}