package com.ahorraseguros.mx.logskotlinmark43.controllers.CatalogosVentaNueva

import com.ahorraseguros.mx.logskotlinmark43.models.catalogosVentaNueva.FlujoPolizaModelsLogs
import com.ahorraseguros.mx.logskotlinmark43.repositories.catalogosVentaNueva.FlujoPolizaRepository
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.web.bind.annotation.*
import javax.validation.Valid

@CrossOrigin
@RestController
@RequestMapping("/v1/flujoPoliza")
class FlujoPolizaController {
    @Autowired
    lateinit var flujoPolizaRepository: FlujoPolizaRepository

    @GetMapping
    fun findAll():MutableList<FlujoPolizaModelsLogs> = flujoPolizaRepository.findAll()

    @PostMapping
    fun create(@Valid @RequestBody nuevoLog: FlujoPolizaModelsLogs): FlujoPolizaModelsLogs = flujoPolizaRepository.save(nuevoLog)
}