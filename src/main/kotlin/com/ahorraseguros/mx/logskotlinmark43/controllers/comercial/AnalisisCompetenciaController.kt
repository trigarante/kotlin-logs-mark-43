package com.ahorraseguros.mx.logskotlinmark43.controllers.comercial


import com.ahorraseguros.mx.logskotlinmark43.models.comercial.AnalisisCompetenciaModelsLogs
import com.ahorraseguros.mx.logskotlinmark43.repositories.comercial.AnalisisCompetenciaRepository
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.web.bind.annotation.*
import javax.validation.Valid

@CrossOrigin
@RestController
@RequestMapping("/v1/analisisCompetencia")
class AnalisisCompetenciaController {
    @Autowired
    lateinit var analisisCompetenciaRepository: AnalisisCompetenciaRepository

    @GetMapping
    fun findAll():MutableList<AnalisisCompetenciaModelsLogs> = analisisCompetenciaRepository.findAll()

    @PostMapping
    fun create(@Valid @RequestBody nuevoLog: AnalisisCompetenciaModelsLogs): AnalisisCompetenciaModelsLogs = analisisCompetenciaRepository.save(nuevoLog)
}