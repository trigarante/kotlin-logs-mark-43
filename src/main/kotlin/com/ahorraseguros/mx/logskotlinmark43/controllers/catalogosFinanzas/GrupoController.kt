package com.ahorraseguros.mx.logskotlinmark43.controllers.catalogosFinanzas

import com.ahorraseguros.mx.logskotlinmark43.models.catalogosFinanzas.GrupoModelsLogs
import com.ahorraseguros.mx.logskotlinmark43.repositories.catalogosFinanzas.GrupoRepository
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.web.bind.annotation.*
import javax.validation.Valid

@CrossOrigin
@RestController
@RequestMapping("/v1/grupo")
class GrupoController {
    @Autowired
    lateinit var grupoRepository: GrupoRepository

    @GetMapping
    fun findAll():MutableList<GrupoModelsLogs> = grupoRepository.findAll()

    @PostMapping
    fun create(@Valid @RequestBody nuevoLog: GrupoModelsLogs): GrupoModelsLogs = grupoRepository.save(nuevoLog)
}