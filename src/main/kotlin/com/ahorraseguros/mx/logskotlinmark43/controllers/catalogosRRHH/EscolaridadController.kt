package com.ahorraseguros.mx.logskotlinmark43.controllers.catalogosRRHH

import com.ahorraseguros.mx.logskotlinmark43.models.catalogosRRHH.EscolaridadModelsLogs
import com.ahorraseguros.mx.logskotlinmark43.repositories.catalogosRRHH.EscolaridadRepository
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.web.bind.annotation.*
import javax.validation.Valid

@CrossOrigin
@RestController
@RequestMapping("/v1/escolaridades")
class EscolaridadController {

    @Autowired
    lateinit var escolaridadRepository: EscolaridadRepository

    @GetMapping
    fun findAll():MutableList<EscolaridadModelsLogs> = escolaridadRepository.findAll()

    @PostMapping
    fun create(@Valid @RequestBody nuevoLog: EscolaridadModelsLogs): EscolaridadModelsLogs =
            escolaridadRepository.save(nuevoLog)
}