package com.ahorraseguros.mx.logskotlinmark43.controllers.catalogosComercial

import com.ahorraseguros.mx.logskotlinmark43.models.catalogosComercial.TipoSubRamoModelsLogs
import com.ahorraseguros.mx.logskotlinmark43.repositories.catalogosComercial.TipoSubRamoRepository
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.web.bind.annotation.*
import javax.validation.Valid

@CrossOrigin
@RestController
@RequestMapping("/v1/tipoSubRamo")
class TipoSubRamoController {
    @Autowired
    lateinit var tipoSubRamoRepository: TipoSubRamoRepository

    @GetMapping
    fun findAll():MutableList<TipoSubRamoModelsLogs> = tipoSubRamoRepository.findAll()

    @PostMapping
    fun create(@Valid @RequestBody nuevoLog: TipoSubRamoModelsLogs): TipoSubRamoModelsLogs = tipoSubRamoRepository.save(nuevoLog)
}