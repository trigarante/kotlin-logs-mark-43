package com.ahorraseguros.mx.logskotlinmark43.controllers.comercial


import com.ahorraseguros.mx.logskotlinmark43.models.comercial.CoberturasModelsLogs
import com.ahorraseguros.mx.logskotlinmark43.repositories.comercial.CoberturasRepository
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.web.bind.annotation.*
import javax.validation.Valid

@CrossOrigin
@RestController
@RequestMapping("/v1/coberturas")
class CoberturaController {
    @Autowired
    lateinit var coberturasRepository: CoberturasRepository

    @GetMapping
    fun findAll():MutableList<CoberturasModelsLogs> = coberturasRepository.findAll()

    @PostMapping
    fun create(@Valid @RequestBody nuevoLog: CoberturasModelsLogs): CoberturasModelsLogs = coberturasRepository.save(nuevoLog)
}