package com.ahorraseguros.mx.logskotlinmark43.controllers.ventaNueva

import com.ahorraseguros.mx.logskotlinmark43.models.ventaNueva.CotizacionesModelsLogs
import com.ahorraseguros.mx.logskotlinmark43.repositories.ventaNueva.CotizacionesRepository
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.web.bind.annotation.*
import javax.validation.Valid

@CrossOrigin
@RestController
@RequestMapping("/v1/cotizaciones")
class CotizacionesController {
    @Autowired
    lateinit var cotizacionesRepository: CotizacionesRepository

    @GetMapping
    fun findAll():MutableList<CotizacionesModelsLogs> = cotizacionesRepository.findAll()

    @PostMapping
    fun create(@Valid @RequestBody nuevoLog: CotizacionesModelsLogs): CotizacionesModelsLogs = cotizacionesRepository.save(nuevoLog)
}