package com.ahorraseguros.mx.logskotlinmark43.controllers.rh

import com.ahorraseguros.mx.logskotlinmark43.models.rh.CapacitacionModelsLogs
import com.ahorraseguros.mx.logskotlinmark43.repositories.rh.CapacitacionRepository
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.web.bind.annotation.*
import javax.validation.Valid

@CrossOrigin
@RestController
@RequestMapping("v1/capacitaciones")

class CapacitacionController {

    @Autowired
    lateinit var capacitacionRepository: CapacitacionRepository

    @GetMapping
    fun findAll(): MutableList<CapacitacionModelsLogs> = capacitacionRepository.findAll()

    @PostMapping
    fun create (@Valid @RequestBody capacitacionModelsLogs: CapacitacionModelsLogs): CapacitacionModelsLogs {
        return capacitacionRepository.save(capacitacionModelsLogs)
    }
}
