package com.ahorraseguros.mx.logskotlinmark43.controllers.comercial

import com.ahorraseguros.mx.logskotlinmark43.models.comercial.ContratosSociosModelsLogs
import com.ahorraseguros.mx.logskotlinmark43.repositories.comercial.ContratosSociosRepository
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.web.bind.annotation.*
import javax.validation.Valid

@CrossOrigin
@RestController
@RequestMapping("/v1/contratosSocios")
class ContratosSociosController {
    @Autowired
    lateinit var contratosSociosRepository: ContratosSociosRepository

    @GetMapping
    fun findAll():MutableList<ContratosSociosModelsLogs> = contratosSociosRepository.findAll()

    @PostMapping
    fun create(@Valid @RequestBody nuevoLog: ContratosSociosModelsLogs): ContratosSociosModelsLogs = contratosSociosRepository.save(nuevoLog)
}