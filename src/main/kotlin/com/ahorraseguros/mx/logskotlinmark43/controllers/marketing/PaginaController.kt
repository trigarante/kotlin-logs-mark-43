package com.ahorraseguros.mx.logskotlinmark43.controllers.marketing


import com.ahorraseguros.mx.logskotlinmark43.models.marketing.PaginaModelsLogs
import com.ahorraseguros.mx.logskotlinmark43.repositories.marketing.PaginaRepository
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.web.bind.annotation.*
import javax.validation.Valid

@CrossOrigin
@RestController
@RequestMapping("/v1/pagina")
class PaginaController {
    @Autowired
    lateinit var paginaRepository: PaginaRepository

    @GetMapping
    fun findAll():MutableList<PaginaModelsLogs> = paginaRepository.findAll()

    @PostMapping
    fun create(@Valid @RequestBody nuevoLog: PaginaModelsLogs): PaginaModelsLogs = paginaRepository.save(nuevoLog)
}