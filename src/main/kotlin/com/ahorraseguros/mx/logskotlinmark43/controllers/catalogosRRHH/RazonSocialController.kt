package com.ahorraseguros.mx.logskotlinmark43.controllers.catalogosRRHH

import com.ahorraseguros.mx.logskotlinmark43.models.catalogosRRHH.RazonSocialModelsLogs
import com.ahorraseguros.mx.logskotlinmark43.repositories.catalogosRRHH.RazonSocialRepository
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.web.bind.annotation.*
import javax.validation.Valid

@CrossOrigin
@RestController
@RequestMapping("/v1/razon-social")
class RazonSocialController {

    @Autowired
    lateinit var razonSocialRepository: RazonSocialRepository

    @GetMapping
    fun findAll():MutableList<RazonSocialModelsLogs> = razonSocialRepository.findAll()

    @PostMapping
    fun create(@Valid @RequestBody nuevoLog: RazonSocialModelsLogs): RazonSocialModelsLogs =
            razonSocialRepository.save(nuevoLog)
}