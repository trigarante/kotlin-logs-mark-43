package com.ahorraseguros.mx.logskotlinmark43.controllers.ti.soporte

import com.ahorraseguros.mx.logskotlinmark43.models.ti.soporte.ChromeBitModelsLogs
import com.ahorraseguros.mx.logskotlinmark43.repositories.ti.soporte.ChromeBitRepository
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.web.bind.annotation.*
import javax.validation.Valid

@CrossOrigin
@RestController
@RequestMapping("/v1/chromeBit")
class ChromeBitController {
    @Autowired
    lateinit var chromeBitRepository: ChromeBitRepository

    @GetMapping
    fun findAll():MutableList<ChromeBitModelsLogs> = chromeBitRepository.findAll()

    @PostMapping
    fun create(@Valid @RequestBody nuevoLog: ChromeBitModelsLogs): ChromeBitModelsLogs = chromeBitRepository.save(nuevoLog)
}
