package com.ahorraseguros.mx.logskotlinmark43.controllers.siniestros

import com.ahorraseguros.mx.logskotlinmark43.models.siniestros.SiniestroModels
import com.ahorraseguros.mx.logskotlinmark43.repositories.siniestros.siniestrosRepository
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.web.bind.annotation.*
import javax.validation.Valid

@CrossOrigin
@RestController
@RequestMapping("/v1/siniestros")
class SiniestrosController {
    @Autowired
    lateinit var siniestrosRepository: siniestrosRepository

    @GetMapping
    fun findAll():MutableList<SiniestroModels> = siniestrosRepository.findAll()

    @PostMapping
    fun create(@Valid @RequestBody nuevoLog: SiniestroModels): SiniestroModels = siniestrosRepository.save(nuevoLog)
}
