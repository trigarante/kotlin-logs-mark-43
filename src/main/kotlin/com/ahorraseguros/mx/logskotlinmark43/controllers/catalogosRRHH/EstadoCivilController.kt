package com.ahorraseguros.mx.logskotlinmark43.controllers.catalogosRRHH

import com.ahorraseguros.mx.logskotlinmark43.models.catalogosRRHH.EstadoCivilModelsLogs
import com.ahorraseguros.mx.logskotlinmark43.repositories.catalogosRRHH.EstadoCivilRepository
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.web.bind.annotation.*
import javax.validation.Valid

@CrossOrigin
@RestController
@RequestMapping("/v1/estado-civil")
class EstadoCivilController {

    @Autowired
    lateinit var estadoCivilRepository: EstadoCivilRepository

    @GetMapping
    fun findAll():MutableList<EstadoCivilModelsLogs> = estadoCivilRepository.findAll()

    @PostMapping
    fun create(@Valid @RequestBody nuevoLog: EstadoCivilModelsLogs): EstadoCivilModelsLogs =
            estadoCivilRepository.save(nuevoLog)
}