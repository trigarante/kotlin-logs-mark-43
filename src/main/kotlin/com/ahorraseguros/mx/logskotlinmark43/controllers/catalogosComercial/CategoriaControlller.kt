package com.ahorraseguros.mx.logskotlinmark43.controllers.catalogosComercial


import com.ahorraseguros.mx.logskotlinmark43.models.catalogosComercial.CategoriaModelsLogs
import com.ahorraseguros.mx.logskotlinmark43.repositories.catalogosComercial.CategoriaRepository
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.web.bind.annotation.*
import javax.validation.Valid

@CrossOrigin
@RestController
@RequestMapping("/v1/categoria")
class CategoriaControlller {
    @Autowired
    lateinit var categoriaRepository: CategoriaRepository

    @GetMapping
    fun findAll():MutableList<CategoriaModelsLogs> = categoriaRepository.findAll()

    @PostMapping
    fun create(@Valid @RequestBody nuevoLog: CategoriaModelsLogs): CategoriaModelsLogs = categoriaRepository.save(nuevoLog)

}