package com.ahorraseguros.mx.logskotlinmark43.controllers.ti.soporte


import com.ahorraseguros.mx.logskotlinmark43.models.ti.soporte.LaptopsModelsLogs
import com.ahorraseguros.mx.logskotlinmark43.repositories.ti.soporte.LaptopsRepository
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.web.bind.annotation.*
import javax.validation.Valid

@CrossOrigin
@RestController
@RequestMapping("/v1/laptops")
class LaptopsController {
    @Autowired
    lateinit var laptopsRepository: LaptopsRepository

    @GetMapping
    fun findAll():MutableList<LaptopsModelsLogs> = laptopsRepository.findAll()

    @PostMapping
    fun create(@Valid @RequestBody nuevoLog: LaptopsModelsLogs): LaptopsModelsLogs = laptopsRepository.save(nuevoLog)
}