package com.ahorraseguros.mx.logskotlinmark43.controllers.comercial


import com.ahorraseguros.mx.logskotlinmark43.models.comercial.ConveniosModelsLogs
import com.ahorraseguros.mx.logskotlinmark43.repositories.comercial.ConveniosRepository
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.web.bind.annotation.*
import javax.validation.Valid

@CrossOrigin
@RestController
@RequestMapping("/v1/convenios")
class ConveniosController {
    @Autowired
    lateinit var conveniosRepository: ConveniosRepository

    @GetMapping
    fun findAll():MutableList<ConveniosModelsLogs> = conveniosRepository.findAll()

    @PostMapping
    fun create(@Valid @RequestBody nuevoLog: ConveniosModelsLogs): ConveniosModelsLogs = conveniosRepository.save(nuevoLog)
}