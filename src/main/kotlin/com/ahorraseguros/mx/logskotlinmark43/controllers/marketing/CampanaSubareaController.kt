package com.ahorraseguros.mx.logskotlinmark43.controllers.marketing

import com.ahorraseguros.mx.logskotlinmark43.models.marketing.CampanaSubareaModelsLogs
import com.ahorraseguros.mx.logskotlinmark43.repositories.marketing.CampanaSubareaRepository
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.web.bind.annotation.*
import javax.validation.Valid
@CrossOrigin
@RestController
@RequestMapping("/v1/campanaSubarea")
class CampanaSubareaController {
    @Autowired
    lateinit var campanaSubareaRepository: CampanaSubareaRepository
    @GetMapping
    fun findAll():MutableList<CampanaSubareaModelsLogs> = campanaSubareaRepository.findAll()

    @PostMapping
    fun create(@Valid @RequestBody nuevoLog: CampanaSubareaModelsLogs): CampanaSubareaModelsLogs = campanaSubareaRepository.save(nuevoLog)
}