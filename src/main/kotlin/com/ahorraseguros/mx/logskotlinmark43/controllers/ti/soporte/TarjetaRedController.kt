package com.ahorraseguros.mx.logskotlinmark43.controllers.ti.soporte

import com.ahorraseguros.mx.logskotlinmark43.models.ti.soporte.TarjetaRedModelsLogs
import com.ahorraseguros.mx.logskotlinmark43.repositories.ti.soporte.TarjetaRedRepository
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.web.bind.annotation.*
import javax.validation.Valid

@CrossOrigin
@RestController
@RequestMapping("/v1/tarjetaRed")
class TarjetaRedController {
    @Autowired
    lateinit var tarjetaRedRepository: TarjetaRedRepository

    @GetMapping
    fun findAll():MutableList<TarjetaRedModelsLogs> = tarjetaRedRepository.findAll()

    @PostMapping
    fun create(@Valid @RequestBody nuevoLog: TarjetaRedModelsLogs): TarjetaRedModelsLogs = tarjetaRedRepository.save(nuevoLog)
}