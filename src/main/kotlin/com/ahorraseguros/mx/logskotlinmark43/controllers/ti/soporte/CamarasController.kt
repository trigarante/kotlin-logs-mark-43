package com.ahorraseguros.mx.logskotlinmark43.controllers.ti.soporte


import com.ahorraseguros.mx.logskotlinmark43.models.ti.soporte.CamarasModelsLogs
import com.ahorraseguros.mx.logskotlinmark43.repositories.ti.soporte.CamarasRepository
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.web.bind.annotation.*
import javax.validation.Valid

@CrossOrigin
@RestController
@RequestMapping("/v1/camaras")
class CamarasController {
    @Autowired
    lateinit var camarasRepository: CamarasRepository

    @GetMapping
    fun findAll():MutableList<CamarasModelsLogs> = camarasRepository.findAll()

    @PostMapping
    fun create(@Valid @RequestBody nuevoLog: CamarasModelsLogs): CamarasModelsLogs = camarasRepository.save(nuevoLog)
}