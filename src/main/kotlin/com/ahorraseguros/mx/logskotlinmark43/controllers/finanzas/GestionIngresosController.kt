package com.ahorraseguros.mx.logskotlinmark43.controllers.finanzas

import com.ahorraseguros.mx.logskotlinmark43.models.finanzas.GestionIngresosModelsLogs
import com.ahorraseguros.mx.logskotlinmark43.repositories.finanzas.GestionIngresosRepository
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.web.bind.annotation.*
import javax.validation.Valid

@CrossOrigin
@RestController
@RequestMapping("/v1/gestionIngresos")
class GestionIngresosController {
    @Autowired
    lateinit var gestionIngresosRepository: GestionIngresosRepository

    @GetMapping
    fun findAll():MutableList<GestionIngresosModelsLogs> = gestionIngresosRepository.findAll()

    @PostMapping
    fun create(@Valid @RequestBody nuevoLog: GestionIngresosModelsLogs): GestionIngresosModelsLogs = gestionIngresosRepository.save(nuevoLog)
}