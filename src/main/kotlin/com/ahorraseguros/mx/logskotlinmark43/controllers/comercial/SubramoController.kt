package com.ahorraseguros.mx.logskotlinmark43.controllers.comercial


import com.ahorraseguros.mx.logskotlinmark43.models.comercial.SubRamoModelsLogs
import com.ahorraseguros.mx.logskotlinmark43.repositories.comercial.SubramoRepository
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.web.bind.annotation.*
import javax.validation.Valid

@CrossOrigin
@RestController
@RequestMapping("/v1/subramo")
class SubramoController {
    @Autowired
    lateinit var subramoRepository: SubramoRepository

    @GetMapping
    fun findAll():MutableList<SubRamoModelsLogs> = subramoRepository.findAll()

    @PostMapping
    fun create(@Valid @RequestBody nuevoLog: SubRamoModelsLogs): SubRamoModelsLogs = subramoRepository.save(nuevoLog)
}