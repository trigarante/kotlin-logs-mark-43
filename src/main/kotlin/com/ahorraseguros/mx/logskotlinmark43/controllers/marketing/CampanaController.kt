package com.ahorraseguros.mx.logskotlinmark43.controllers.marketing

import com.ahorraseguros.mx.logskotlinmark43.models.marketing.CampanaModelsLogs
import com.ahorraseguros.mx.logskotlinmark43.repositories.marketing.CampanaRepository
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.web.bind.annotation.*
import javax.validation.Valid

@CrossOrigin
@RestController
@RequestMapping("/v1/campana")
class CampanaController {
    @Autowired
    lateinit var campanaRepository: CampanaRepository

    @GetMapping
    fun findAll():MutableList<CampanaModelsLogs> = campanaRepository.findAll()

    @PostMapping
    fun create(@Valid @RequestBody nuevoLog: CampanaModelsLogs): CampanaModelsLogs = campanaRepository.save(nuevoLog)
}