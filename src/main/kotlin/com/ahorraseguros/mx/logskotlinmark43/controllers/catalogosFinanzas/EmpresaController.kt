package com.ahorraseguros.mx.logskotlinmark43.controllers.catalogosFinanzas

import com.ahorraseguros.mx.logskotlinmark43.models.catalogosFinanzas.EmpresaModelsLogs
import com.ahorraseguros.mx.logskotlinmark43.repositories.catalogosFinanzas.EmpresaRepository
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.web.bind.annotation.*
import javax.validation.Valid

@CrossOrigin
@RestController
@RequestMapping("/v1/empresa")
class EmpresaController {
    @Autowired
    lateinit var empresaRepository: EmpresaRepository

    @GetMapping
    fun findAll():MutableList<EmpresaModelsLogs> = empresaRepository.findAll()

    @PostMapping
    fun create(@Valid @RequestBody nuevoLog: EmpresaModelsLogs): EmpresaModelsLogs = empresaRepository.save(nuevoLog)
}