package com.ahorraseguros.mx.logskotlinmark43.controllers.ti

import com.ahorraseguros.mx.logskotlinmark43.models.ti.UsuariosModelsLogs
import com.ahorraseguros.mx.logskotlinmark43.repositories.ti.UsuariosRepository
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.web.bind.annotation.*
import javax.validation.Valid

@CrossOrigin
@RestController
@RequestMapping("/v1/usuarios")
class UsuariosController {

    @Autowired
    lateinit var usuariosRepository: UsuariosRepository

    @GetMapping
    fun findAll():MutableList<UsuariosModelsLogs> = usuariosRepository.findAll()

    @PostMapping
    fun create(@Valid @RequestBody nuevoLog: UsuariosModelsLogs): UsuariosModelsLogs = usuariosRepository.save(nuevoLog)
}