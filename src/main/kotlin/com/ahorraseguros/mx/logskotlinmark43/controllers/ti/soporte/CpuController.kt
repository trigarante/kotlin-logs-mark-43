package com.ahorraseguros.mx.logskotlinmark43.controllers.ti.soporte

import com.ahorraseguros.mx.logskotlinmark43.models.ti.soporte.CpuModel
import com.ahorraseguros.mx.logskotlinmark43.repositories.ti.soporte.CpuRepository
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.web.bind.annotation.*
import javax.validation.Valid

@CrossOrigin
@RestController
@RequestMapping("/v1/cpu")
class CpuController
{

    @Autowired
    lateinit var cpuRepository: CpuRepository

    @GetMapping
    fun findAll():MutableList<CpuModel> = cpuRepository.findAll()

    @PostMapping
    fun create(@Valid @RequestBody nuevoLog: CpuModel): CpuModel = cpuRepository.save(nuevoLog)
}
