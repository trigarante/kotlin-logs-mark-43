package com.ahorraseguros.mx.logskotlinmark43.controllers.marketing.catalogos

import com.ahorraseguros.mx.logskotlinmark43.models.marketing.catalogos.TipoPaginaModelsLogs
import com.ahorraseguros.mx.logskotlinmark43.repositories.marketing.catalogos.TipoPaginaRepository
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.web.bind.annotation.*
import javax.validation.Valid

@CrossOrigin
@RestController
@RequestMapping("/v1/tipoPagina")
class TipoPaginaController {
    @Autowired
    lateinit var tipoPaginaRepository: TipoPaginaRepository

    @GetMapping
    fun findAll():MutableList<TipoPaginaModelsLogs> = tipoPaginaRepository.findAll()

    @PostMapping
    fun create(@Valid @RequestBody nuevoLog: TipoPaginaModelsLogs): TipoPaginaModelsLogs = tipoPaginaRepository.save(nuevoLog)
}