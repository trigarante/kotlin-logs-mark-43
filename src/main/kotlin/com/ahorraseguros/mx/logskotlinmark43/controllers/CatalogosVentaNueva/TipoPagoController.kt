package com.ahorraseguros.mx.logskotlinmark43.controllers.CatalogosVentaNueva

import com.ahorraseguros.mx.logskotlinmark43.models.catalogosVentaNueva.TipoPagoModelsLogs
import com.ahorraseguros.mx.logskotlinmark43.repositories.catalogosVentaNueva.TipoPagoRepository
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.web.bind.annotation.*
import javax.validation.Valid

@CrossOrigin
@RestController
@RequestMapping("/v1/tipoPago")
class TipoPagoController {

    @Autowired
    lateinit var tipoPagoRepository: TipoPagoRepository

    @GetMapping
    fun findAll():MutableList<TipoPagoModelsLogs> = tipoPagoRepository.findAll()

    @PostMapping
    fun create(@Valid @RequestBody nuevoLog: TipoPagoModelsLogs): TipoPagoModelsLogs = tipoPagoRepository.save(nuevoLog)
}