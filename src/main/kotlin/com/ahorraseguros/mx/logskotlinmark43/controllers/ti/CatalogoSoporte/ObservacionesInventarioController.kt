package com.ahorraseguros.mx.logskotlinmark43.controllers.ti.catalogoSoporte


import com.ahorraseguros.mx.logskotlinmark43.models.ti.catalogosSoporte.ObservacionesInventarioModelsLogs
import com.ahorraseguros.mx.logskotlinmark43.repositories.ti.catalogoSoporte.ObservacionesInventarioRepository
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.web.bind.annotation.*
import javax.validation.Valid

@CrossOrigin
@RestController
@RequestMapping("/v1/observacionesInventario")
class ObservacionesInventarioController {
    @Autowired
    lateinit var observacionesInventarioRepository: ObservacionesInventarioRepository

    @GetMapping
    fun findAll():MutableList<ObservacionesInventarioModelsLogs> = observacionesInventarioRepository.findAll()

    @PostMapping
    fun create(@Valid @RequestBody nuevoLog: ObservacionesInventarioModelsLogs): ObservacionesInventarioModelsLogs = observacionesInventarioRepository.save(nuevoLog)
}