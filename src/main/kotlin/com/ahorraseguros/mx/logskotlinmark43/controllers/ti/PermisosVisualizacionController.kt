package com.ahorraseguros.mx.logskotlinmark43.controllers.ti



import com.ahorraseguros.mx.logskotlinmark43.models.ti.catalogos.PermisosVisualizacionModelLogs
import com.ahorraseguros.mx.logskotlinmark43.repositories.ti.catalogos.PermisosVisualizacionRepository
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.web.bind.annotation.*
import javax.validation.Valid

@CrossOrigin
@RestController
@RequestMapping("/v1/permisosVisualizacion")
class PermisosVisualizacionController {
    @Autowired
    lateinit var permisosVisualizacionRepository: PermisosVisualizacionRepository

    @GetMapping
    fun findAll():MutableList<PermisosVisualizacionModelLogs> = permisosVisualizacionRepository.findAll()

    @PostMapping
    fun create(@Valid @RequestBody nuevoLog: PermisosVisualizacionModelLogs): PermisosVisualizacionModelLogs = permisosVisualizacionRepository.save(nuevoLog)
}