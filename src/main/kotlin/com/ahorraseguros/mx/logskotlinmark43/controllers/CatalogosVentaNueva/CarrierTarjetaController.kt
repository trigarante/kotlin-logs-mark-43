package com.ahorraseguros.mx.logskotlinmark43.controllers.CatalogosVentaNueva

import com.ahorraseguros.mx.logskotlinmark43.models.catalogosVentaNueva.CarrierTarjetasModelsLogs
import com.ahorraseguros.mx.logskotlinmark43.repositories.catalogosVentaNueva.CarrrierTarjetaRepository
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.web.bind.annotation.*
import javax.validation.Valid

@CrossOrigin
@RestController
@RequestMapping("/v1/corrierTarjeta")
class CarrierTarjetaController {

    @Autowired
    lateinit var carrierTarjetaRepository: CarrrierTarjetaRepository

    @GetMapping
    fun findAll():MutableList<CarrierTarjetasModelsLogs> = carrierTarjetaRepository.findAll()

    @PostMapping
    fun create(@Valid @RequestBody nuevoLog: CarrierTarjetasModelsLogs): CarrierTarjetasModelsLogs = carrierTarjetaRepository.save(nuevoLog)
}