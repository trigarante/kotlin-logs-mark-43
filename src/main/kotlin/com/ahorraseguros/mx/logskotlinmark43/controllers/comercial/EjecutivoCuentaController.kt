package com.ahorraseguros.mx.logskotlinmark43.controllers.comercial


import com.ahorraseguros.mx.logskotlinmark43.models.comercial.EjecutivoCuentaModelsLogs
import com.ahorraseguros.mx.logskotlinmark43.repositories.comercial.EjecutivoCuentaRepository
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.web.bind.annotation.*
import javax.validation.Valid

@CrossOrigin
@RestController
@RequestMapping("/v1/ejecutivoCuenta")
class EjecutivoCuentaController {
    @Autowired
    lateinit var ejecutivoCuentaRepository: EjecutivoCuentaRepository

    @GetMapping
    fun findAll():MutableList<EjecutivoCuentaModelsLogs> = ejecutivoCuentaRepository.findAll()

    @PostMapping
    fun create(@Valid @RequestBody nuevoLog: EjecutivoCuentaModelsLogs): EjecutivoCuentaModelsLogs {
       println(nuevoLog.idLog)
        return ejecutivoCuentaRepository.save(nuevoLog) }

}