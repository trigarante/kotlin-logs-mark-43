package com.ahorraseguros.mx.logskotlinmark43.controllers.verificacion

import com.ahorraseguros.mx.logskotlinmark43.models.verificacion.VerificacionRegistroModelsLogs
import com.ahorraseguros.mx.logskotlinmark43.repositories.verificacion.VerificacacionRegistroRepository
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.web.bind.annotation.*
import javax.validation.Valid

@CrossOrigin
@RestController
@RequestMapping("/v1/verificacionRegistro")
class VerificacionRegistroController {
    @Autowired
    lateinit var verificacionRegistroRepository: VerificacacionRegistroRepository

    @GetMapping
    fun findAll():MutableList<VerificacionRegistroModelsLogs> = verificacionRegistroRepository.findAll()

    @PostMapping
    fun create(@Valid @RequestBody nuevoLog: VerificacionRegistroModelsLogs): VerificacionRegistroModelsLogs = verificacionRegistroRepository.save(nuevoLog)
}