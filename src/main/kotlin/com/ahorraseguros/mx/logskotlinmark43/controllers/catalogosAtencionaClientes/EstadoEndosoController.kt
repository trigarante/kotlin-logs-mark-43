package com.ahorraseguros.mx.logskotlinmark43.controllers.catalogosAtencionaClientes

import com.ahorraseguros.mx.logskotlinmark43.models.catalogosAtencionaClientes.EstadoEndosoModelsLogs
import com.ahorraseguros.mx.logskotlinmark43.repositories.catalogosAtencionaClientes.EstadoEndosoRepository
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.web.bind.annotation.*
import javax.validation.Valid

@CrossOrigin
@RestController
@RequestMapping("/v1/estadoEndoso")
class EstadoEndosoController {
    @Autowired
    lateinit var estadoEndosoRepository: EstadoEndosoRepository

    @GetMapping
    fun findAll():MutableList<EstadoEndosoModelsLogs> = estadoEndosoRepository.findAll()

    @PostMapping
    fun create(@Valid @RequestBody nuevoLog: EstadoEndosoModelsLogs): EstadoEndosoModelsLogs = estadoEndosoRepository.save(nuevoLog)
}