package com.ahorraseguros.mx.logskotlinmark43.controllers.ventaNueva

import com.ahorraseguros.mx.logskotlinmark43.models.ventaNueva.PagosModelsLogs
import com.ahorraseguros.mx.logskotlinmark43.repositories.ventaNueva.PagosRepository
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.web.bind.annotation.*
import javax.validation.Valid

@CrossOrigin
@RestController
@RequestMapping("/v1/pagos")
class PagosController {
    @Autowired
    lateinit var pagosRepository: PagosRepository

    @GetMapping
    fun findAll():MutableList<PagosModelsLogs> = pagosRepository.findAll()

    @PostMapping
    fun create(@Valid @RequestBody nuevoLog: PagosModelsLogs): PagosModelsLogs = pagosRepository.save(nuevoLog)
}