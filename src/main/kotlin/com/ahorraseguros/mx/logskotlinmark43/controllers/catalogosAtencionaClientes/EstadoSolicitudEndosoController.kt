package com.ahorraseguros.mx.logskotlinmark43.controllers.catalogosAtencionaClientes


import com.ahorraseguros.mx.logskotlinmark43.models.catalogosAtencionaClientes.EstadoSolicitudEndosoModelsLogs
import com.ahorraseguros.mx.logskotlinmark43.repositories.catalogosAtencionaClientes.EstadoSolicitudEndosoRepository
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.web.bind.annotation.*
import javax.validation.Valid

@CrossOrigin
@RestController
@RequestMapping("/v1/estadoSolicitudEndoso")
class EstadoSolicitudEndosoController {
    @Autowired
    lateinit var estadoSolicitudEndosoRepository: EstadoSolicitudEndosoRepository

    @GetMapping
    fun findAll():MutableList<EstadoSolicitudEndosoModelsLogs> = estadoSolicitudEndosoRepository.findAll()

    @PostMapping
    fun create(@Valid @RequestBody nuevoLog: EstadoSolicitudEndosoModelsLogs): EstadoSolicitudEndosoModelsLogs = estadoSolicitudEndosoRepository.save(nuevoLog)
}