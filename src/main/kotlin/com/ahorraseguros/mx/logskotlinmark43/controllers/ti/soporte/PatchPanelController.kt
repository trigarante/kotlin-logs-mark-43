package com.ahorraseguros.mx.logskotlinmark43.controllers.ti.soporte

import com.ahorraseguros.mx.logskotlinmark43.models.ti.soporte.PatchPanelModelsLogs
import com.ahorraseguros.mx.logskotlinmark43.repositories.ti.soporte.PatchPanelRepository
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.web.bind.annotation.*
import javax.validation.Valid

@CrossOrigin
@RestController
@RequestMapping("/v1/patchPanel")
class PatchPanelController {
    @Autowired
    lateinit var patchPanelRepository: PatchPanelRepository

    @GetMapping
    fun findAll():MutableList<PatchPanelModelsLogs> = patchPanelRepository.findAll()

    @PostMapping
    fun create(@Valid @RequestBody nuevoLog: PatchPanelModelsLogs): PatchPanelModelsLogs = patchPanelRepository.save(nuevoLog)
}