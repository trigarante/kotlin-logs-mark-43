package com.ahorraseguros.mx.logskotlinmark43.controllers.ventaNueva

import com.ahorraseguros.mx.logskotlinmark43.models.ventaNueva.ClienteModelsLogs
import com.ahorraseguros.mx.logskotlinmark43.repositories.ventaNueva.ClienteRepository
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.web.bind.annotation.*
import javax.validation.Valid

@CrossOrigin
@RestController
@RequestMapping("/v1/cliente")
class ClienteController {

    @Autowired
    lateinit var clienteRepository: ClienteRepository

    @GetMapping
    fun findAll(): MutableList<ClienteModelsLogs> = clienteRepository.findAll()


    @PostMapping
    fun create(@Valid @RequestBody nuevoLog: ClienteModelsLogs): ClienteModelsLogs = clienteRepository.save(nuevoLog)
}

