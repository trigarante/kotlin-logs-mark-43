package com.ahorraseguros.mx.logskotlinmark43.controllers.comercial


import com.ahorraseguros.mx.logskotlinmark43.models.comercial.UrlSociosModelsLogs
import com.ahorraseguros.mx.logskotlinmark43.repositories.comercial.urlSociosRepository

import org.springframework.beans.factory.annotation.Autowired
import org.springframework.web.bind.annotation.*
import javax.validation.Valid

@CrossOrigin
@RestController
@RequestMapping("/v1/urlSocios")
class UrlSociosController {
    @Autowired
    lateinit var urlsociosRepository: urlSociosRepository

    @GetMapping
    fun findAll():MutableList<UrlSociosModelsLogs> = urlsociosRepository.findAll()

    @PostMapping
    fun create(@Valid @RequestBody nuevoLog: UrlSociosModelsLogs): UrlSociosModelsLogs = urlsociosRepository.save(nuevoLog)
}