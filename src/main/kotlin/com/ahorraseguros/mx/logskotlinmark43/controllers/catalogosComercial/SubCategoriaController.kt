package com.ahorraseguros.mx.logskotlinmark43.controllers.catalogosComercial


import com.ahorraseguros.mx.logskotlinmark43.models.catalogosComercial.SubCategoriaModelsLogs
import com.ahorraseguros.mx.logskotlinmark43.repositories.catalogosComercial.SubCategoriaRepository
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.web.bind.annotation.*
import javax.validation.Valid

@CrossOrigin
@RestController
@RequestMapping("/v1/subCategoria")
class SubCategoriaController {
    @Autowired
    lateinit var subCategoriaRepository: SubCategoriaRepository

    @GetMapping
    fun findAll():MutableList<SubCategoriaModelsLogs> = subCategoriaRepository.findAll()

    @PostMapping
    fun create(@Valid @RequestBody nuevoLog: SubCategoriaModelsLogs): SubCategoriaModelsLogs = subCategoriaRepository.save(nuevoLog)
}