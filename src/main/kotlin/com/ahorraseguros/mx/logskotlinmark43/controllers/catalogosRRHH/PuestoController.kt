package com.ahorraseguros.mx.logskotlinmark43.controllers.catalogosRRHH

import com.ahorraseguros.mx.logskotlinmark43.models.catalogosRRHH.PuestoModelsLogs
import com.ahorraseguros.mx.logskotlinmark43.repositories.catalogosRRHH.PuestoRepository
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.web.bind.annotation.*
import javax.validation.Valid

@CrossOrigin
@RestController
@RequestMapping("/v1/puestos")
class PuestoController {

    @Autowired
    lateinit var puestoRepository: PuestoRepository

    @GetMapping
    fun findAll():MutableList<PuestoModelsLogs> = puestoRepository.findAll()

    @PostMapping
    fun create(@Valid @RequestBody nuevoLog: PuestoModelsLogs): PuestoModelsLogs = puestoRepository.save(nuevoLog)
}