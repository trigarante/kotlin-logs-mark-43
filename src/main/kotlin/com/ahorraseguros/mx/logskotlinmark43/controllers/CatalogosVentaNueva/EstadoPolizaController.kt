package com.ahorraseguros.mx.logskotlinmark43.controllers.CatalogosVentaNueva

import com.ahorraseguros.mx.logskotlinmark43.models.catalogosVentaNueva.EstadoPolizaModelsLogs
import com.ahorraseguros.mx.logskotlinmark43.repositories.catalogosVentaNueva.EstadoPolizaRepository
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.web.bind.annotation.*
import javax.validation.Valid

@CrossOrigin
@RestController
@RequestMapping("/v1/estadoPoliza")
class EstadoPolizaController {
    
    @Autowired
    lateinit var estadoPolizaRepository: EstadoPolizaRepository

    @GetMapping
    fun findAll():MutableList<EstadoPolizaModelsLogs> = estadoPolizaRepository.findAll()

    @PostMapping
    fun create(@Valid @RequestBody nuevoLog: EstadoPolizaModelsLogs): EstadoPolizaModelsLogs = estadoPolizaRepository.save(nuevoLog)
}