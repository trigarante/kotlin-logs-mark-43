package com.ahorraseguros.mx.logskotlinmark43.controllers.CatalogosVentaNueva

import com.ahorraseguros.mx.logskotlinmark43.models.catalogosVentaNueva.EstadoSolicitudModelsLogs
import com.ahorraseguros.mx.logskotlinmark43.repositories.catalogosVentaNueva.EstadoSolicitudRepository
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.web.bind.annotation.*
import javax.validation.Valid

@CrossOrigin
@RestController
@RequestMapping("/v1/estadoSolicitud")
class EstadoSolicitudController {
    @Autowired
    lateinit var estadoSolicitudRepository: EstadoSolicitudRepository

    @GetMapping
    fun findAll():MutableList<EstadoSolicitudModelsLogs> = estadoSolicitudRepository.findAll()

    @PostMapping
    fun create(@Valid @RequestBody nuevoLog: EstadoSolicitudModelsLogs): EstadoSolicitudModelsLogs = estadoSolicitudRepository.save(nuevoLog)
}