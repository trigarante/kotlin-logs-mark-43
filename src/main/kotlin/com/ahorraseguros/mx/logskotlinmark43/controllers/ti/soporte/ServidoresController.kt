package com.ahorraseguros.mx.logskotlinmark43.controllers.ti.soporte


import com.ahorraseguros.mx.logskotlinmark43.models.ti.soporte.ServidoresModelsLogs
import com.ahorraseguros.mx.logskotlinmark43.repositories.ti.soporte.ServidoresRepository
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.web.bind.annotation.*
import javax.validation.Valid

@CrossOrigin
@RestController
@RequestMapping("/v1/servidores")
class ServidoresController {
    @Autowired
    lateinit var servidoresRepository: ServidoresRepository

    @GetMapping
    fun findAll():MutableList<ServidoresModelsLogs> = servidoresRepository.findAll()

    @PostMapping
    fun create(@Valid @RequestBody nuevoLog: ServidoresModelsLogs): ServidoresModelsLogs = servidoresRepository.save(nuevoLog)
}