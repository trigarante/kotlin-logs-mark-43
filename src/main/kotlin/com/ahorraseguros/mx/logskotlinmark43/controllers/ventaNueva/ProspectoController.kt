package com.ahorraseguros.mx.logskotlinmark43.controllers.ventaNueva

import com.ahorraseguros.mx.logskotlinmark43.models.ventaNueva.ProspectoModelsLogs
import com.ahorraseguros.mx.logskotlinmark43.repositories.ventaNueva.ProspectoRepository
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.web.bind.annotation.*
import javax.validation.Valid

@CrossOrigin
@RestController
@RequestMapping("/v1/prospecto")
class ProspectoController {
    @Autowired
    lateinit var prospectoRepository: ProspectoRepository

    @GetMapping
    fun findAll():MutableList<ProspectoModelsLogs> = prospectoRepository.findAll()

    @PostMapping
    fun create(@Valid @RequestBody nuevoLog: ProspectoModelsLogs): ProspectoModelsLogs = prospectoRepository.save(nuevoLog)
}