package com.ahorraseguros.mx.logskotlinmark43.controllers.rh

import com.ahorraseguros.mx.logskotlinmark43.models.rh.VacantesModelsLogs
import com.ahorraseguros.mx.logskotlinmark43.repositories.rh.VacantesRepository
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.web.bind.annotation.*
import javax.validation.Valid

@CrossOrigin
@RestController
@RequestMapping("v1/vacantes")

class VacantesController {

    @Autowired
    lateinit var vacantesRepository: VacantesRepository

    @GetMapping()
    fun findAll(): MutableList<VacantesModelsLogs> = vacantesRepository.findAll()

    @PostMapping
    fun create(@Valid @RequestBody vacantesModelsLogs: VacantesModelsLogs): VacantesModelsLogs {
        println(vacantesModelsLogs)
        return  vacantesRepository.save(vacantesModelsLogs)
    }
}
