package com.ahorraseguros.mx.logskotlinmark43.controllers.comercial



import com.ahorraseguros.mx.logskotlinmark43.models.comercial.PresupuestoSocioModelsLogs
import com.ahorraseguros.mx.logskotlinmark43.repositories.comercial.PresupuestoSocioRepository
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.web.bind.annotation.*
import javax.validation.Valid

@CrossOrigin
@RestController
@RequestMapping("/v1/presupuestoSocio")
class PresupuestoSocioController {
    @Autowired
    lateinit var presupuestosSocioRepository: PresupuestoSocioRepository

    @GetMapping
    fun findAll():MutableList<PresupuestoSocioModelsLogs> = presupuestosSocioRepository.findAll()

    @PostMapping
    fun create(@Valid @RequestBody nuevoLog: PresupuestoSocioModelsLogs): PresupuestoSocioModelsLogs = presupuestosSocioRepository.save(nuevoLog)
}