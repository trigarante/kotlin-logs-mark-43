package com.ahorraseguros.mx.logskotlinmark43.controllers.ti.soporte

import com.ahorraseguros.mx.logskotlinmark43.models.ti.soporte.DiademaModelsLogs
import com.ahorraseguros.mx.logskotlinmark43.repositories.ti.soporte.DiademaRepository
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.web.bind.annotation.*
import javax.validation.Valid

@CrossOrigin
@RestController
@RequestMapping("/v1/diadema")
class DiademaController {
    @Autowired
    lateinit var diademaRepository: DiademaRepository

    @GetMapping
    fun findAll():MutableList<DiademaModelsLogs> = diademaRepository.findAll()

    @PostMapping
    fun create(@Valid @RequestBody nuevoLog: DiademaModelsLogs): DiademaModelsLogs = diademaRepository.save(nuevoLog)
}