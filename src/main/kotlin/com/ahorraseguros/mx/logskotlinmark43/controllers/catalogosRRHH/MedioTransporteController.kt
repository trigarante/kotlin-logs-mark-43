package com.ahorraseguros.mx.logskotlinmark43.controllers.catalogosRRHH

import com.ahorraseguros.mx.logskotlinmark43.models.catalogosRRHH.MedioTransporteModelsLogs
import com.ahorraseguros.mx.logskotlinmark43.repositories.catalogosRRHH.MedioTransporteRepository
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.web.bind.annotation.*
import javax.validation.Valid

@CrossOrigin
@RestController
@RequestMapping("/v1/medio-transporte")
class MedioTransporteController {

    @Autowired
    lateinit var medioTransporteRepository: MedioTransporteRepository

    @GetMapping
    fun findAll():MutableList<MedioTransporteModelsLogs> = medioTransporteRepository.findAll()

    @PostMapping
    fun create(@Valid @RequestBody nuevoLog: MedioTransporteModelsLogs): MedioTransporteModelsLogs =
            medioTransporteRepository.save(nuevoLog)
}