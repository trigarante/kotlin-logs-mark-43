package com.ahorraseguros.mx.logskotlinmark43.controllers.catalogosRRHH

import com.ahorraseguros.mx.logskotlinmark43.models.catalogosRRHH.PuestoTipoModelsLogs
import com.ahorraseguros.mx.logskotlinmark43.repositories.catalogosRRHH.PuestoTipoRepository
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.web.bind.annotation.*
import javax.validation.Valid

@CrossOrigin
@RestController
@RequestMapping("/v1/tipo-puesto")
class PuestoTipoController {

    @Autowired
    lateinit var puestoTipoRepository: PuestoTipoRepository

    @GetMapping
    fun findAll():MutableList<PuestoTipoModelsLogs> = puestoTipoRepository.findAll()

    @PostMapping
    fun create(@Valid @RequestBody nuevoLog: PuestoTipoModelsLogs): PuestoTipoModelsLogs =
            puestoTipoRepository.save(nuevoLog)
}