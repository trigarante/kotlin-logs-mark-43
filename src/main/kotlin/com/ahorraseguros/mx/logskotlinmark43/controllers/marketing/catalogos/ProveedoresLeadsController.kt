package com.ahorraseguros.mx.logskotlinmark43.controllers.marketing.catalogos


import com.ahorraseguros.mx.logskotlinmark43.models.marketing.catalogos.ProveedoresLeadsModelsLogs
import com.ahorraseguros.mx.logskotlinmark43.repositories.marketing.catalogos.ProveedoresLeadsRepository
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.web.bind.annotation.*
import javax.validation.Valid

@CrossOrigin
@RestController
@RequestMapping("/v1/proveedoresLeads")
class ProveedoresLeadsController {

    @Autowired
    lateinit var proveedoresLeadsRepository: ProveedoresLeadsRepository

    @GetMapping
    fun findAll():MutableList<ProveedoresLeadsModelsLogs> = proveedoresLeadsRepository.findAll()

    @PostMapping
    fun create(@Valid @RequestBody nuevoLog: ProveedoresLeadsModelsLogs): ProveedoresLeadsModelsLogs = proveedoresLeadsRepository.save(nuevoLog)
}