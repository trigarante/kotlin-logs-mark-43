package com.ahorraseguros.mx.logskotlinmark43.controllers.CatalogosVentaNueva


import com.ahorraseguros.mx.logskotlinmark43.models.catalogosVentaNueva.FlujoSolicitudModelsLogs
import com.ahorraseguros.mx.logskotlinmark43.repositories.catalogosVentaNueva.FlujoSolicitudRepository
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.web.bind.annotation.*
import javax.validation.Valid

@CrossOrigin
@RestController
@RequestMapping("/v1/flujoSolicitud")
class FlujoSolicitudController {
    @Autowired
    lateinit var flujoSolicitudRepository: FlujoSolicitudRepository

    @GetMapping
    fun findAll():MutableList<FlujoSolicitudModelsLogs> = flujoSolicitudRepository.findAll()

    @PostMapping
    fun create(@Valid @RequestBody nuevoLog: FlujoSolicitudModelsLogs): FlujoSolicitudModelsLogs = flujoSolicitudRepository.save(nuevoLog)
}
