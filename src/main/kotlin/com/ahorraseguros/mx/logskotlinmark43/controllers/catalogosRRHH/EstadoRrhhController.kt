package com.ahorraseguros.mx.logskotlinmark43.controllers.catalogosRRHH

import com.ahorraseguros.mx.logskotlinmark43.models.catalogosRRHH.EstadoRrhhModelsLogs
import com.ahorraseguros.mx.logskotlinmark43.repositories.catalogosRRHH.EstadoRrhhRepository
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.web.bind.annotation.*
import javax.validation.Valid

@CrossOrigin
@RestController
@RequestMapping("/v1/estadosRrhh")
class EstadoRrhhController {

    @Autowired
    lateinit var estadoRrhhRepository: EstadoRrhhRepository


    @GetMapping
    fun findAll():MutableList<EstadoRrhhModelsLogs> = estadoRrhhRepository.findAll()


    @PostMapping
    fun create(@Valid @RequestBody nuevoLog: EstadoRrhhModelsLogs): EstadoRrhhModelsLogs {
        return estadoRrhhRepository.save(nuevoLog)
    }
}