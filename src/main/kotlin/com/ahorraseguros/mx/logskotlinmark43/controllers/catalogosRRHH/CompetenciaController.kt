package com.ahorraseguros.mx.logskotlinmark43.controllers.catalogosRRHH

import com.ahorraseguros.mx.logskotlinmark43.models.catalogosRRHH.CompetenciaModelsLogs
import com.ahorraseguros.mx.logskotlinmark43.repositories.catalogosRRHH.CompetenciaRepository
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.web.bind.annotation.*
import javax.validation.Valid

@CrossOrigin
@RestController
@RequestMapping("/v1/competencias")
class CompetenciaController {

    @Autowired
    lateinit var competenciaRepository: CompetenciaRepository

    @GetMapping
    fun findAll():MutableList<CompetenciaModelsLogs> = competenciaRepository.findAll()

    @PostMapping
    fun create(@Valid @RequestBody nuevoLog: CompetenciaModelsLogs): CompetenciaModelsLogs = competenciaRepository.save(nuevoLog)
}