package com.ahorraseguros.mx.logskotlinmark43.controllers.CatalogosVentaNueva

import com.ahorraseguros.mx.logskotlinmark43.models.catalogosVentaNueva.TarjetasBancoModelsLogs
import com.ahorraseguros.mx.logskotlinmark43.repositories.catalogosVentaNueva.TarjetaBancoRepository
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.web.bind.annotation.*
import javax.validation.Valid

@CrossOrigin
@RestController
@RequestMapping("/v1/tarjetaBanco")
class TarjetaBancoController {
    @Autowired
    lateinit var tarjetaBancoRepository: TarjetaBancoRepository

    @GetMapping
    fun findAll():MutableList<TarjetasBancoModelsLogs> = tarjetaBancoRepository.findAll()

    @PostMapping
    fun create(@Valid @RequestBody nuevoLog: TarjetasBancoModelsLogs): TarjetasBancoModelsLogs = tarjetaBancoRepository.save(nuevoLog)
}