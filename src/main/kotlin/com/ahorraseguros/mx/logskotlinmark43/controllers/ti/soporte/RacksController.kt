package com.ahorraseguros.mx.logskotlinmark43.controllers.ti.soporte


import com.ahorraseguros.mx.logskotlinmark43.models.ti.soporte.RacksModelsLogs
import com.ahorraseguros.mx.logskotlinmark43.repositories.ti.soporte.RacksRepository

import org.springframework.beans.factory.annotation.Autowired
import org.springframework.web.bind.annotation.*
import javax.validation.Valid

@CrossOrigin
@RestController
@RequestMapping("/v1/racks")
class RacksController {
    @Autowired
    lateinit var racksRepository: RacksRepository

    @GetMapping
    fun findAll():MutableList<RacksModelsLogs> = racksRepository.findAll()

    @PostMapping
    fun create(@Valid @RequestBody nuevoLog: RacksModelsLogs): RacksModelsLogs = racksRepository.save(nuevoLog)
}