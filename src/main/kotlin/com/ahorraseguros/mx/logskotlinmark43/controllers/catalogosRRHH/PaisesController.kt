package com.ahorraseguros.mx.logskotlinmark43.controllers.catalogosRRHH

import com.ahorraseguros.mx.logskotlinmark43.models.catalogosRRHH.PaisesModelsLogs
import com.ahorraseguros.mx.logskotlinmark43.repositories.catalogosRRHH.PaisesRepository
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.web.bind.annotation.*
import javax.validation.Valid

@CrossOrigin
@RestController
@RequestMapping("/v1/paises")
class PaisesController {

    @Autowired
    lateinit var paisesRepository: PaisesRepository

    @GetMapping
    fun findAll():MutableList<PaisesModelsLogs> = paisesRepository.findAll()

    @PostMapping
    fun create(@Valid @RequestBody nuevoLog: PaisesModelsLogs): PaisesModelsLogs =
            paisesRepository.save(nuevoLog)
}