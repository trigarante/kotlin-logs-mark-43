package com.ahorraseguros.mx.logskotlinmark43.controllers.catalogosAtencionaClientes

import com.ahorraseguros.mx.logskotlinmark43.models.catalogosAtencionaClientes.TipoEndosoModelsLogs
import com.ahorraseguros.mx.logskotlinmark43.repositories.catalogosAtencionaClientes.TipoEndosoRepository
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.web.bind.annotation.*
import javax.validation.Valid

@CrossOrigin
@RestController
@RequestMapping("/v1/tipoEndoso")
class TipoEndosoController {
    @Autowired
    lateinit var tipoEndosoRepository: TipoEndosoRepository

    @GetMapping
    fun findAll():MutableList<TipoEndosoModelsLogs> = tipoEndosoRepository.findAll()

    @PostMapping
    fun create(@Valid @RequestBody nuevoLog: TipoEndosoModelsLogs): TipoEndosoModelsLogs = tipoEndosoRepository.save(nuevoLog)
}