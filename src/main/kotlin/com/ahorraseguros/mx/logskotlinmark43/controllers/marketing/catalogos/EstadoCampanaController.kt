package com.ahorraseguros.mx.logskotlinmark43.controllers.marketing.catalogos

import com.ahorraseguros.mx.logskotlinmark43.models.marketing.catalogos.EstadoCampanaModelsLogs
import com.ahorraseguros.mx.logskotlinmark43.repositories.marketing.catalogos.EstadoCampanaRepository
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.web.bind.annotation.*
import javax.validation.Valid
@CrossOrigin
@RestController
@RequestMapping("/v1/estadoCampana")                                                                                             
class EstadoCampanaController {
    @Autowired
    lateinit var estadoCampanaRepository: EstadoCampanaRepository

    @GetMapping
    fun findAll():MutableList<EstadoCampanaModelsLogs> = estadoCampanaRepository.findAll()

    @PostMapping
    fun create(@Valid @RequestBody nuevoLog: EstadoCampanaModelsLogs): EstadoCampanaModelsLogs = estadoCampanaRepository.save(nuevoLog)
}