package com.ahorraseguros.mx.logskotlinmark43.controllers.catalogosComercial


import com.ahorraseguros.mx.logskotlinmark43.models.catalogosComercial.TipoCompetenciaModelsLogs
import com.ahorraseguros.mx.logskotlinmark43.repositories.catalogosComercial.TipoCompetenciaRepository
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.web.bind.annotation.*
import javax.validation.Valid

@CrossOrigin
@RestController
@RequestMapping("/v1/tipoCompetencia")
class TipoCompetenciaController {
    @Autowired
    lateinit var tipoCompetenciaRepository: TipoCompetenciaRepository

    @GetMapping
    fun findAll():MutableList<TipoCompetenciaModelsLogs> = tipoCompetenciaRepository.findAll()

    @PostMapping
    fun create(@Valid @RequestBody nuevoLog: TipoCompetenciaModelsLogs): TipoCompetenciaModelsLogs = tipoCompetenciaRepository.save(nuevoLog)
}