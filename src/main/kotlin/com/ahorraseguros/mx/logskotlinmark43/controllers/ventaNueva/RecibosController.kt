package com.ahorraseguros.mx.logskotlinmark43.controllers.ventaNueva

import com.ahorraseguros.mx.logskotlinmark43.models.ventaNueva.RecibosModelsLogs
import com.ahorraseguros.mx.logskotlinmark43.repositories.ventaNueva.RecibosRepository
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.web.bind.annotation.*
import javax.validation.Valid

@CrossOrigin
@RestController
@RequestMapping("/v1/recibos")
class RecibosController {
    @Autowired
    lateinit var recibosRepository: RecibosRepository

    @GetMapping
    fun findAll():MutableList<RecibosModelsLogs> = recibosRepository.findAll()

    @PostMapping
    fun create(@Valid @RequestBody nuevoLog: MutableList<RecibosModelsLogs>): MutableList<RecibosModelsLogs> = recibosRepository.saveAll(nuevoLog)
}