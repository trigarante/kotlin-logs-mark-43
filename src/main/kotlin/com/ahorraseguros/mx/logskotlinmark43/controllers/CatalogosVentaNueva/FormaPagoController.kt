package com.ahorraseguros.mx.logskotlinmark43.controllers.CatalogosVentaNueva

import com.ahorraseguros.mx.logskotlinmark43.models.catalogosVentaNueva.FormaPagoModelsLogs
import com.ahorraseguros.mx.logskotlinmark43.repositories.catalogosVentaNueva.FormaPagoRepository
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.web.bind.annotation.*
import javax.validation.Valid

@CrossOrigin
@RestController
@RequestMapping("/v1/formaPago")
class FormaPagoController {
    @Autowired
    lateinit var formaPagoRepository: FormaPagoRepository

    @GetMapping
    fun findAll():MutableList<FormaPagoModelsLogs> = formaPagoRepository.findAll()

    @PostMapping
    fun create(@Valid @RequestBody nuevoLog: FormaPagoModelsLogs): FormaPagoModelsLogs = formaPagoRepository.save(nuevoLog)
}