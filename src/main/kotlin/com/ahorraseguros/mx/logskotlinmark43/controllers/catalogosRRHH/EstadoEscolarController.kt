package com.ahorraseguros.mx.logskotlinmark43.controllers.catalogosRRHH

import com.ahorraseguros.mx.logskotlinmark43.models.catalogosRRHH.EstadoEscolaridadModelsLogs
import com.ahorraseguros.mx.logskotlinmark43.repositories.catalogosRRHH.EstadoEscolaridadRepository
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.web.bind.annotation.*
import javax.validation.Valid

@CrossOrigin
@RestController
@RequestMapping("/v1/estado-escolar")
class EstadoEscolarController {

    @Autowired
    lateinit var estadoEscolaridadRepository: EstadoEscolaridadRepository

    @GetMapping
    fun findAll():MutableList<EstadoEscolaridadModelsLogs> = estadoEscolaridadRepository.findAll()

    @PostMapping
    fun create(@Valid @RequestBody nuevoLog: EstadoEscolaridadModelsLogs): EstadoEscolaridadModelsLogs =
            estadoEscolaridadRepository.save(nuevoLog)
}