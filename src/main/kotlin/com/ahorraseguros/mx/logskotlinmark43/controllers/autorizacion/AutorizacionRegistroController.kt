package com.ahorraseguros.mx.logskotlinmark43.controllers.autorizacion

import com.ahorraseguros.mx.logskotlinmark43.models.autorizacion.AutorizacionRegistroLogs
import com.ahorraseguros.mx.logskotlinmark43.repositories.autorizacion.AutorizacionRegistroRepository
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.web.bind.annotation.*
import javax.validation.Valid

@CrossOrigin
@RestController
@RequestMapping("/v1/autorizacionRegistro")
class AutorizacionRegistroController {
    @Autowired
    lateinit var autorizacionRegistroRepository: AutorizacionRegistroRepository

    @GetMapping
    fun findAll():MutableList<AutorizacionRegistroLogs> = autorizacionRegistroRepository.findAll()

    @PostMapping
    fun create(@Valid @RequestBody nuevoLog: AutorizacionRegistroLogs): AutorizacionRegistroLogs = autorizacionRegistroRepository.save(nuevoLog)
}