package com.ahorraseguros.mx.logskotlinmark43.controllers.ventaNueva

import com.ahorraseguros.mx.logskotlinmark43.models.ventaNueva.ColaSolicitudesModelsLogs
import com.ahorraseguros.mx.logskotlinmark43.repositories.ventaNueva.ColaSolicitudesRepository
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.web.bind.annotation.*
import javax.validation.Valid

@CrossOrigin
@RestController
@RequestMapping("/v1/colaSolicitudes")
class ColaSolicitudesController {
    @Autowired
    lateinit var colaSolicitudesRepository: ColaSolicitudesRepository

    @GetMapping
    fun findAll():MutableList<ColaSolicitudesModelsLogs> = colaSolicitudesRepository.findAll()

    @PostMapping
    fun create(@Valid @RequestBody nuevoLog: ColaSolicitudesModelsLogs): ColaSolicitudesModelsLogs = colaSolicitudesRepository.save(nuevoLog)
}