package com.ahorraseguros.mx.logskotlinmark43.controllers.rh

import com.ahorraseguros.mx.logskotlinmark43.models.rh.EmpleadoModelsLogs
import com.ahorraseguros.mx.logskotlinmark43.repositories.rh.EmpleadoRepository
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.web.bind.annotation.*
import javax.validation.Valid

@CrossOrigin
@RestController
@RequestMapping("/v1/empleados")

class EmpleadoController {
    @Autowired
    lateinit var empleadoRepository: EmpleadoRepository

    @GetMapping
    fun findAll():MutableList<EmpleadoModelsLogs> = empleadoRepository.findAll()

    @PostMapping
    fun create(@Valid @RequestBody empleadoModelsLogs: EmpleadoModelsLogs): EmpleadoModelsLogs = empleadoRepository.save(empleadoModelsLogs)
}