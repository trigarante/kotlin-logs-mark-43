package com.ahorraseguros.mx.logskotlinmark43.controllers.ti.soporte

import com.ahorraseguros.mx.logskotlinmark43.models.ti.soporte.TelefonoIpModelsLogs
import com.ahorraseguros.mx.logskotlinmark43.repositories.ti.soporte.TelefonoIpRepository
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.web.bind.annotation.*
import javax.validation.Valid
@CrossOrigin
@RestController
@RequestMapping("/v1/telefonoIp")
class TelefonoIpController {
    @Autowired
    lateinit var telefonoIpRepository: TelefonoIpRepository

    @GetMapping
    fun findAll():MutableList<TelefonoIpModelsLogs> = telefonoIpRepository.findAll()

    @PostMapping
    fun create(@Valid @RequestBody nuevoLog: TelefonoIpModelsLogs): TelefonoIpModelsLogs = telefonoIpRepository.save(nuevoLog)
}