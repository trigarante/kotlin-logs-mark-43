package com.ahorraseguros.mx.logskotlinmark43.controllers.CatalogosVentaNueva

import com.ahorraseguros.mx.logskotlinmark43.models.catalogosVentaNueva.EstadoInspeccionModelsLogs
import com.ahorraseguros.mx.logskotlinmark43.repositories.catalogosVentaNueva.EstadoInspeccionRepository
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.web.bind.annotation.*
import javax.validation.Valid

@CrossOrigin
@RestController
@RequestMapping("/v1/estadoInspeccion")
class EstadoInspeccionController {
    @Autowired
    lateinit var estadoInspeccionRepository: EstadoInspeccionRepository

    @GetMapping
    fun findAll():MutableList<EstadoInspeccionModelsLogs> = estadoInspeccionRepository.findAll()

    @PostMapping
    fun create(@Valid @RequestBody nuevoLog: EstadoInspeccionModelsLogs): EstadoInspeccionModelsLogs = estadoInspeccionRepository.save(nuevoLog)
}