package com.ahorraseguros.mx.logskotlinmark43.controllers.catalogosComercial


import com.ahorraseguros.mx.logskotlinmark43.models.catalogosComercial.TipoRamoModelsLogs
import com.ahorraseguros.mx.logskotlinmark43.repositories.catalogosComercial.TipoRamoRepository
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.web.bind.annotation.*
import javax.validation.Valid

@CrossOrigin
@RestController
@RequestMapping("/v1/tipoRamo")
class TipoRamoController {
    @Autowired
    lateinit var tipoRamoRepository: TipoRamoRepository

    @GetMapping
    fun findAll():MutableList<TipoRamoModelsLogs> = tipoRamoRepository.findAll()

    @PostMapping
    fun create(@Valid @RequestBody nuevoLog: TipoRamoModelsLogs): TipoRamoModelsLogs = tipoRamoRepository.save(nuevoLog)
}