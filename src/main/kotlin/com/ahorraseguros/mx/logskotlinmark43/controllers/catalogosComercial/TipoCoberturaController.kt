package com.ahorraseguros.mx.logskotlinmark43.controllers.catalogosComercial

import com.ahorraseguros.mx.logskotlinmark43.models.catalogosComercial.TipoCoberturaModelsLogs
import com.ahorraseguros.mx.logskotlinmark43.repositories.catalogosComercial.TipoCoberturaRepository
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.web.bind.annotation.*
import javax.validation.Valid

@CrossOrigin
@RestController
@RequestMapping("/v1/tipoCobertura")
class TipoCoberturaController {
    @Autowired
    lateinit var tipoCoberturaRepository: TipoCoberturaRepository

    @GetMapping
    fun findAll():MutableList<TipoCoberturaModelsLogs> = tipoCoberturaRepository.findAll()

    @PostMapping
    fun create(@Valid @RequestBody nuevoLog: TipoCoberturaModelsLogs): TipoCoberturaModelsLogs = tipoCoberturaRepository.save(nuevoLog)
}