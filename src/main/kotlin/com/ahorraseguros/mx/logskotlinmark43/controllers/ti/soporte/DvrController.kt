package com.ahorraseguros.mx.logskotlinmark43.controllers.ti.soporte


import com.ahorraseguros.mx.logskotlinmark43.models.ti.soporte.DvrModelsLogs
import com.ahorraseguros.mx.logskotlinmark43.repositories.ti.soporte.DvrRepository
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.web.bind.annotation.*
import javax.validation.Valid

@CrossOrigin
@RestController
@RequestMapping("/v1/dvr")
class DvrController {
    @Autowired
    lateinit var dvrRepository: DvrRepository

    @GetMapping
    fun findAll():MutableList<DvrModelsLogs> = dvrRepository.findAll()

    @PostMapping
    fun create(@Valid @RequestBody nuevoLog: DvrModelsLogs): DvrModelsLogs = dvrRepository.save(nuevoLog)
}