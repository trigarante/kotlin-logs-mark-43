package com.ahorraseguros.mx.logskotlinmark43.controllers.ti.soporte


import com.ahorraseguros.mx.logskotlinmark43.models.ti.soporte.DiscoDuroExternoModelsLogs
import com.ahorraseguros.mx.logskotlinmark43.repositories.ti.soporte.DiscoDuroExternoRepository
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.web.bind.annotation.*
import javax.validation.Valid

@CrossOrigin
@RestController
@RequestMapping("/v1/discoDuroExterno")
class DiscoDuroExternoController {
    @Autowired
    lateinit var discoDuroExternoRepository: DiscoDuroExternoRepository

    @GetMapping
    fun findAll():MutableList<DiscoDuroExternoModelsLogs> = discoDuroExternoRepository.findAll()

    @PostMapping
    fun create(@Valid @RequestBody nuevoLog: DiscoDuroExternoModelsLogs): DiscoDuroExternoModelsLogs = discoDuroExternoRepository.save(nuevoLog)
}