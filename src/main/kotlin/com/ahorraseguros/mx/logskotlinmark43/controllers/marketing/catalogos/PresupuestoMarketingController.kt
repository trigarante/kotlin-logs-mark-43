package com.ahorraseguros.mx.logskotlinmark43.controllers.marketing.catalogos

import com.ahorraseguros.mx.logskotlinmark43.models.marketing.catalogos.PresuspuestoMarketingModelsLogs
import com.ahorraseguros.mx.logskotlinmark43.repositories.marketing.catalogos.PresupuestoMarketingRepository
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.web.bind.annotation.*
import javax.validation.Valid

@CrossOrigin
@RestController
@RequestMapping("/v1/presuspuestoMarketing")
class PresupuestoMarketingController {
    @Autowired
    lateinit var presupuestoMarketingRepository: PresupuestoMarketingRepository

    @GetMapping
    fun findAll():MutableList<PresuspuestoMarketingModelsLogs> = presupuestoMarketingRepository.findAll()

    @PostMapping
    fun create(@Valid @RequestBody nuevoLog: PresuspuestoMarketingModelsLogs): PresuspuestoMarketingModelsLogs = presupuestoMarketingRepository.save(nuevoLog)
}