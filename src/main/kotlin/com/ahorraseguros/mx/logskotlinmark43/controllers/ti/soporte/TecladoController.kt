package com.ahorraseguros.mx.logskotlinmark43.controllers.ti.soporte

import com.ahorraseguros.mx.logskotlinmark43.models.ti.soporte.TecladoModelsLogs
import com.ahorraseguros.mx.logskotlinmark43.repositories.ti.soporte.TecladoRepository
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.web.bind.annotation.*
import javax.validation.Valid

@CrossOrigin
@RestController
@RequestMapping("/v1/teclado")
class TecladoController {
    @Autowired
    lateinit var tecladoRepository: TecladoRepository

    @GetMapping
    fun findAll():MutableList<TecladoModelsLogs> = tecladoRepository.findAll()

    @PostMapping
    fun create(@Valid @RequestBody nuevoLog: TecladoModelsLogs): TecladoModelsLogs = tecladoRepository.save(nuevoLog)
}