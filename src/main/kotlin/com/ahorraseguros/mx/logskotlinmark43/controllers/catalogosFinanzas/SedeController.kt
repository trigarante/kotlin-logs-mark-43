package com.ahorraseguros.mx.logskotlinmark43.controllers.catalogosFinanzas

import com.ahorraseguros.mx.logskotlinmark43.models.catalogosFinanzas.SedeModelsLogs
import com.ahorraseguros.mx.logskotlinmark43.repositories.catalogosFinanzas.SedeRepository
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.web.bind.annotation.*
import javax.validation.Valid

@CrossOrigin
@RestController
@RequestMapping("/v1/sede")
class SedeController {
    @Autowired
    lateinit var sedeRepository: SedeRepository

    @GetMapping
    fun findAll():MutableList<SedeModelsLogs> = sedeRepository.findAll()

    @PostMapping
    fun create(@Valid @RequestBody nuevoLog: SedeModelsLogs): SedeModelsLogs = sedeRepository.save(nuevoLog)
}