package com.ahorraseguros.mx.logskotlinmark43.controllers.CatalogosVentaNueva

import com.ahorraseguros.mx.logskotlinmark43.models.catalogosVentaNueva.EstadoReciboModelsLogs
import com.ahorraseguros.mx.logskotlinmark43.repositories.catalogosVentaNueva.EstadoReciboRepository
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.web.bind.annotation.*
import javax.validation.Valid

@CrossOrigin
@RestController
@RequestMapping("/v1/estadoRecibo")
class EstadoReciboController {

    @Autowired
    lateinit var estadoReciboRepository: EstadoReciboRepository

    @GetMapping
    fun findAll():MutableList<EstadoReciboModelsLogs> = estadoReciboRepository.findAll()

    @PostMapping
    fun create(@Valid @RequestBody nuevoLog: EstadoReciboModelsLogs): EstadoReciboModelsLogs = estadoReciboRepository.save(nuevoLog)
}