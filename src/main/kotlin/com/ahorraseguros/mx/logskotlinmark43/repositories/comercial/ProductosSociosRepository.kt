package com.ahorraseguros.mx.logskotlinmark43.repositories.comercial


import com.ahorraseguros.mx.logskotlinmark43.models.comercial.ProductosSocioModelsLogs
import org.springframework.data.jpa.repository.JpaRepository

interface ProductosSociosRepository: JpaRepository<ProductosSocioModelsLogs, Long> {
}