package com.ahorraseguros.mx.logskotlinmark43.repositories.ti

import com.ahorraseguros.mx.logskotlinmark43.models.ti.ExtensionesModelsLogs
import org.springframework.data.jpa.repository.JpaRepository

interface ExtensionesRepository: JpaRepository<ExtensionesModelsLogs, Long>