package com.ahorraseguros.mx.logskotlinmark43.repositories.ti.soporte

import com.ahorraseguros.mx.logskotlinmark43.models.ti.soporte.MouseModelsLogs
import org.springframework.data.jpa.repository.JpaRepository

interface MouseRepository: JpaRepository<MouseModelsLogs, Long> {
}