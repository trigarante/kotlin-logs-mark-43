package com.ahorraseguros.mx.logskotlinmark43.repositories.marketing


import com.ahorraseguros.mx.logskotlinmark43.models.marketing.CampanaModelsLogs
import org.springframework.data.jpa.repository.JpaRepository

interface CampanaRepository: JpaRepository<CampanaModelsLogs, Long> {
}