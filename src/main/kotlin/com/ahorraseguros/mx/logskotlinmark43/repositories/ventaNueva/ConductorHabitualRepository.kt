package com.ahorraseguros.mx.logskotlinmark43.repositories.ventaNueva

import com.ahorraseguros.mx.logskotlinmark43.models.ventaNueva.ConductorHabitualModelsLogs
import org.springframework.data.jpa.repository.JpaRepository

interface ConductorHabitualRepository : JpaRepository<ConductorHabitualModelsLogs, Long> {
}