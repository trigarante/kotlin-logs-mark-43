package com.ahorraseguros.mx.logskotlinmark43.repositories.rh

import com.ahorraseguros.mx.logskotlinmark43.models.rh.PrecandidatoModelsLogs
import org.springframework.data.jpa.repository.JpaRepository

interface PrecandidatoRepository: JpaRepository<PrecandidatoModelsLogs, Long>