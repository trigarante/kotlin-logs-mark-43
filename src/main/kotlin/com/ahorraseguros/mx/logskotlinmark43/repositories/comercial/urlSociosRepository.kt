package com.ahorraseguros.mx.logskotlinmark43.repositories.comercial


import com.ahorraseguros.mx.logskotlinmark43.models.comercial.UrlSociosModelsLogs
import org.springframework.data.jpa.repository.JpaRepository

interface urlSociosRepository: JpaRepository<UrlSociosModelsLogs, Long> {
}