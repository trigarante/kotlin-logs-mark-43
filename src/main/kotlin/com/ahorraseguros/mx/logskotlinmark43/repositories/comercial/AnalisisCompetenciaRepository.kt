package com.ahorraseguros.mx.logskotlinmark43.repositories.comercial


import com.ahorraseguros.mx.logskotlinmark43.models.comercial.AnalisisCompetenciaModelsLogs
import org.springframework.data.jpa.repository.JpaRepository

interface AnalisisCompetenciaRepository: JpaRepository<AnalisisCompetenciaModelsLogs, Long> {
}