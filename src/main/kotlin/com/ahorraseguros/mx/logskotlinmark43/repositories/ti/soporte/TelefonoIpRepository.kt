package com.ahorraseguros.mx.logskotlinmark43.repositories.ti.soporte

import com.ahorraseguros.mx.logskotlinmark43.models.ti.soporte.TelefonoIpModelsLogs
import org.springframework.data.jpa.repository.JpaRepository

interface TelefonoIpRepository: JpaRepository<TelefonoIpModelsLogs, Long> {
}