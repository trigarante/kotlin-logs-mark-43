package com.ahorraseguros.mx.logskotlinmark43.repositories.catalogosVentaNueva

import com.ahorraseguros.mx.logskotlinmark43.models.catalogosVentaNueva.EstadoPagoModelLogs
import org.springframework.data.jpa.repository.JpaRepository

interface EstadoPagoRepository : JpaRepository<EstadoPagoModelLogs, Long> {
}