package com.ahorraseguros.mx.logskotlinmark43.repositories.rh

import com.ahorraseguros.mx.logskotlinmark43.models.rh.EmpleadoModelsLogs
import org.springframework.data.jpa.repository.JpaRepository

interface EmpleadoRepository: JpaRepository<EmpleadoModelsLogs, Long>

