package com.ahorraseguros.mx.logskotlinmark43.repositories.ventaNueva

import com.ahorraseguros.mx.logskotlinmark43.models.ventaNueva.ProductoClienteModelsLogs
import org.springframework.data.jpa.repository.JpaRepository

interface ProductoClienteRepository : JpaRepository<ProductoClienteModelsLogs, Long> {
}