package com.ahorraseguros.mx.logskotlinmark43.repositories.catalogosRRHH

import com.ahorraseguros.mx.logskotlinmark43.models.catalogosRRHH.EstadoRrhhModelsLogs
import org.springframework.data.jpa.repository.JpaRepository

interface EstadoRrhhRepository: JpaRepository<EstadoRrhhModelsLogs, Long>