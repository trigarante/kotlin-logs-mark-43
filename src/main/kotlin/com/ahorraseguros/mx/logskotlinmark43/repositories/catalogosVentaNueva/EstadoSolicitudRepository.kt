package com.ahorraseguros.mx.logskotlinmark43.repositories.catalogosVentaNueva

import com.ahorraseguros.mx.logskotlinmark43.models.catalogosVentaNueva.EstadoSolicitudModelsLogs
import org.springframework.data.jpa.repository.JpaRepository

interface EstadoSolicitudRepository : JpaRepository<EstadoSolicitudModelsLogs, Long> {
}