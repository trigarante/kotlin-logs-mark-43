package com.ahorraseguros.mx.logskotlinmark43.repositories.ti.soporte


import com.ahorraseguros.mx.logskotlinmark43.models.ti.soporte.CharolasRackModelsLogs
import org.springframework.data.jpa.repository.JpaRepository

interface CharolasRackRepository: JpaRepository<CharolasRackModelsLogs, Long> {
}