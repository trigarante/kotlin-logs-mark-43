package com.ahorraseguros.mx.logskotlinmark43.repositories.verificacion

import com.ahorraseguros.mx.logskotlinmark43.models.autorizacion.ErroresAutorizacionLogs
import com.ahorraseguros.mx.logskotlinmark43.models.verificacion.ErroresAnexosVModelsLogs
import org.springframework.data.jpa.repository.JpaRepository

interface ErroresAnexosVRepository: JpaRepository<ErroresAnexosVModelsLogs, Long> {
}