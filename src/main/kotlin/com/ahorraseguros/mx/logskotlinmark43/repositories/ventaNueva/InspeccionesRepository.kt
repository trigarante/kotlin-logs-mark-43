package com.ahorraseguros.mx.logskotlinmark43.repositories.ventaNueva

import com.ahorraseguros.mx.logskotlinmark43.models.ventaNueva.InspeccionesModelsLogs
import org.springframework.data.jpa.repository.JpaRepository

interface InspeccionesRepository  : JpaRepository<InspeccionesModelsLogs, Long> {
}