package com.ahorraseguros.mx.logskotlinmark43.repositories.catalogosComercial


import com.ahorraseguros.mx.logskotlinmark43.models.catalogosComercial.TipoSubRamoModelsLogs
import org.springframework.data.jpa.repository.JpaRepository

interface TipoSubRamoRepository: JpaRepository<TipoSubRamoModelsLogs, Long> {
}