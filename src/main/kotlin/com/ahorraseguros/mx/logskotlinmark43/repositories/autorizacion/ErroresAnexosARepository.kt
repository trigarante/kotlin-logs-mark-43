package com.ahorraseguros.mx.logskotlinmark43.repositories.autorizacion

import com.ahorraseguros.mx.logskotlinmark43.models.autorizacion.ErroresAnexosALogs
import org.springframework.data.jpa.repository.JpaRepository

interface ErroresAnexosARepository: JpaRepository<ErroresAnexosALogs, Long> {
}