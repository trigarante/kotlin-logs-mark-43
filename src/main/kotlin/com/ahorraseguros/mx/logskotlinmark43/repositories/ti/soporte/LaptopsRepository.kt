package com.ahorraseguros.mx.logskotlinmark43.repositories.ti.soporte

import com.ahorraseguros.mx.logskotlinmark43.models.ti.soporte.LaptopsModelsLogs
import org.springframework.data.jpa.repository.JpaRepository

interface LaptopsRepository: JpaRepository<LaptopsModelsLogs, Long> {
}