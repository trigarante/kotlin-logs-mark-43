package com.ahorraseguros.mx.logskotlinmark43.repositories.comercial


import com.ahorraseguros.mx.logskotlinmark43.models.comercial.SubRamoModelsLogs
import org.springframework.data.jpa.repository.JpaRepository

interface SubramoRepository: JpaRepository<SubRamoModelsLogs, Long> {
}