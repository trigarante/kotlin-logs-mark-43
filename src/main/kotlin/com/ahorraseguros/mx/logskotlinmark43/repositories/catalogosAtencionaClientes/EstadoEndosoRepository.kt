package com.ahorraseguros.mx.logskotlinmark43.repositories.catalogosAtencionaClientes

import com.ahorraseguros.mx.logskotlinmark43.models.catalogosAtencionaClientes.EstadoEndosoModelsLogs
import org.springframework.data.jpa.repository.JpaRepository

interface EstadoEndosoRepository : JpaRepository<EstadoEndosoModelsLogs, Long> {
}