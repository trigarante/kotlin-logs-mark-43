package com.ahorraseguros.mx.logskotlinmark43.repositories.rh

import com.ahorraseguros.mx.logskotlinmark43.models.rh.CandidatoModelsLogs
import org.springframework.data.jpa.repository.JpaRepository

interface CandidatoRepository:JpaRepository<CandidatoModelsLogs,Long>