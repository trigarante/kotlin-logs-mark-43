package com.ahorraseguros.mx.logskotlinmark43.repositories.catalogosRRHH

import com.ahorraseguros.mx.logskotlinmark43.models.catalogosRRHH.RazonSocialModelsLogs
import org.springframework.data.jpa.repository.JpaRepository

interface RazonSocialRepository: JpaRepository<RazonSocialModelsLogs, Long>
