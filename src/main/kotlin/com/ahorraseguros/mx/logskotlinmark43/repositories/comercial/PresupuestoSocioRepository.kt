package com.ahorraseguros.mx.logskotlinmark43.repositories.comercial

import com.ahorraseguros.mx.logskotlinmark43.models.comercial.PresupuestoSocioModelsLogs
import org.springframework.data.jpa.repository.JpaRepository

interface PresupuestoSocioRepository  : JpaRepository<PresupuestoSocioModelsLogs, Long> {
}