package com.ahorraseguros.mx.logskotlinmark43.repositories.catalogosComercial


import com.ahorraseguros.mx.logskotlinmark43.models.catalogosComercial.SubCategoriaModelsLogs
import org.springframework.data.jpa.repository.JpaRepository

interface SubCategoriaRepository: JpaRepository<SubCategoriaModelsLogs, Long> {
}