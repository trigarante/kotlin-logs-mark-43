package com.ahorraseguros.mx.logskotlinmark43.repositories.catalogosVentaNueva

import com.ahorraseguros.mx.logskotlinmark43.models.catalogosVentaNueva.EstadoReciboModelsLogs
import org.springframework.data.jpa.repository.JpaRepository

interface EstadoReciboRepository: JpaRepository<EstadoReciboModelsLogs, Long> {
}