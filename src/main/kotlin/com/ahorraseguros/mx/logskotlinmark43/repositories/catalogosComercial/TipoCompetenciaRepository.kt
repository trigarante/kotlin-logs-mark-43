package com.ahorraseguros.mx.logskotlinmark43.repositories.catalogosComercial


import com.ahorraseguros.mx.logskotlinmark43.models.catalogosComercial.TipoCompetenciaModelsLogs
import org.springframework.data.jpa.repository.JpaRepository

interface TipoCompetenciaRepository : JpaRepository<TipoCompetenciaModelsLogs, Long> {
}