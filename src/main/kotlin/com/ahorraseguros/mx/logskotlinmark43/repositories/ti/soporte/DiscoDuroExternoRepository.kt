package com.ahorraseguros.mx.logskotlinmark43.repositories.ti.soporte

import com.ahorraseguros.mx.logskotlinmark43.models.ti.soporte.DiscoDuroExternoModelsLogs
import org.springframework.data.jpa.repository.JpaRepository

interface DiscoDuroExternoRepository: JpaRepository<DiscoDuroExternoModelsLogs, Long> {
}