package com.ahorraseguros.mx.logskotlinmark43.repositories.catalogosFinanzas

import com.ahorraseguros.mx.logskotlinmark43.models.catalogosFinanzas.SubareaModelsLogs
import org.springframework.data.jpa.repository.JpaRepository

interface SubAreaRepository : JpaRepository<SubareaModelsLogs, Long> {
}