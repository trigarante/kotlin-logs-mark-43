package com.ahorraseguros.mx.logskotlinmark43.repositories.catalogosRRHH

import com.ahorraseguros.mx.logskotlinmark43.models.catalogosRRHH.CompetenciaModelsLogs
import org.springframework.data.jpa.repository.JpaRepository

interface CompetenciaRepository: JpaRepository<CompetenciaModelsLogs, Long>