package com.ahorraseguros.mx.logskotlinmark43.repositories.catalogosComercial


import com.ahorraseguros.mx.logskotlinmark43.models.catalogosComercial.TipoCoberturaModelsLogs
import org.springframework.data.jpa.repository.JpaRepository

interface TipoCoberturaRepository : JpaRepository<TipoCoberturaModelsLogs, Long> {
}