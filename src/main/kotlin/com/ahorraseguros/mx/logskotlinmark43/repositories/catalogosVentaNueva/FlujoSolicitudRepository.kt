package com.ahorraseguros.mx.logskotlinmark43.repositories.catalogosVentaNueva

import com.ahorraseguros.mx.logskotlinmark43.models.catalogosVentaNueva.FlujoSolicitudModelsLogs
import org.springframework.data.jpa.repository.JpaRepository

interface FlujoSolicitudRepository: JpaRepository<FlujoSolicitudModelsLogs, Long> {
}