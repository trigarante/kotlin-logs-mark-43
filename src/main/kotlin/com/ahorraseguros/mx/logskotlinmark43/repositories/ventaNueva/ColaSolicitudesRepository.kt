package com.ahorraseguros.mx.logskotlinmark43.repositories.ventaNueva

import com.ahorraseguros.mx.logskotlinmark43.models.ventaNueva.ColaSolicitudesModelsLogs
import org.springframework.data.jpa.repository.JpaRepository

interface ColaSolicitudesRepository : JpaRepository<ColaSolicitudesModelsLogs, Long> {
}