package com.ahorraseguros.mx.logskotlinmark43.repositories.catalogosVentaNueva

import com.ahorraseguros.mx.logskotlinmark43.models.catalogosVentaNueva.TarjetasBancoModelsLogs
import org.springframework.data.jpa.repository.JpaRepository

interface TarjetaBancoRepository : JpaRepository<TarjetasBancoModelsLogs, Long> {
}