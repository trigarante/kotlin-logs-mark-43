package com.ahorraseguros.mx.logskotlinmark43.repositories.catalogosVentaNueva

import com.ahorraseguros.mx.logskotlinmark43.models.catalogosVentaNueva.EstadoInspeccionModelsLogs
import org.springframework.data.jpa.repository.JpaRepository

interface EstadoInspeccionRepository : JpaRepository<EstadoInspeccionModelsLogs, Long> {
}