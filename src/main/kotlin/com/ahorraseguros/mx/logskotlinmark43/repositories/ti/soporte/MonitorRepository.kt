package com.ahorraseguros.mx.logskotlinmark43.repositories.ti.soporte

import com.ahorraseguros.mx.logskotlinmark43.models.ti.soporte.MonitorModelsLogs
import org.springframework.data.jpa.repository.JpaRepository

interface MonitorRepository: JpaRepository<MonitorModelsLogs, Long> {
}