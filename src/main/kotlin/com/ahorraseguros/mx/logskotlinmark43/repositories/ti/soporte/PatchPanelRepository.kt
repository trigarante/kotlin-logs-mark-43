package com.ahorraseguros.mx.logskotlinmark43.repositories.ti.soporte

import com.ahorraseguros.mx.logskotlinmark43.models.ti.soporte.PatchPanelModelsLogs
import org.springframework.data.jpa.repository.JpaRepository

interface PatchPanelRepository: JpaRepository<PatchPanelModelsLogs, Long> {
}