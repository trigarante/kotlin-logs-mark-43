package com.ahorraseguros.mx.logskotlinmark43.repositories.ti.soporte


import com.ahorraseguros.mx.logskotlinmark43.models.ti.soporte.DiademaModelsLogs
import org.springframework.data.jpa.repository.JpaRepository

interface DiademaRepository: JpaRepository<DiademaModelsLogs, Long> {
}