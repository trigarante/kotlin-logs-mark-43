package com.ahorraseguros.mx.logskotlinmark43.repositories.ti.soporte

import com.ahorraseguros.mx.logskotlinmark43.models.ti.soporte.ApsModelsLogs
import org.springframework.data.jpa.repository.JpaRepository

interface ApsRepository: JpaRepository<ApsModelsLogs, Long> {
}