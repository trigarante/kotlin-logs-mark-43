package com.ahorraseguros.mx.logskotlinmark43.repositories.ti.soporte


import com.ahorraseguros.mx.logskotlinmark43.models.ti.soporte.ChromeBitModelsLogs
import org.springframework.data.jpa.repository.JpaRepository

interface ChromeBitRepository: JpaRepository<ChromeBitModelsLogs, Long> {
}