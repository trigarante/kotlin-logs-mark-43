package com.ahorraseguros.mx.logskotlinmark43.repositories.catalogosRRHH

import com.ahorraseguros.mx.logskotlinmark43.models.catalogosRRHH.BancosModelsLogs
import org.springframework.data.jpa.repository.JpaRepository

interface BancosRepository: JpaRepository<BancosModelsLogs, Long>