package com.ahorraseguros.mx.logskotlinmark43.repositories.marketing


import com.ahorraseguros.mx.logskotlinmark43.models.marketing.PaginaModelsLogs
import org.springframework.data.jpa.repository.JpaRepository

interface PaginaRepository : JpaRepository<PaginaModelsLogs, Long> {
}