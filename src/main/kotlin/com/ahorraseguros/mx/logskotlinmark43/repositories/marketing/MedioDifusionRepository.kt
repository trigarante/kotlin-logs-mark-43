package com.ahorraseguros.mx.logskotlinmark43.repositories.marketing


import com.ahorraseguros.mx.logskotlinmark43.models.marketing.MedioDifusionModelsLogs
import org.springframework.data.jpa.repository.JpaRepository

interface MedioDifusionRepository: JpaRepository<MedioDifusionModelsLogs, Long> {
}