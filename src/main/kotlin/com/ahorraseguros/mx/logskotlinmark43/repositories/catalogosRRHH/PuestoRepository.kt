package com.ahorraseguros.mx.logskotlinmark43.repositories.catalogosRRHH

import com.ahorraseguros.mx.logskotlinmark43.models.catalogosRRHH.PuestoModelsLogs
import org.springframework.data.jpa.repository.JpaRepository

interface PuestoRepository: JpaRepository<PuestoModelsLogs, Long>