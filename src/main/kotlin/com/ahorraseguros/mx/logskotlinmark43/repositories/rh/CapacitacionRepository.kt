package com.ahorraseguros.mx.logskotlinmark43.repositories.rh

import com.ahorraseguros.mx.logskotlinmark43.models.rh.CapacitacionModelsLogs
import org.springframework.data.jpa.repository.JpaRepository

interface CapacitacionRepository: JpaRepository<CapacitacionModelsLogs, Long>