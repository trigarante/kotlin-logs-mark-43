package com.ahorraseguros.mx.logskotlinmark43.repositories.ti.soporte

import com.ahorraseguros.mx.logskotlinmark43.models.ti.soporte.DiscoDuroInternoModelsLogs
import org.springframework.data.jpa.repository.JpaRepository

interface DiscoDuroInternoRepository: JpaRepository<DiscoDuroInternoModelsLogs, Long> {
}