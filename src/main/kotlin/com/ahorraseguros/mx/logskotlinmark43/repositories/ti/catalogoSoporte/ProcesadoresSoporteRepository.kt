package com.ahorraseguros.mx.logskotlinmark43.repositories.ti.catalogoSoporte


import com.ahorraseguros.mx.logskotlinmark43.models.ti.catalogosSoporte.ProcesadoresSoporteModelsLogs
import org.springframework.data.jpa.repository.JpaRepository

interface ProcesadoresSoporteRepository: JpaRepository<ProcesadoresSoporteModelsLogs, Long> {
}