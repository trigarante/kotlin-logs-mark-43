package com.ahorraseguros.mx.logskotlinmark43.repositories.catalogosVentaNueva

import com.ahorraseguros.mx.logskotlinmark43.models.catalogosVentaNueva.CarrierTarjetasModelsLogs
import org.springframework.data.jpa.repository.JpaRepository

interface CarrrierTarjetaRepository: JpaRepository<CarrierTarjetasModelsLogs, Long> {
}