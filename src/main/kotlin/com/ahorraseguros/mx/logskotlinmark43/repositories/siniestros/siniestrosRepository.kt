package com.ahorraseguros.mx.logskotlinmark43.repositories.siniestros

import com.ahorraseguros.mx.logskotlinmark43.models.siniestros.SiniestroModels
import org.springframework.data.jpa.repository.JpaRepository

interface siniestrosRepository : JpaRepository<SiniestroModels, Long> {
}