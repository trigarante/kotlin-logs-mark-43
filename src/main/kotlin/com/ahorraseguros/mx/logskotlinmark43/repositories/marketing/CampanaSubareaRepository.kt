package com.ahorraseguros.mx.logskotlinmark43.repositories.marketing

import com.ahorraseguros.mx.logskotlinmark43.models.marketing.CampanaSubareaModelsLogs
import org.springframework.data.jpa.repository.JpaRepository

interface CampanaSubareaRepository : JpaRepository<CampanaSubareaModelsLogs, Long>