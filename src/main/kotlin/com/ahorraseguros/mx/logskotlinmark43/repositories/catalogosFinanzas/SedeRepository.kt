package com.ahorraseguros.mx.logskotlinmark43.repositories.catalogosFinanzas

import com.ahorraseguros.mx.logskotlinmark43.models.catalogosFinanzas.SedeModelsLogs
import org.springframework.data.jpa.repository.JpaRepository

interface SedeRepository: JpaRepository<SedeModelsLogs, Long> {
}