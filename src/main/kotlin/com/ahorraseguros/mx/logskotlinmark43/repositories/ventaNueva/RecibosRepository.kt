package com.ahorraseguros.mx.logskotlinmark43.repositories.ventaNueva

import com.ahorraseguros.mx.logskotlinmark43.models.ventaNueva.RecibosModelsLogs
import org.springframework.data.jpa.repository.JpaRepository

interface RecibosRepository  : JpaRepository<RecibosModelsLogs, Long> {
}