package com.ahorraseguros.mx.logskotlinmark43.repositories.ventaNueva

import com.ahorraseguros.mx.logskotlinmark43.models.ventaNueva.RegistroModelsLogs
import org.springframework.data.jpa.repository.JpaRepository

interface RegistroRepository  : JpaRepository<RegistroModelsLogs, Long> {
}