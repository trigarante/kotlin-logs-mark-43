package com.ahorraseguros.mx.logskotlinmark43.repositories.atencionaClientes


import com.ahorraseguros.mx.logskotlinmark43.models.atencionaClientes.EndososCancelacionesModelsLogs
import org.springframework.data.jpa.repository.JpaRepository

interface EndososCancelacionesRepository : JpaRepository<EndososCancelacionesModelsLogs, Long> {
}