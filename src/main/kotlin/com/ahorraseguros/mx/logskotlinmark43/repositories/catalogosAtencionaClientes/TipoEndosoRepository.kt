package com.ahorraseguros.mx.logskotlinmark43.repositories.catalogosAtencionaClientes

import com.ahorraseguros.mx.logskotlinmark43.models.catalogosAtencionaClientes.TipoEndosoModelsLogs
import org.springframework.data.jpa.repository.JpaRepository

interface TipoEndosoRepository : JpaRepository<TipoEndosoModelsLogs, Long> {
}