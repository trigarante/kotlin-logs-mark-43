package com.ahorraseguros.mx.logskotlinmark43.repositories.ti.soporte


import com.ahorraseguros.mx.logskotlinmark43.models.ti.soporte.SwitchModelsLogs
import org.springframework.data.jpa.repository.JpaRepository

interface SwitchRepository: JpaRepository<SwitchModelsLogs, Long> {
}