package com.ahorraseguros.mx.logskotlinmark43.repositories.catalogosRRHH

import com.ahorraseguros.mx.logskotlinmark43.models.catalogosRRHH.PuestoTipoModelsLogs
import org.springframework.data.jpa.repository.JpaRepository

interface PuestoTipoRepository: JpaRepository<PuestoTipoModelsLogs, Long>