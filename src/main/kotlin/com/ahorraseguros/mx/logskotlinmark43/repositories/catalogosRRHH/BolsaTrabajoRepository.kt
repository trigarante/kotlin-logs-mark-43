package com.ahorraseguros.mx.logskotlinmark43.repositories.catalogosRRHH

import com.ahorraseguros.mx.logskotlinmark43.models.catalogosRRHH.BolsaTrabajoModelsLogs
import org.springframework.data.jpa.repository.JpaRepository

interface BolsaTrabajoRepository: JpaRepository<BolsaTrabajoModelsLogs, Long>