package com.ahorraseguros.mx.logskotlinmark43.repositories.comercial


import com.ahorraseguros.mx.logskotlinmark43.models.comercial.RamoModelsLogs
import org.springframework.data.jpa.repository.JpaRepository

interface RamoRepository: JpaRepository<RamoModelsLogs, Long> {
}