package com.ahorraseguros.mx.logskotlinmark43.repositories.ti.soporte

import com.ahorraseguros.mx.logskotlinmark43.models.ti.soporte.TarjetaRedModelsLogs
import org.springframework.data.jpa.repository.JpaRepository

interface TarjetaRedRepository: JpaRepository<TarjetaRedModelsLogs, Long> {
}