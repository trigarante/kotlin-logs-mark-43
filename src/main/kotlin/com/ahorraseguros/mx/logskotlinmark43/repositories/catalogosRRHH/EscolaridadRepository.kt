package com.ahorraseguros.mx.logskotlinmark43.repositories.catalogosRRHH

import com.ahorraseguros.mx.logskotlinmark43.models.catalogosRRHH.EscolaridadModelsLogs
import org.springframework.data.jpa.repository.JpaRepository

interface EscolaridadRepository: JpaRepository<EscolaridadModelsLogs, Long>