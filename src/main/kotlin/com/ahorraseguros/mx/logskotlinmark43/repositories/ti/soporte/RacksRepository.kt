package com.ahorraseguros.mx.logskotlinmark43.repositories.ti.soporte

import com.ahorraseguros.mx.logskotlinmark43.models.ti.soporte.RacksModelsLogs
import org.springframework.data.jpa.repository.JpaRepository

interface RacksRepository: JpaRepository<RacksModelsLogs, Long> {
}