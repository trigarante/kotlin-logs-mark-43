package com.ahorraseguros.mx.logskotlinmark43.repositories.rh

import com.ahorraseguros.mx.logskotlinmark43.models.rh.SolicitudesRrhhModelsLogs
import org.springframework.data.jpa.repository.JpaRepository

interface SolicitudesRrhhRepository: JpaRepository<SolicitudesRrhhModelsLogs, Long> {
}