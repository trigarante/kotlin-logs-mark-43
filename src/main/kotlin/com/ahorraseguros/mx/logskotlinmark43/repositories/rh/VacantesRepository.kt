package com.ahorraseguros.mx.logskotlinmark43.repositories.rh

import com.ahorraseguros.mx.logskotlinmark43.models.rh.VacantesModelsLogs
import org.springframework.data.jpa.repository.JpaRepository

interface VacantesRepository: JpaRepository<VacantesModelsLogs, Long>