package com.ahorraseguros.mx.logskotlinmark43.repositories.comercial


import com.ahorraseguros.mx.logskotlinmark43.models.comercial.ConveniosModelsLogs
import org.springframework.data.jpa.repository.JpaRepository

interface ConveniosRepository: JpaRepository<ConveniosModelsLogs, Long> {
}