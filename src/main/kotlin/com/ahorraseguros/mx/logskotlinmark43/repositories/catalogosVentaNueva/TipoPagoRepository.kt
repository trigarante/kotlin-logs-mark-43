package com.ahorraseguros.mx.logskotlinmark43.repositories.catalogosVentaNueva

import com.ahorraseguros.mx.logskotlinmark43.models.catalogosVentaNueva.TipoPagoModelsLogs
import org.springframework.data.jpa.repository.JpaRepository

interface TipoPagoRepository: JpaRepository<TipoPagoModelsLogs, Long> {
}