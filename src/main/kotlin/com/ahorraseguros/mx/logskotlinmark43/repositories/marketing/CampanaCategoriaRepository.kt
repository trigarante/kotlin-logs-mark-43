package com.ahorraseguros.mx.logskotlinmark43.repositories.marketing


import com.ahorraseguros.mx.logskotlinmark43.models.marketing.CampanaCategoriaModelsLogs
import org.springframework.data.jpa.repository.JpaRepository

interface CampanaCategoriaRepository: JpaRepository<CampanaCategoriaModelsLogs, Long> {
}