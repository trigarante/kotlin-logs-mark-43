package com.ahorraseguros.mx.logskotlinmark43.repositories.ti

import com.ahorraseguros.mx.logskotlinmark43.models.ti.GrupoUsuariosModelsLogs
import org.springframework.data.jpa.repository.JpaRepository

interface GrupoUsuariosRepository: JpaRepository<GrupoUsuariosModelsLogs, Long>