package com.ahorraseguros.mx.logskotlinmark43.repositories.ti.soporte

import com.ahorraseguros.mx.logskotlinmark43.models.ti.soporte.TabletsModelsLogs
import org.springframework.data.jpa.repository.JpaRepository

interface TabletsRepository: JpaRepository<TabletsModelsLogs, Long> {
}