package com.ahorraseguros.mx.logskotlinmark43.repositories.ti.soporte

import com.ahorraseguros.mx.logskotlinmark43.models.ti.soporte.BarrasMulticontactoModelsLogs
import org.springframework.data.jpa.repository.JpaRepository

interface BarrasMulticontactoRepository: JpaRepository<BarrasMulticontactoModelsLogs, Long> {
}