package com.ahorraseguros.mx.logskotlinmark43.repositories.comercial

import com.ahorraseguros.mx.logskotlinmark43.models.comercial.SociosModelsLogs
import org.springframework.data.jpa.repository.JpaRepository

interface SociosRepository : JpaRepository<SociosModelsLogs, Long> {
}