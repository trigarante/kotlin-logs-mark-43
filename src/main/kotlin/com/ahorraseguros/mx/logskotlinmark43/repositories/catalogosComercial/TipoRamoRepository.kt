package com.ahorraseguros.mx.logskotlinmark43.repositories.catalogosComercial


import com.ahorraseguros.mx.logskotlinmark43.models.catalogosComercial.TipoRamoModelsLogs
import org.springframework.data.jpa.repository.JpaRepository

interface TipoRamoRepository : JpaRepository<TipoRamoModelsLogs, Long> {
}