package com.ahorraseguros.mx.logskotlinmark43.repositories.catalogosComercial

import com.ahorraseguros.mx.logskotlinmark43.models.catalogosComercial.CategoriaModelsLogs
import org.springframework.data.jpa.repository.JpaRepository

interface CategoriaRepository: JpaRepository<CategoriaModelsLogs, Long> {
}