package com.ahorraseguros.mx.logskotlinmark43.repositories.ventaNueva

import com.ahorraseguros.mx.logskotlinmark43.models.ventaNueva.ProductoSolicitudModelsLogs
import org.springframework.data.jpa.repository.JpaRepository

interface ProductoSolicitudRepository : JpaRepository<ProductoSolicitudModelsLogs, Long> {
}