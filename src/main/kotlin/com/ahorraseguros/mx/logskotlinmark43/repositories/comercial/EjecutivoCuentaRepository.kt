package com.ahorraseguros.mx.logskotlinmark43.repositories.comercial

import com.ahorraseguros.mx.logskotlinmark43.models.comercial.EjecutivoCuentaModelsLogs
import org.springframework.data.jpa.repository.JpaRepository

interface EjecutivoCuentaRepository: JpaRepository<EjecutivoCuentaModelsLogs, Long> {
}