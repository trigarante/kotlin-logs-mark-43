package com.ahorraseguros.mx.logskotlinmark43.repositories.verificacion

import com.ahorraseguros.mx.logskotlinmark43.models.autorizacion.ErroresAutorizacionLogs
import com.ahorraseguros.mx.logskotlinmark43.models.verificacion.VerificacionRegistroModelsLogs
import org.springframework.data.jpa.repository.JpaRepository

interface VerificacacionRegistroRepository: JpaRepository<VerificacionRegistroModelsLogs, Long> {
}