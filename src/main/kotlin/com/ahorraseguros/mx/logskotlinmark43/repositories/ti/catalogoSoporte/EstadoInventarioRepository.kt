package com.ahorraseguros.mx.logskotlinmark43.repositories.ti.catalogoSoporte


import com.ahorraseguros.mx.logskotlinmark43.models.ti.catalogosSoporte.EstadoInventarioModelsLogs
import org.springframework.data.jpa.repository.JpaRepository

interface EstadoInventarioRepository : JpaRepository<EstadoInventarioModelsLogs, Long> {
}