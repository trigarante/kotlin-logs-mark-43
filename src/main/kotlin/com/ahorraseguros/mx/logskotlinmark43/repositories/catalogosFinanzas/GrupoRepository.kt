package com.ahorraseguros.mx.logskotlinmark43.repositories.catalogosFinanzas


import com.ahorraseguros.mx.logskotlinmark43.models.catalogosFinanzas.GrupoModelsLogs
import org.springframework.data.jpa.repository.JpaRepository

interface GrupoRepository : JpaRepository<GrupoModelsLogs, Long> {
}