package com.ahorraseguros.mx.logskotlinmark43.repositories.ti.soporte


import com.ahorraseguros.mx.logskotlinmark43.models.ti.soporte.CamarasModelsLogs
import org.springframework.data.jpa.repository.JpaRepository

interface CamarasRepository: JpaRepository<CamarasModelsLogs, Long> {
}