package com.ahorraseguros.mx.logskotlinmark43.repositories.ti.soporte
import com.ahorraseguros.mx.logskotlinmark43.models.ti.soporte.CpuModel
import org.springframework.data.jpa.repository.JpaRepository


interface CpuRepository: JpaRepository<CpuModel, Long>