package com.ahorraseguros.mx.logskotlinmark43.repositories.comercial


import com.ahorraseguros.mx.logskotlinmark43.models.comercial.CoberturasModelsLogs
import org.springframework.data.jpa.repository.JpaRepository

interface CoberturasRepository : JpaRepository<CoberturasModelsLogs, Long> {
}