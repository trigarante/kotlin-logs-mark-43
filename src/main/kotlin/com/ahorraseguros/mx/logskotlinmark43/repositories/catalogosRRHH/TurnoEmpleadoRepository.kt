package com.ahorraseguros.mx.logskotlinmark43.repositories.catalogosRRHH


import com.ahorraseguros.mx.logskotlinmark43.models.catalogosRRHH.TurnoEmpleadoModelsLogs
import org.springframework.data.jpa.repository.JpaRepository

interface TurnoEmpleadoRepository : JpaRepository<TurnoEmpleadoModelsLogs, Long> {
}