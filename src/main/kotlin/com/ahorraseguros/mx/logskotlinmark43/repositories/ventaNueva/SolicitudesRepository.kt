package com.ahorraseguros.mx.logskotlinmark43.repositories.ventaNueva

import com.ahorraseguros.mx.logskotlinmark43.models.ventaNueva.SolicitudesModelsLogs
import org.springframework.data.jpa.repository.JpaRepository

interface SolicitudesRepository : JpaRepository<SolicitudesModelsLogs, Long> {
}