package com.ahorraseguros.mx.logskotlinmark43.repositories.comercial

import com.ahorraseguros.mx.logskotlinmark43.models.comercial.CompetenciasSocioModelsLogs
import org.springframework.data.jpa.repository.JpaRepository

interface CompetenciasSocioRepository : JpaRepository<CompetenciasSocioModelsLogs, Long> {
}