package com.ahorraseguros.mx.logskotlinmark43.repositories.rh

import com.ahorraseguros.mx.logskotlinmark43.models.rh.SeguimientoModelsLogs
import org.springframework.data.jpa.repository.JpaRepository

interface SeguimientoRepository: JpaRepository<SeguimientoModelsLogs, Long> {
}