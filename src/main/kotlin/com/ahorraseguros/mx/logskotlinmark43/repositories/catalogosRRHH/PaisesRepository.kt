package com.ahorraseguros.mx.logskotlinmark43.repositories.catalogosRRHH

import com.ahorraseguros.mx.logskotlinmark43.models.catalogosRRHH.PaisesModelsLogs
import org.springframework.data.jpa.repository.JpaRepository

interface PaisesRepository: JpaRepository<PaisesModelsLogs, Long>