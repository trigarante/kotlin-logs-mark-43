package com.ahorraseguros.mx.logskotlinmark43.repositories.catalogosComercial


import com.ahorraseguros.mx.logskotlinmark43.models.catalogosComercial.EstadoSocioModelsLogs
import org.springframework.data.jpa.repository.JpaRepository

interface EstadoSocioRepository: JpaRepository<EstadoSocioModelsLogs, Long> {
}