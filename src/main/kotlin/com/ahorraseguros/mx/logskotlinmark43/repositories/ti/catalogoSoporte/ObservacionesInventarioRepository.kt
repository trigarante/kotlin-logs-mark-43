package com.ahorraseguros.mx.logskotlinmark43.repositories.ti.catalogoSoporte


import com.ahorraseguros.mx.logskotlinmark43.models.ti.catalogosSoporte.ObservacionesInventarioModelsLogs
import org.springframework.data.jpa.repository.JpaRepository

interface ObservacionesInventarioRepository : JpaRepository<ObservacionesInventarioModelsLogs, Long> {
}