package com.ahorraseguros.mx.logskotlinmark43.repositories.ti.catalogos

import com.ahorraseguros.mx.logskotlinmark43.models.ti.catalogos.PermisosVisualizacionModelLogs
import org.springframework.data.jpa.repository.JpaRepository

interface PermisosVisualizacionRepository : JpaRepository<PermisosVisualizacionModelLogs, Long>