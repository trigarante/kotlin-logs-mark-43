package com.ahorraseguros.mx.logskotlinmark43.repositories.catalogosSiniestros

import com.ahorraseguros.mx.logskotlinmark43.models.catalogosSiniestros.TipoSiniestroModelsLogs
import org.springframework.data.jpa.repository.JpaRepository

interface TipoSiniestroRepository : JpaRepository<TipoSiniestroModelsLogs, Long> {
}