package com.ahorraseguros.mx.logskotlinmark43.repositories.catalogosVentaNueva

import com.ahorraseguros.mx.logskotlinmark43.models.catalogosVentaNueva.TipoContactoModelsLogs
import org.springframework.data.jpa.repository.JpaRepository

interface TipoContactoRepository : JpaRepository<TipoContactoModelsLogs, Long> {
}