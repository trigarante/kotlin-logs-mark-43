package com.ahorraseguros.mx.logskotlinmark43.repositories.autorizacion

import com.ahorraseguros.mx.logskotlinmark43.models.autorizacion.ErroresAutorizacionLogs
import org.springframework.data.jpa.repository.JpaRepository

interface ErroresAutorizacionRepository : JpaRepository<ErroresAutorizacionLogs, Long> {
}