package com.ahorraseguros.mx.logskotlinmark43.repositories.ti

import com.ahorraseguros.mx.logskotlinmark43.models.ti.UsuariosModelsLogs
import org.springframework.data.jpa.repository.JpaRepository

interface UsuariosRepository: JpaRepository<UsuariosModelsLogs, Long>