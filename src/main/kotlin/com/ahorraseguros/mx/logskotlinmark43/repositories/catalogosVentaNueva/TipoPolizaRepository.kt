package com.ahorraseguros.mx.logskotlinmark43.repositories.catalogosVentaNueva

import com.ahorraseguros.mx.logskotlinmark43.models.catalogosVentaNueva.TipoPolizaModelsLogs
import org.springframework.data.jpa.repository.JpaRepository

interface TipoPolizaRepository: JpaRepository<TipoPolizaModelsLogs, Long> {
}