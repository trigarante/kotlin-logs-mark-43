package com.ahorraseguros.mx.logskotlinmark43.repositories.ti.catalogoSoporte

import com.ahorraseguros.mx.logskotlinmark43.models.ti.catalogosSoporte.MarcaSoporteModelsLogs
import org.springframework.data.jpa.repository.JpaRepository

interface MarcaSoporteRepository: JpaRepository<MarcaSoporteModelsLogs, Long> {
}