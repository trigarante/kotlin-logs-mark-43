package com.ahorraseguros.mx.logskotlinmark43.repositories.catalogosVentaNueva

import com.ahorraseguros.mx.logskotlinmark43.models.catalogosVentaNueva.EtiquetaSolicitudModelsLogs
import org.springframework.data.jpa.repository.JpaRepository

interface EtiquetaSolicitudRepository: JpaRepository<EtiquetaSolicitudModelsLogs, Long> {
}