package com.ahorraseguros.mx.logskotlinmark43.repositories.finanzas

import com.ahorraseguros.mx.logskotlinmark43.models.finanzas.GestionIngresosModelsLogs
import org.springframework.data.jpa.repository.JpaRepository

interface GestionIngresosRepository : JpaRepository<GestionIngresosModelsLogs, Long> {
}