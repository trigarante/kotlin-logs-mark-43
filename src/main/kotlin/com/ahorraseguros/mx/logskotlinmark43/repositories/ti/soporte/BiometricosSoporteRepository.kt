package com.ahorraseguros.mx.logskotlinmark43.repositories.ti.soporte


import com.ahorraseguros.mx.logskotlinmark43.models.ti.soporte.BiometricosSoporteModelsLogs
import org.springframework.data.jpa.repository.JpaRepository

interface BiometricosSoporteRepository: JpaRepository<BiometricosSoporteModelsLogs, Long> {
}