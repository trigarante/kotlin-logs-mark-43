package com.ahorraseguros.mx.logskotlinmark43.repositories.catalogosFinanzas


import com.ahorraseguros.mx.logskotlinmark43.models.catalogosFinanzas.EmpresaModelsLogs
import org.springframework.data.jpa.repository.JpaRepository

interface EmpresaRepository : JpaRepository<EmpresaModelsLogs, Long> {
}