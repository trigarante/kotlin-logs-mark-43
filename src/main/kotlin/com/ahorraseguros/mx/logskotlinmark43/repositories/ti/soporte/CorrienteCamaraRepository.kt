package com.ahorraseguros.mx.logskotlinmark43.repositories.ti.soporte


import com.ahorraseguros.mx.logskotlinmark43.models.ti.soporte.CorrienteCamaraModelsLogs
import org.springframework.data.jpa.repository.JpaRepository

interface CorrienteCamaraRepository: JpaRepository<CorrienteCamaraModelsLogs, Long> {
}