package com.ahorraseguros.mx.logskotlinmark43.repositories.ventaNueva

import com.ahorraseguros.mx.logskotlinmark43.models.ventaNueva.ProspectoModelsLogs
import org.springframework.data.jpa.repository.JpaRepository

interface ProspectoRepository  : JpaRepository<ProspectoModelsLogs, Long> {
}