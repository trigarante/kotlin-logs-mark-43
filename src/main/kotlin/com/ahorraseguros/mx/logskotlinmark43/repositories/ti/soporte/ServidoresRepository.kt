package com.ahorraseguros.mx.logskotlinmark43.repositories.ti.soporte

import com.ahorraseguros.mx.logskotlinmark43.models.ti.soporte.ServidoresModelsLogs
import org.springframework.data.jpa.repository.JpaRepository

interface ServidoresRepository: JpaRepository<ServidoresModelsLogs, Long>  {
}