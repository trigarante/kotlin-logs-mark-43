package com.ahorraseguros.mx.logskotlinmark43.repositories.verificacion

import com.ahorraseguros.mx.logskotlinmark43.models.autorizacion.ErroresAutorizacionLogs
import com.ahorraseguros.mx.logskotlinmark43.models.verificacion.ErroresVerificacionModelsLogs
import org.springframework.data.jpa.repository.JpaRepository

interface ErroresVerificacionRepository: JpaRepository<ErroresVerificacionModelsLogs, Long> {
}