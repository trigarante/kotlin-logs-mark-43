package com.ahorraseguros.mx.logskotlinmark43.repositories.autorizacion

import com.ahorraseguros.mx.logskotlinmark43.models.autorizacion.AutorizacionRegistroLogs
import org.springframework.data.jpa.repository.JpaRepository

interface AutorizacionRegistroRepository : JpaRepository<AutorizacionRegistroLogs, Long> {
}