package com.ahorraseguros.mx.logskotlinmark43.repositories.comercial


import com.ahorraseguros.mx.logskotlinmark43.models.comercial.ContratosSociosModelsLogs
import org.springframework.data.jpa.repository.JpaRepository

interface ContratosSociosRepository : JpaRepository<ContratosSociosModelsLogs, Long> {
}