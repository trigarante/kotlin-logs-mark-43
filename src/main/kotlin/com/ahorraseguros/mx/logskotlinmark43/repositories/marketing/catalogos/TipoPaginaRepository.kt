package com.ahorraseguros.mx.logskotlinmark43.repositories.marketing.catalogos


import com.ahorraseguros.mx.logskotlinmark43.models.marketing.catalogos.TipoPaginaModelsLogs
import org.springframework.data.jpa.repository.JpaRepository

interface TipoPaginaRepository : JpaRepository<TipoPaginaModelsLogs, Long> {
}