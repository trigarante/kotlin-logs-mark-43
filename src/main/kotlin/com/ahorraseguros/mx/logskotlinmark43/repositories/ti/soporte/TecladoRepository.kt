package com.ahorraseguros.mx.logskotlinmark43.repositories.ti.soporte


import com.ahorraseguros.mx.logskotlinmark43.models.ti.soporte.TecladoModelsLogs
import org.springframework.data.jpa.repository.JpaRepository

interface TecladoRepository: JpaRepository<TecladoModelsLogs, Long> {
}