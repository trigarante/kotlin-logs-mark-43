package com.ahorraseguros.mx.logskotlinmark43.repositories.catalogosComercial


import com.ahorraseguros.mx.logskotlinmark43.models.catalogosComercial.TipoProductoModelsLogs
import org.springframework.data.jpa.repository.JpaRepository

interface TipoProductoRepository : JpaRepository<TipoProductoModelsLogs, Long> {
}